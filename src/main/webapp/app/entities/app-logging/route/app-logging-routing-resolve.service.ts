import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAppLogging } from '../app-logging.model';
import { AppLoggingService } from '../service/app-logging.service';

@Injectable({ providedIn: 'root' })
export class AppLoggingRoutingResolveService implements Resolve<IAppLogging | null> {
  constructor(protected service: AppLoggingService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAppLogging | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((appLogging: HttpResponse<IAppLogging>) => {
          if (appLogging.body) {
            return of(appLogging.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
