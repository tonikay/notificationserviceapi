import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AppLoggingComponent } from '../list/app-logging.component';
import { AppLoggingDetailComponent } from '../detail/app-logging-detail.component';
import { AppLoggingUpdateComponent } from '../update/app-logging-update.component';
import { AppLoggingRoutingResolveService } from './app-logging-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const appLoggingRoute: Routes = [
  {
    path: '',
    component: AppLoggingComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AppLoggingDetailComponent,
    resolve: {
      appLogging: AppLoggingRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AppLoggingUpdateComponent,
    resolve: {
      appLogging: AppLoggingRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AppLoggingUpdateComponent,
    resolve: {
      appLogging: AppLoggingRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(appLoggingRoute)],
  exports: [RouterModule],
})
export class AppLoggingRoutingModule {}
