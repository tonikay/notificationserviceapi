import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLoggingDetailComponent } from './app-logging-detail.component';

describe('AppLogging Management Detail Component', () => {
  let comp: AppLoggingDetailComponent;
  let fixture: ComponentFixture<AppLoggingDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppLoggingDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ appLogging: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AppLoggingDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AppLoggingDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load appLogging on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.appLogging).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
