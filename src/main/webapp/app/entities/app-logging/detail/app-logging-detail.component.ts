import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAppLogging } from '../app-logging.model';

@Component({
  selector: 'jhi-app-logging-detail',
  templateUrl: './app-logging-detail.component.html',
})
export class AppLoggingDetailComponent implements OnInit {
  appLogging: IAppLogging | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ appLogging }) => {
      this.appLogging = appLogging;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
