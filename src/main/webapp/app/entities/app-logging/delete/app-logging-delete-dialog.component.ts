import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAppLogging } from '../app-logging.model';
import { AppLoggingService } from '../service/app-logging.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './app-logging-delete-dialog.component.html',
})
export class AppLoggingDeleteDialogComponent {
  appLogging?: IAppLogging;

  constructor(protected appLoggingService: AppLoggingService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.appLoggingService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
