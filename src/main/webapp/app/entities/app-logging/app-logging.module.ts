import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AppLoggingComponent } from './list/app-logging.component';
import { AppLoggingDetailComponent } from './detail/app-logging-detail.component';
import { AppLoggingUpdateComponent } from './update/app-logging-update.component';
import { AppLoggingDeleteDialogComponent } from './delete/app-logging-delete-dialog.component';
import { AppLoggingRoutingModule } from './route/app-logging-routing.module';

@NgModule({
  imports: [SharedModule, AppLoggingRoutingModule],
  declarations: [AppLoggingComponent, AppLoggingDetailComponent, AppLoggingUpdateComponent, AppLoggingDeleteDialogComponent],
})
export class AppLoggingModule {}
