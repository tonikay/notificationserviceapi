import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IAppLogging, NewAppLogging } from '../app-logging.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAppLogging for edit and NewAppLoggingFormGroupInput for create.
 */
type AppLoggingFormGroupInput = IAppLogging | PartialWithRequiredKeyOf<NewAppLogging>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IAppLogging | NewAppLogging> = Omit<T, 'createdDate'> & {
  createdDate?: string | null;
};

type AppLoggingFormRawValue = FormValueOf<IAppLogging>;

type NewAppLoggingFormRawValue = FormValueOf<NewAppLogging>;

type AppLoggingFormDefaults = Pick<NewAppLogging, 'id' | 'createdDate'>;

type AppLoggingFormGroupContent = {
  id: FormControl<AppLoggingFormRawValue['id'] | NewAppLogging['id']>;
  comments: FormControl<AppLoggingFormRawValue['comments']>;
  actionType: FormControl<AppLoggingFormRawValue['actionType']>;
  createdDate: FormControl<AppLoggingFormRawValue['createdDate']>;
  application: FormControl<AppLoggingFormRawValue['application']>;
};

export type AppLoggingFormGroup = FormGroup<AppLoggingFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AppLoggingFormService {
  createAppLoggingFormGroup(appLogging: AppLoggingFormGroupInput = { id: null }): AppLoggingFormGroup {
    const appLoggingRawValue = this.convertAppLoggingToAppLoggingRawValue({
      ...this.getFormDefaults(),
      ...appLogging,
    });
    return new FormGroup<AppLoggingFormGroupContent>({
      id: new FormControl(
        { value: appLoggingRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      comments: new FormControl(appLoggingRawValue.comments),
      actionType: new FormControl(appLoggingRawValue.actionType),
      createdDate: new FormControl(appLoggingRawValue.createdDate),
      application: new FormControl(appLoggingRawValue.application),
    });
  }

  getAppLogging(form: AppLoggingFormGroup): IAppLogging | NewAppLogging {
    return this.convertAppLoggingRawValueToAppLogging(form.getRawValue() as AppLoggingFormRawValue | NewAppLoggingFormRawValue);
  }

  resetForm(form: AppLoggingFormGroup, appLogging: AppLoggingFormGroupInput): void {
    const appLoggingRawValue = this.convertAppLoggingToAppLoggingRawValue({ ...this.getFormDefaults(), ...appLogging });
    form.reset(
      {
        ...appLoggingRawValue,
        id: { value: appLoggingRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): AppLoggingFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      createdDate: currentTime,
    };
  }

  private convertAppLoggingRawValueToAppLogging(
    rawAppLogging: AppLoggingFormRawValue | NewAppLoggingFormRawValue
  ): IAppLogging | NewAppLogging {
    return {
      ...rawAppLogging,
      createdDate: dayjs(rawAppLogging.createdDate, DATE_TIME_FORMAT),
    };
  }

  private convertAppLoggingToAppLoggingRawValue(
    appLogging: IAppLogging | (Partial<NewAppLogging> & AppLoggingFormDefaults)
  ): AppLoggingFormRawValue | PartialWithRequiredKeyOf<NewAppLoggingFormRawValue> {
    return {
      ...appLogging,
      createdDate: appLogging.createdDate ? appLogging.createdDate.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
