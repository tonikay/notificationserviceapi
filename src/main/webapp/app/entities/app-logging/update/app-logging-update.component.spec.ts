import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { AppLoggingFormService } from './app-logging-form.service';
import { AppLoggingService } from '../service/app-logging.service';
import { IAppLogging } from '../app-logging.model';
import { IApplication } from 'app/entities/application/application.model';
import { ApplicationService } from 'app/entities/application/service/application.service';

import { AppLoggingUpdateComponent } from './app-logging-update.component';

describe('AppLogging Management Update Component', () => {
  let comp: AppLoggingUpdateComponent;
  let fixture: ComponentFixture<AppLoggingUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let appLoggingFormService: AppLoggingFormService;
  let appLoggingService: AppLoggingService;
  let applicationService: ApplicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [AppLoggingUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AppLoggingUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AppLoggingUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    appLoggingFormService = TestBed.inject(AppLoggingFormService);
    appLoggingService = TestBed.inject(AppLoggingService);
    applicationService = TestBed.inject(ApplicationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Application query and add missing value', () => {
      const appLogging: IAppLogging = { id: 456 };
      const application: IApplication = { id: 53222 };
      appLogging.application = application;

      const applicationCollection: IApplication[] = [{ id: 86840 }];
      jest.spyOn(applicationService, 'query').mockReturnValue(of(new HttpResponse({ body: applicationCollection })));
      const additionalApplications = [application];
      const expectedCollection: IApplication[] = [...additionalApplications, ...applicationCollection];
      jest.spyOn(applicationService, 'addApplicationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ appLogging });
      comp.ngOnInit();

      expect(applicationService.query).toHaveBeenCalled();
      expect(applicationService.addApplicationToCollectionIfMissing).toHaveBeenCalledWith(
        applicationCollection,
        ...additionalApplications.map(expect.objectContaining)
      );
      expect(comp.applicationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const appLogging: IAppLogging = { id: 456 };
      const application: IApplication = { id: 15178 };
      appLogging.application = application;

      activatedRoute.data = of({ appLogging });
      comp.ngOnInit();

      expect(comp.applicationsSharedCollection).toContain(application);
      expect(comp.appLogging).toEqual(appLogging);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAppLogging>>();
      const appLogging = { id: 123 };
      jest.spyOn(appLoggingFormService, 'getAppLogging').mockReturnValue(appLogging);
      jest.spyOn(appLoggingService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ appLogging });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: appLogging }));
      saveSubject.complete();

      // THEN
      expect(appLoggingFormService.getAppLogging).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(appLoggingService.update).toHaveBeenCalledWith(expect.objectContaining(appLogging));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAppLogging>>();
      const appLogging = { id: 123 };
      jest.spyOn(appLoggingFormService, 'getAppLogging').mockReturnValue({ id: null });
      jest.spyOn(appLoggingService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ appLogging: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: appLogging }));
      saveSubject.complete();

      // THEN
      expect(appLoggingFormService.getAppLogging).toHaveBeenCalled();
      expect(appLoggingService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAppLogging>>();
      const appLogging = { id: 123 };
      jest.spyOn(appLoggingService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ appLogging });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(appLoggingService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareApplication', () => {
      it('Should forward to applicationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(applicationService, 'compareApplication');
        comp.compareApplication(entity, entity2);
        expect(applicationService.compareApplication).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
