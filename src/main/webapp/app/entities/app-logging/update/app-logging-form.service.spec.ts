import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../app-logging.test-samples';

import { AppLoggingFormService } from './app-logging-form.service';

describe('AppLogging Form Service', () => {
  let service: AppLoggingFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppLoggingFormService);
  });

  describe('Service methods', () => {
    describe('createAppLoggingFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createAppLoggingFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            comments: expect.any(Object),
            actionType: expect.any(Object),
            createdDate: expect.any(Object),
            application: expect.any(Object),
          })
        );
      });

      it('passing IAppLogging should create a new form with FormGroup', () => {
        const formGroup = service.createAppLoggingFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            comments: expect.any(Object),
            actionType: expect.any(Object),
            createdDate: expect.any(Object),
            application: expect.any(Object),
          })
        );
      });
    });

    describe('getAppLogging', () => {
      it('should return NewAppLogging for default AppLogging initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createAppLoggingFormGroup(sampleWithNewData);

        const appLogging = service.getAppLogging(formGroup) as any;

        expect(appLogging).toMatchObject(sampleWithNewData);
      });

      it('should return NewAppLogging for empty AppLogging initial value', () => {
        const formGroup = service.createAppLoggingFormGroup();

        const appLogging = service.getAppLogging(formGroup) as any;

        expect(appLogging).toMatchObject({});
      });

      it('should return IAppLogging', () => {
        const formGroup = service.createAppLoggingFormGroup(sampleWithRequiredData);

        const appLogging = service.getAppLogging(formGroup) as any;

        expect(appLogging).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IAppLogging should not enable id FormControl', () => {
        const formGroup = service.createAppLoggingFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewAppLogging should disable id FormControl', () => {
        const formGroup = service.createAppLoggingFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
