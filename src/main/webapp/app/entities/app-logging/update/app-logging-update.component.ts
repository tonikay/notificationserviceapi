import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { AppLoggingFormService, AppLoggingFormGroup } from './app-logging-form.service';
import { IAppLogging } from '../app-logging.model';
import { AppLoggingService } from '../service/app-logging.service';
import { IApplication } from 'app/entities/application/application.model';
import { ApplicationService } from 'app/entities/application/service/application.service';
import { ActionType } from 'app/entities/enumerations/action-type.model';

@Component({
  selector: 'jhi-app-logging-update',
  templateUrl: './app-logging-update.component.html',
})
export class AppLoggingUpdateComponent implements OnInit {
  isSaving = false;
  appLogging: IAppLogging | null = null;
  actionTypeValues = Object.keys(ActionType);

  applicationsSharedCollection: IApplication[] = [];

  editForm: AppLoggingFormGroup = this.appLoggingFormService.createAppLoggingFormGroup();

  constructor(
    protected appLoggingService: AppLoggingService,
    protected appLoggingFormService: AppLoggingFormService,
    protected applicationService: ApplicationService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareApplication = (o1: IApplication | null, o2: IApplication | null): boolean => this.applicationService.compareApplication(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ appLogging }) => {
      this.appLogging = appLogging;
      if (appLogging) {
        this.updateForm(appLogging);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const appLogging = this.appLoggingFormService.getAppLogging(this.editForm);
    if (appLogging.id !== null) {
      this.subscribeToSaveResponse(this.appLoggingService.update(appLogging));
    } else {
      this.subscribeToSaveResponse(this.appLoggingService.create(appLogging));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAppLogging>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(appLogging: IAppLogging): void {
    this.appLogging = appLogging;
    this.appLoggingFormService.resetForm(this.editForm, appLogging);

    this.applicationsSharedCollection = this.applicationService.addApplicationToCollectionIfMissing<IApplication>(
      this.applicationsSharedCollection,
      appLogging.application
    );
  }

  protected loadRelationshipsOptions(): void {
    this.applicationService
      .query()
      .pipe(map((res: HttpResponse<IApplication[]>) => res.body ?? []))
      .pipe(
        map((applications: IApplication[]) =>
          this.applicationService.addApplicationToCollectionIfMissing<IApplication>(applications, this.appLogging?.application)
        )
      )
      .subscribe((applications: IApplication[]) => (this.applicationsSharedCollection = applications));
  }
}
