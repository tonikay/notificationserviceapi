import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IAppLogging } from '../app-logging.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../app-logging.test-samples';

import { AppLoggingService, RestAppLogging } from './app-logging.service';

const requireRestSample: RestAppLogging = {
  ...sampleWithRequiredData,
  createdDate: sampleWithRequiredData.createdDate?.toJSON(),
};

describe('AppLogging Service', () => {
  let service: AppLoggingService;
  let httpMock: HttpTestingController;
  let expectedResult: IAppLogging | IAppLogging[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AppLoggingService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a AppLogging', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const appLogging = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(appLogging).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a AppLogging', () => {
      const appLogging = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(appLogging).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a AppLogging', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AppLogging', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a AppLogging', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addAppLoggingToCollectionIfMissing', () => {
      it('should add a AppLogging to an empty array', () => {
        const appLogging: IAppLogging = sampleWithRequiredData;
        expectedResult = service.addAppLoggingToCollectionIfMissing([], appLogging);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(appLogging);
      });

      it('should not add a AppLogging to an array that contains it', () => {
        const appLogging: IAppLogging = sampleWithRequiredData;
        const appLoggingCollection: IAppLogging[] = [
          {
            ...appLogging,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addAppLoggingToCollectionIfMissing(appLoggingCollection, appLogging);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AppLogging to an array that doesn't contain it", () => {
        const appLogging: IAppLogging = sampleWithRequiredData;
        const appLoggingCollection: IAppLogging[] = [sampleWithPartialData];
        expectedResult = service.addAppLoggingToCollectionIfMissing(appLoggingCollection, appLogging);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(appLogging);
      });

      it('should add only unique AppLogging to an array', () => {
        const appLoggingArray: IAppLogging[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const appLoggingCollection: IAppLogging[] = [sampleWithRequiredData];
        expectedResult = service.addAppLoggingToCollectionIfMissing(appLoggingCollection, ...appLoggingArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const appLogging: IAppLogging = sampleWithRequiredData;
        const appLogging2: IAppLogging = sampleWithPartialData;
        expectedResult = service.addAppLoggingToCollectionIfMissing([], appLogging, appLogging2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(appLogging);
        expect(expectedResult).toContain(appLogging2);
      });

      it('should accept null and undefined values', () => {
        const appLogging: IAppLogging = sampleWithRequiredData;
        expectedResult = service.addAppLoggingToCollectionIfMissing([], null, appLogging, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(appLogging);
      });

      it('should return initial array if no AppLogging is added', () => {
        const appLoggingCollection: IAppLogging[] = [sampleWithRequiredData];
        expectedResult = service.addAppLoggingToCollectionIfMissing(appLoggingCollection, undefined, null);
        expect(expectedResult).toEqual(appLoggingCollection);
      });
    });

    describe('compareAppLogging', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareAppLogging(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareAppLogging(entity1, entity2);
        const compareResult2 = service.compareAppLogging(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareAppLogging(entity1, entity2);
        const compareResult2 = service.compareAppLogging(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareAppLogging(entity1, entity2);
        const compareResult2 = service.compareAppLogging(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
