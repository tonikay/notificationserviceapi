import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAppLogging, NewAppLogging } from '../app-logging.model';

export type PartialUpdateAppLogging = Partial<IAppLogging> & Pick<IAppLogging, 'id'>;

type RestOf<T extends IAppLogging | NewAppLogging> = Omit<T, 'createdDate'> & {
  createdDate?: string | null;
};

export type RestAppLogging = RestOf<IAppLogging>;

export type NewRestAppLogging = RestOf<NewAppLogging>;

export type PartialUpdateRestAppLogging = RestOf<PartialUpdateAppLogging>;

export type EntityResponseType = HttpResponse<IAppLogging>;
export type EntityArrayResponseType = HttpResponse<IAppLogging[]>;

@Injectable({ providedIn: 'root' })
export class AppLoggingService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/app-loggings');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(appLogging: NewAppLogging): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(appLogging);
    return this.http
      .post<RestAppLogging>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(appLogging: IAppLogging): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(appLogging);
    return this.http
      .put<RestAppLogging>(`${this.resourceUrl}/${this.getAppLoggingIdentifier(appLogging)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(appLogging: PartialUpdateAppLogging): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(appLogging);
    return this.http
      .patch<RestAppLogging>(`${this.resourceUrl}/${this.getAppLoggingIdentifier(appLogging)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestAppLogging>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestAppLogging[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAppLoggingIdentifier(appLogging: Pick<IAppLogging, 'id'>): number {
    return appLogging.id;
  }

  compareAppLogging(o1: Pick<IAppLogging, 'id'> | null, o2: Pick<IAppLogging, 'id'> | null): boolean {
    return o1 && o2 ? this.getAppLoggingIdentifier(o1) === this.getAppLoggingIdentifier(o2) : o1 === o2;
  }

  addAppLoggingToCollectionIfMissing<Type extends Pick<IAppLogging, 'id'>>(
    appLoggingCollection: Type[],
    ...appLoggingsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const appLoggings: Type[] = appLoggingsToCheck.filter(isPresent);
    if (appLoggings.length > 0) {
      const appLoggingCollectionIdentifiers = appLoggingCollection.map(appLoggingItem => this.getAppLoggingIdentifier(appLoggingItem)!);
      const appLoggingsToAdd = appLoggings.filter(appLoggingItem => {
        const appLoggingIdentifier = this.getAppLoggingIdentifier(appLoggingItem);
        if (appLoggingCollectionIdentifiers.includes(appLoggingIdentifier)) {
          return false;
        }
        appLoggingCollectionIdentifiers.push(appLoggingIdentifier);
        return true;
      });
      return [...appLoggingsToAdd, ...appLoggingCollection];
    }
    return appLoggingCollection;
  }

  protected convertDateFromClient<T extends IAppLogging | NewAppLogging | PartialUpdateAppLogging>(appLogging: T): RestOf<T> {
    return {
      ...appLogging,
      createdDate: appLogging.createdDate?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restAppLogging: RestAppLogging): IAppLogging {
    return {
      ...restAppLogging,
      createdDate: restAppLogging.createdDate ? dayjs(restAppLogging.createdDate) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestAppLogging>): HttpResponse<IAppLogging> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestAppLogging[]>): HttpResponse<IAppLogging[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
