import dayjs from 'dayjs/esm';
import { IApplication } from 'app/entities/application/application.model';
import { ActionType } from 'app/entities/enumerations/action-type.model';

export interface IAppLogging {
  id: number;
  comments?: string | null;
  actionType?: ActionType | null;
  createdDate?: dayjs.Dayjs | null;
  application?: Pick<IApplication, 'id'> | null;
}

export type NewAppLogging = Omit<IAppLogging, 'id'> & { id: null };
