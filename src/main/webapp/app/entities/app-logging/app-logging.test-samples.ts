import dayjs from 'dayjs/esm';

import { ActionType } from 'app/entities/enumerations/action-type.model';

import { IAppLogging, NewAppLogging } from './app-logging.model';

export const sampleWithRequiredData: IAppLogging = {
  id: 9074,
};

export const sampleWithPartialData: IAppLogging = {
  id: 40751,
  actionType: ActionType['PUSH_NOTIFICATION'],
};

export const sampleWithFullData: IAppLogging = {
  id: 76624,
  comments: 'Chair',
  actionType: ActionType['PUSH_NOTIFICATION'],
  createdDate: dayjs('2022-12-08T12:24'),
};

export const sampleWithNewData: NewAppLogging = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
