import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { SMSHostConfigComponent } from './list/sms-host-config.component';
import { SMSHostConfigDetailComponent } from './detail/sms-host-config-detail.component';
import { SMSHostConfigUpdateComponent } from './update/sms-host-config-update.component';
import { SMSHostConfigDeleteDialogComponent } from './delete/sms-host-config-delete-dialog.component';
import { SMSHostConfigRoutingModule } from './route/sms-host-config-routing.module';

@NgModule({
  imports: [SharedModule, SMSHostConfigRoutingModule],
  declarations: [SMSHostConfigComponent, SMSHostConfigDetailComponent, SMSHostConfigUpdateComponent, SMSHostConfigDeleteDialogComponent],
})
export class SMSHostConfigModule {}
