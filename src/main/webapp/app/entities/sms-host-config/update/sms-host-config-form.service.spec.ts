import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../sms-host-config.test-samples';

import { SMSHostConfigFormService } from './sms-host-config-form.service';

describe('SMSHostConfig Form Service', () => {
  let service: SMSHostConfigFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SMSHostConfigFormService);
  });

  describe('Service methods', () => {
    describe('createSMSHostConfigFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createSMSHostConfigFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            hostEndpoint: expect.any(Object),
            token: expect.any(Object),
            senderNumber: expect.any(Object),
            createdDate: expect.any(Object),
            createdBy: expect.any(Object),
            updatedDate: expect.any(Object),
            updatedBy: expect.any(Object),
            application: expect.any(Object),
          })
        );
      });

      it('passing ISMSHostConfig should create a new form with FormGroup', () => {
        const formGroup = service.createSMSHostConfigFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            hostEndpoint: expect.any(Object),
            token: expect.any(Object),
            senderNumber: expect.any(Object),
            createdDate: expect.any(Object),
            createdBy: expect.any(Object),
            updatedDate: expect.any(Object),
            updatedBy: expect.any(Object),
            application: expect.any(Object),
          })
        );
      });
    });

    describe('getSMSHostConfig', () => {
      it('should return NewSMSHostConfig for default SMSHostConfig initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createSMSHostConfigFormGroup(sampleWithNewData);

        const sMSHostConfig = service.getSMSHostConfig(formGroup) as any;

        expect(sMSHostConfig).toMatchObject(sampleWithNewData);
      });

      it('should return NewSMSHostConfig for empty SMSHostConfig initial value', () => {
        const formGroup = service.createSMSHostConfigFormGroup();

        const sMSHostConfig = service.getSMSHostConfig(formGroup) as any;

        expect(sMSHostConfig).toMatchObject({});
      });

      it('should return ISMSHostConfig', () => {
        const formGroup = service.createSMSHostConfigFormGroup(sampleWithRequiredData);

        const sMSHostConfig = service.getSMSHostConfig(formGroup) as any;

        expect(sMSHostConfig).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ISMSHostConfig should not enable id FormControl', () => {
        const formGroup = service.createSMSHostConfigFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewSMSHostConfig should disable id FormControl', () => {
        const formGroup = service.createSMSHostConfigFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
