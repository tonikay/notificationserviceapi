import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ISMSHostConfig, NewSMSHostConfig } from '../sms-host-config.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ISMSHostConfig for edit and NewSMSHostConfigFormGroupInput for create.
 */
type SMSHostConfigFormGroupInput = ISMSHostConfig | PartialWithRequiredKeyOf<NewSMSHostConfig>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends ISMSHostConfig | NewSMSHostConfig> = Omit<T, 'createdDate' | 'updatedDate'> & {
  createdDate?: string | null;
  updatedDate?: string | null;
};

type SMSHostConfigFormRawValue = FormValueOf<ISMSHostConfig>;

type NewSMSHostConfigFormRawValue = FormValueOf<NewSMSHostConfig>;

type SMSHostConfigFormDefaults = Pick<NewSMSHostConfig, 'id' | 'createdDate' | 'updatedDate'>;

type SMSHostConfigFormGroupContent = {
  id: FormControl<SMSHostConfigFormRawValue['id'] | NewSMSHostConfig['id']>;
  hostEndpoint: FormControl<SMSHostConfigFormRawValue['hostEndpoint']>;
  token: FormControl<SMSHostConfigFormRawValue['token']>;
  senderNumber: FormControl<SMSHostConfigFormRawValue['senderNumber']>;
  createdDate: FormControl<SMSHostConfigFormRawValue['createdDate']>;
  createdBy: FormControl<SMSHostConfigFormRawValue['createdBy']>;
  updatedDate: FormControl<SMSHostConfigFormRawValue['updatedDate']>;
  updatedBy: FormControl<SMSHostConfigFormRawValue['updatedBy']>;
  application: FormControl<SMSHostConfigFormRawValue['application']>;
};

export type SMSHostConfigFormGroup = FormGroup<SMSHostConfigFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class SMSHostConfigFormService {
  createSMSHostConfigFormGroup(sMSHostConfig: SMSHostConfigFormGroupInput = { id: null }): SMSHostConfigFormGroup {
    const sMSHostConfigRawValue = this.convertSMSHostConfigToSMSHostConfigRawValue({
      ...this.getFormDefaults(),
      ...sMSHostConfig,
    });
    return new FormGroup<SMSHostConfigFormGroupContent>({
      id: new FormControl(
        { value: sMSHostConfigRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      hostEndpoint: new FormControl(sMSHostConfigRawValue.hostEndpoint, {
        validators: [Validators.required],
      }),
      token: new FormControl(sMSHostConfigRawValue.token, {
        validators: [Validators.required],
      }),
      senderNumber: new FormControl(sMSHostConfigRawValue.senderNumber, {
        validators: [Validators.required],
      }),
      createdDate: new FormControl(sMSHostConfigRawValue.createdDate),
      createdBy: new FormControl(sMSHostConfigRawValue.createdBy),
      updatedDate: new FormControl(sMSHostConfigRawValue.updatedDate),
      updatedBy: new FormControl(sMSHostConfigRawValue.updatedBy),
      application: new FormControl(sMSHostConfigRawValue.application),
    });
  }

  getSMSHostConfig(form: SMSHostConfigFormGroup): ISMSHostConfig | NewSMSHostConfig {
    return this.convertSMSHostConfigRawValueToSMSHostConfig(form.getRawValue() as SMSHostConfigFormRawValue | NewSMSHostConfigFormRawValue);
  }

  resetForm(form: SMSHostConfigFormGroup, sMSHostConfig: SMSHostConfigFormGroupInput): void {
    const sMSHostConfigRawValue = this.convertSMSHostConfigToSMSHostConfigRawValue({ ...this.getFormDefaults(), ...sMSHostConfig });
    form.reset(
      {
        ...sMSHostConfigRawValue,
        id: { value: sMSHostConfigRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): SMSHostConfigFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      createdDate: currentTime,
      updatedDate: currentTime,
    };
  }

  private convertSMSHostConfigRawValueToSMSHostConfig(
    rawSMSHostConfig: SMSHostConfigFormRawValue | NewSMSHostConfigFormRawValue
  ): ISMSHostConfig | NewSMSHostConfig {
    return {
      ...rawSMSHostConfig,
      createdDate: dayjs(rawSMSHostConfig.createdDate, DATE_TIME_FORMAT),
      updatedDate: dayjs(rawSMSHostConfig.updatedDate, DATE_TIME_FORMAT),
    };
  }

  private convertSMSHostConfigToSMSHostConfigRawValue(
    sMSHostConfig: ISMSHostConfig | (Partial<NewSMSHostConfig> & SMSHostConfigFormDefaults)
  ): SMSHostConfigFormRawValue | PartialWithRequiredKeyOf<NewSMSHostConfigFormRawValue> {
    return {
      ...sMSHostConfig,
      createdDate: sMSHostConfig.createdDate ? sMSHostConfig.createdDate.format(DATE_TIME_FORMAT) : undefined,
      updatedDate: sMSHostConfig.updatedDate ? sMSHostConfig.updatedDate.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
