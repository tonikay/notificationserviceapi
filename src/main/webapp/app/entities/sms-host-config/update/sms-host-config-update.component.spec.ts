import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { SMSHostConfigFormService } from './sms-host-config-form.service';
import { SMSHostConfigService } from '../service/sms-host-config.service';
import { ISMSHostConfig } from '../sms-host-config.model';
import { IApplication } from 'app/entities/application/application.model';
import { ApplicationService } from 'app/entities/application/service/application.service';

import { SMSHostConfigUpdateComponent } from './sms-host-config-update.component';

describe('SMSHostConfig Management Update Component', () => {
  let comp: SMSHostConfigUpdateComponent;
  let fixture: ComponentFixture<SMSHostConfigUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let sMSHostConfigFormService: SMSHostConfigFormService;
  let sMSHostConfigService: SMSHostConfigService;
  let applicationService: ApplicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [SMSHostConfigUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SMSHostConfigUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SMSHostConfigUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    sMSHostConfigFormService = TestBed.inject(SMSHostConfigFormService);
    sMSHostConfigService = TestBed.inject(SMSHostConfigService);
    applicationService = TestBed.inject(ApplicationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Application query and add missing value', () => {
      const sMSHostConfig: ISMSHostConfig = { id: 456 };
      const application: IApplication = { id: 50858 };
      sMSHostConfig.application = application;

      const applicationCollection: IApplication[] = [{ id: 91572 }];
      jest.spyOn(applicationService, 'query').mockReturnValue(of(new HttpResponse({ body: applicationCollection })));
      const additionalApplications = [application];
      const expectedCollection: IApplication[] = [...additionalApplications, ...applicationCollection];
      jest.spyOn(applicationService, 'addApplicationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ sMSHostConfig });
      comp.ngOnInit();

      expect(applicationService.query).toHaveBeenCalled();
      expect(applicationService.addApplicationToCollectionIfMissing).toHaveBeenCalledWith(
        applicationCollection,
        ...additionalApplications.map(expect.objectContaining)
      );
      expect(comp.applicationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const sMSHostConfig: ISMSHostConfig = { id: 456 };
      const application: IApplication = { id: 28918 };
      sMSHostConfig.application = application;

      activatedRoute.data = of({ sMSHostConfig });
      comp.ngOnInit();

      expect(comp.applicationsSharedCollection).toContain(application);
      expect(comp.sMSHostConfig).toEqual(sMSHostConfig);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISMSHostConfig>>();
      const sMSHostConfig = { id: 123 };
      jest.spyOn(sMSHostConfigFormService, 'getSMSHostConfig').mockReturnValue(sMSHostConfig);
      jest.spyOn(sMSHostConfigService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sMSHostConfig });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: sMSHostConfig }));
      saveSubject.complete();

      // THEN
      expect(sMSHostConfigFormService.getSMSHostConfig).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(sMSHostConfigService.update).toHaveBeenCalledWith(expect.objectContaining(sMSHostConfig));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISMSHostConfig>>();
      const sMSHostConfig = { id: 123 };
      jest.spyOn(sMSHostConfigFormService, 'getSMSHostConfig').mockReturnValue({ id: null });
      jest.spyOn(sMSHostConfigService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sMSHostConfig: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: sMSHostConfig }));
      saveSubject.complete();

      // THEN
      expect(sMSHostConfigFormService.getSMSHostConfig).toHaveBeenCalled();
      expect(sMSHostConfigService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISMSHostConfig>>();
      const sMSHostConfig = { id: 123 };
      jest.spyOn(sMSHostConfigService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sMSHostConfig });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(sMSHostConfigService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareApplication', () => {
      it('Should forward to applicationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(applicationService, 'compareApplication');
        comp.compareApplication(entity, entity2);
        expect(applicationService.compareApplication).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
