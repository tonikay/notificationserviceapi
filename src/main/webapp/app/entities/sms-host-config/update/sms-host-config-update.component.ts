import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { SMSHostConfigFormService, SMSHostConfigFormGroup } from './sms-host-config-form.service';
import { ISMSHostConfig } from '../sms-host-config.model';
import { SMSHostConfigService } from '../service/sms-host-config.service';
import { IApplication } from 'app/entities/application/application.model';
import { ApplicationService } from 'app/entities/application/service/application.service';

@Component({
  selector: 'jhi-sms-host-config-update',
  templateUrl: './sms-host-config-update.component.html',
})
export class SMSHostConfigUpdateComponent implements OnInit {
  isSaving = false;
  sMSHostConfig: ISMSHostConfig | null = null;

  applicationsSharedCollection: IApplication[] = [];

  editForm: SMSHostConfigFormGroup = this.sMSHostConfigFormService.createSMSHostConfigFormGroup();

  constructor(
    protected sMSHostConfigService: SMSHostConfigService,
    protected sMSHostConfigFormService: SMSHostConfigFormService,
    protected applicationService: ApplicationService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareApplication = (o1: IApplication | null, o2: IApplication | null): boolean => this.applicationService.compareApplication(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sMSHostConfig }) => {
      this.sMSHostConfig = sMSHostConfig;
      if (sMSHostConfig) {
        this.updateForm(sMSHostConfig);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sMSHostConfig = this.sMSHostConfigFormService.getSMSHostConfig(this.editForm);
    if (sMSHostConfig.id !== null) {
      this.subscribeToSaveResponse(this.sMSHostConfigService.update(sMSHostConfig));
    } else {
      this.subscribeToSaveResponse(this.sMSHostConfigService.create(sMSHostConfig));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISMSHostConfig>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(sMSHostConfig: ISMSHostConfig): void {
    this.sMSHostConfig = sMSHostConfig;
    this.sMSHostConfigFormService.resetForm(this.editForm, sMSHostConfig);

    this.applicationsSharedCollection = this.applicationService.addApplicationToCollectionIfMissing<IApplication>(
      this.applicationsSharedCollection,
      sMSHostConfig.application
    );
  }

  protected loadRelationshipsOptions(): void {
    this.applicationService
      .query()
      .pipe(map((res: HttpResponse<IApplication[]>) => res.body ?? []))
      .pipe(
        map((applications: IApplication[]) =>
          this.applicationService.addApplicationToCollectionIfMissing<IApplication>(applications, this.sMSHostConfig?.application)
        )
      )
      .subscribe((applications: IApplication[]) => (this.applicationsSharedCollection = applications));
  }
}
