import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ISMSHostConfig } from '../sms-host-config.model';
import { SMSHostConfigService } from '../service/sms-host-config.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './sms-host-config-delete-dialog.component.html',
})
export class SMSHostConfigDeleteDialogComponent {
  sMSHostConfig?: ISMSHostConfig;

  constructor(protected sMSHostConfigService: SMSHostConfigService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.sMSHostConfigService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
