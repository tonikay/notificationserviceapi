import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISMSHostConfig } from '../sms-host-config.model';

@Component({
  selector: 'jhi-sms-host-config-detail',
  templateUrl: './sms-host-config-detail.component.html',
})
export class SMSHostConfigDetailComponent implements OnInit {
  sMSHostConfig: ISMSHostConfig | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sMSHostConfig }) => {
      this.sMSHostConfig = sMSHostConfig;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
