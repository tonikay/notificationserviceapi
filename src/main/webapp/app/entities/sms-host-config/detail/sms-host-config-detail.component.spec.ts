import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SMSHostConfigDetailComponent } from './sms-host-config-detail.component';

describe('SMSHostConfig Management Detail Component', () => {
  let comp: SMSHostConfigDetailComponent;
  let fixture: ComponentFixture<SMSHostConfigDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SMSHostConfigDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ sMSHostConfig: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(SMSHostConfigDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(SMSHostConfigDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load sMSHostConfig on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.sMSHostConfig).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
