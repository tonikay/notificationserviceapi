import dayjs from 'dayjs/esm';

import { ISMSHostConfig, NewSMSHostConfig } from './sms-host-config.model';

export const sampleWithRequiredData: ISMSHostConfig = {
  id: 36872,
  hostEndpoint: 'Sausages',
  token: 'optical Implemented',
  senderNumber: 77402,
};

export const sampleWithPartialData: ISMSHostConfig = {
  id: 1969,
  hostEndpoint: 'Dobra paradigms',
  token: 'parsing',
  senderNumber: 56773,
  createdDate: dayjs('2022-12-08T05:52'),
};

export const sampleWithFullData: ISMSHostConfig = {
  id: 68457,
  hostEndpoint: 'lime functionalities Bolivia',
  token: 'visionary Skyway optical',
  senderNumber: 86443,
  createdDate: dayjs('2022-12-07T22:05'),
  createdBy: 'withdrawal payment Awesome',
  updatedDate: dayjs('2022-12-08T06:13'),
  updatedBy: 'Austria',
};

export const sampleWithNewData: NewSMSHostConfig = {
  hostEndpoint: 'Home auxiliary Rue',
  token: 'Intelligent',
  senderNumber: 2006,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
