import dayjs from 'dayjs/esm';
import { IApplication } from 'app/entities/application/application.model';

export interface ISMSHostConfig {
  id: number;
  hostEndpoint?: string | null;
  token?: string | null;
  senderNumber?: number | null;
  createdDate?: dayjs.Dayjs | null;
  createdBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  application?: Pick<IApplication, 'id'> | null;
}

export type NewSMSHostConfig = Omit<ISMSHostConfig, 'id'> & { id: null };
