import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ISMSHostConfig } from '../sms-host-config.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../sms-host-config.test-samples';

import { SMSHostConfigService, RestSMSHostConfig } from './sms-host-config.service';

const requireRestSample: RestSMSHostConfig = {
  ...sampleWithRequiredData,
  createdDate: sampleWithRequiredData.createdDate?.toJSON(),
  updatedDate: sampleWithRequiredData.updatedDate?.toJSON(),
};

describe('SMSHostConfig Service', () => {
  let service: SMSHostConfigService;
  let httpMock: HttpTestingController;
  let expectedResult: ISMSHostConfig | ISMSHostConfig[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SMSHostConfigService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a SMSHostConfig', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const sMSHostConfig = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(sMSHostConfig).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a SMSHostConfig', () => {
      const sMSHostConfig = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(sMSHostConfig).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a SMSHostConfig', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of SMSHostConfig', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a SMSHostConfig', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addSMSHostConfigToCollectionIfMissing', () => {
      it('should add a SMSHostConfig to an empty array', () => {
        const sMSHostConfig: ISMSHostConfig = sampleWithRequiredData;
        expectedResult = service.addSMSHostConfigToCollectionIfMissing([], sMSHostConfig);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sMSHostConfig);
      });

      it('should not add a SMSHostConfig to an array that contains it', () => {
        const sMSHostConfig: ISMSHostConfig = sampleWithRequiredData;
        const sMSHostConfigCollection: ISMSHostConfig[] = [
          {
            ...sMSHostConfig,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addSMSHostConfigToCollectionIfMissing(sMSHostConfigCollection, sMSHostConfig);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a SMSHostConfig to an array that doesn't contain it", () => {
        const sMSHostConfig: ISMSHostConfig = sampleWithRequiredData;
        const sMSHostConfigCollection: ISMSHostConfig[] = [sampleWithPartialData];
        expectedResult = service.addSMSHostConfigToCollectionIfMissing(sMSHostConfigCollection, sMSHostConfig);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sMSHostConfig);
      });

      it('should add only unique SMSHostConfig to an array', () => {
        const sMSHostConfigArray: ISMSHostConfig[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const sMSHostConfigCollection: ISMSHostConfig[] = [sampleWithRequiredData];
        expectedResult = service.addSMSHostConfigToCollectionIfMissing(sMSHostConfigCollection, ...sMSHostConfigArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const sMSHostConfig: ISMSHostConfig = sampleWithRequiredData;
        const sMSHostConfig2: ISMSHostConfig = sampleWithPartialData;
        expectedResult = service.addSMSHostConfigToCollectionIfMissing([], sMSHostConfig, sMSHostConfig2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sMSHostConfig);
        expect(expectedResult).toContain(sMSHostConfig2);
      });

      it('should accept null and undefined values', () => {
        const sMSHostConfig: ISMSHostConfig = sampleWithRequiredData;
        expectedResult = service.addSMSHostConfigToCollectionIfMissing([], null, sMSHostConfig, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sMSHostConfig);
      });

      it('should return initial array if no SMSHostConfig is added', () => {
        const sMSHostConfigCollection: ISMSHostConfig[] = [sampleWithRequiredData];
        expectedResult = service.addSMSHostConfigToCollectionIfMissing(sMSHostConfigCollection, undefined, null);
        expect(expectedResult).toEqual(sMSHostConfigCollection);
      });
    });

    describe('compareSMSHostConfig', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareSMSHostConfig(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareSMSHostConfig(entity1, entity2);
        const compareResult2 = service.compareSMSHostConfig(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareSMSHostConfig(entity1, entity2);
        const compareResult2 = service.compareSMSHostConfig(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareSMSHostConfig(entity1, entity2);
        const compareResult2 = service.compareSMSHostConfig(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
