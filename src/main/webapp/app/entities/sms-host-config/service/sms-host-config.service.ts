import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISMSHostConfig, NewSMSHostConfig } from '../sms-host-config.model';

export type PartialUpdateSMSHostConfig = Partial<ISMSHostConfig> & Pick<ISMSHostConfig, 'id'>;

type RestOf<T extends ISMSHostConfig | NewSMSHostConfig> = Omit<T, 'createdDate' | 'updatedDate'> & {
  createdDate?: string | null;
  updatedDate?: string | null;
};

export type RestSMSHostConfig = RestOf<ISMSHostConfig>;

export type NewRestSMSHostConfig = RestOf<NewSMSHostConfig>;

export type PartialUpdateRestSMSHostConfig = RestOf<PartialUpdateSMSHostConfig>;

export type EntityResponseType = HttpResponse<ISMSHostConfig>;
export type EntityArrayResponseType = HttpResponse<ISMSHostConfig[]>;

@Injectable({ providedIn: 'root' })
export class SMSHostConfigService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/sms-host-configs');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(sMSHostConfig: NewSMSHostConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sMSHostConfig);
    return this.http
      .post<RestSMSHostConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(sMSHostConfig: ISMSHostConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sMSHostConfig);
    return this.http
      .put<RestSMSHostConfig>(`${this.resourceUrl}/${this.getSMSHostConfigIdentifier(sMSHostConfig)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(sMSHostConfig: PartialUpdateSMSHostConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sMSHostConfig);
    return this.http
      .patch<RestSMSHostConfig>(`${this.resourceUrl}/${this.getSMSHostConfigIdentifier(sMSHostConfig)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestSMSHostConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestSMSHostConfig[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getSMSHostConfigIdentifier(sMSHostConfig: Pick<ISMSHostConfig, 'id'>): number {
    return sMSHostConfig.id;
  }

  compareSMSHostConfig(o1: Pick<ISMSHostConfig, 'id'> | null, o2: Pick<ISMSHostConfig, 'id'> | null): boolean {
    return o1 && o2 ? this.getSMSHostConfigIdentifier(o1) === this.getSMSHostConfigIdentifier(o2) : o1 === o2;
  }

  addSMSHostConfigToCollectionIfMissing<Type extends Pick<ISMSHostConfig, 'id'>>(
    sMSHostConfigCollection: Type[],
    ...sMSHostConfigsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const sMSHostConfigs: Type[] = sMSHostConfigsToCheck.filter(isPresent);
    if (sMSHostConfigs.length > 0) {
      const sMSHostConfigCollectionIdentifiers = sMSHostConfigCollection.map(
        sMSHostConfigItem => this.getSMSHostConfigIdentifier(sMSHostConfigItem)!
      );
      const sMSHostConfigsToAdd = sMSHostConfigs.filter(sMSHostConfigItem => {
        const sMSHostConfigIdentifier = this.getSMSHostConfigIdentifier(sMSHostConfigItem);
        if (sMSHostConfigCollectionIdentifiers.includes(sMSHostConfigIdentifier)) {
          return false;
        }
        sMSHostConfigCollectionIdentifiers.push(sMSHostConfigIdentifier);
        return true;
      });
      return [...sMSHostConfigsToAdd, ...sMSHostConfigCollection];
    }
    return sMSHostConfigCollection;
  }

  protected convertDateFromClient<T extends ISMSHostConfig | NewSMSHostConfig | PartialUpdateSMSHostConfig>(sMSHostConfig: T): RestOf<T> {
    return {
      ...sMSHostConfig,
      createdDate: sMSHostConfig.createdDate?.toJSON() ?? null,
      updatedDate: sMSHostConfig.updatedDate?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restSMSHostConfig: RestSMSHostConfig): ISMSHostConfig {
    return {
      ...restSMSHostConfig,
      createdDate: restSMSHostConfig.createdDate ? dayjs(restSMSHostConfig.createdDate) : undefined,
      updatedDate: restSMSHostConfig.updatedDate ? dayjs(restSMSHostConfig.updatedDate) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestSMSHostConfig>): HttpResponse<ISMSHostConfig> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestSMSHostConfig[]>): HttpResponse<ISMSHostConfig[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
