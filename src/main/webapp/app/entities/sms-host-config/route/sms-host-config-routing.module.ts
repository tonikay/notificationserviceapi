import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { SMSHostConfigComponent } from '../list/sms-host-config.component';
import { SMSHostConfigDetailComponent } from '../detail/sms-host-config-detail.component';
import { SMSHostConfigUpdateComponent } from '../update/sms-host-config-update.component';
import { SMSHostConfigRoutingResolveService } from './sms-host-config-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const sMSHostConfigRoute: Routes = [
  {
    path: '',
    component: SMSHostConfigComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SMSHostConfigDetailComponent,
    resolve: {
      sMSHostConfig: SMSHostConfigRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SMSHostConfigUpdateComponent,
    resolve: {
      sMSHostConfig: SMSHostConfigRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SMSHostConfigUpdateComponent,
    resolve: {
      sMSHostConfig: SMSHostConfigRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(sMSHostConfigRoute)],
  exports: [RouterModule],
})
export class SMSHostConfigRoutingModule {}
