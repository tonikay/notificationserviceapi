import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISMSHostConfig } from '../sms-host-config.model';
import { SMSHostConfigService } from '../service/sms-host-config.service';

@Injectable({ providedIn: 'root' })
export class SMSHostConfigRoutingResolveService implements Resolve<ISMSHostConfig | null> {
  constructor(protected service: SMSHostConfigService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISMSHostConfig | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((sMSHostConfig: HttpResponse<ISMSHostConfig>) => {
          if (sMSHostConfig.body) {
            return of(sMSHostConfig.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
