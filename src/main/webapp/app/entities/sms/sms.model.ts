export interface ISMS {
  id: number;
  recipent?: string | null;
  message?: string | null;
}

export type NewSMS = Omit<ISMS, 'id'> & { id: null };
