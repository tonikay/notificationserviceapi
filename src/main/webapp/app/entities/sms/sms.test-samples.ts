import { ISMS, NewSMS } from './sms.model';

export const sampleWithRequiredData: ISMS = {
  id: 14680,
};

export const sampleWithPartialData: ISMS = {
  id: 99933,
};

export const sampleWithFullData: ISMS = {
  id: 18170,
  recipent: 'Generic invoice Avon',
  message: 'Bedfordshire Electronics',
};

export const sampleWithNewData: NewSMS = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
