import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISMS } from '../sms.model';

@Component({
  selector: 'jhi-sms-detail',
  templateUrl: './sms-detail.component.html',
})
export class SMSDetailComponent implements OnInit {
  sMS: ISMS | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sMS }) => {
      this.sMS = sMS;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
