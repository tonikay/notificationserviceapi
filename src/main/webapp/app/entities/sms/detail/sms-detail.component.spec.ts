import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SMSDetailComponent } from './sms-detail.component';

describe('SMS Management Detail Component', () => {
  let comp: SMSDetailComponent;
  let fixture: ComponentFixture<SMSDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SMSDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ sMS: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(SMSDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(SMSDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load sMS on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.sMS).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
