import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISMS, NewSMS } from '../sms.model';

export type PartialUpdateSMS = Partial<ISMS> & Pick<ISMS, 'id'>;

export type EntityResponseType = HttpResponse<ISMS>;
export type EntityArrayResponseType = HttpResponse<ISMS[]>;

@Injectable({ providedIn: 'root' })
export class SMSService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/sms');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(sMS: NewSMS): Observable<EntityResponseType> {
    return this.http.post<ISMS>(this.resourceUrl, sMS, { observe: 'response' });
  }

  update(sMS: ISMS): Observable<EntityResponseType> {
    return this.http.put<ISMS>(`${this.resourceUrl}/${this.getSMSIdentifier(sMS)}`, sMS, { observe: 'response' });
  }

  partialUpdate(sMS: PartialUpdateSMS): Observable<EntityResponseType> {
    return this.http.patch<ISMS>(`${this.resourceUrl}/${this.getSMSIdentifier(sMS)}`, sMS, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISMS>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISMS[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getSMSIdentifier(sMS: Pick<ISMS, 'id'>): number {
    return sMS.id;
  }

  compareSMS(o1: Pick<ISMS, 'id'> | null, o2: Pick<ISMS, 'id'> | null): boolean {
    return o1 && o2 ? this.getSMSIdentifier(o1) === this.getSMSIdentifier(o2) : o1 === o2;
  }

  addSMSToCollectionIfMissing<Type extends Pick<ISMS, 'id'>>(sMSCollection: Type[], ...sMSToCheck: (Type | null | undefined)[]): Type[] {
    const sMS: Type[] = sMSToCheck.filter(isPresent);
    if (sMS.length > 0) {
      const sMSCollectionIdentifiers = sMSCollection.map(sMSItem => this.getSMSIdentifier(sMSItem)!);
      const sMSToAdd = sMS.filter(sMSItem => {
        const sMSIdentifier = this.getSMSIdentifier(sMSItem);
        if (sMSCollectionIdentifiers.includes(sMSIdentifier)) {
          return false;
        }
        sMSCollectionIdentifiers.push(sMSIdentifier);
        return true;
      });
      return [...sMSToAdd, ...sMSCollection];
    }
    return sMSCollection;
  }
}
