import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ISMS } from '../sms.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../sms.test-samples';

import { SMSService } from './sms.service';

const requireRestSample: ISMS = {
  ...sampleWithRequiredData,
};

describe('SMS Service', () => {
  let service: SMSService;
  let httpMock: HttpTestingController;
  let expectedResult: ISMS | ISMS[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SMSService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a SMS', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const sMS = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(sMS).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a SMS', () => {
      const sMS = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(sMS).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a SMS', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of SMS', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a SMS', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addSMSToCollectionIfMissing', () => {
      it('should add a SMS to an empty array', () => {
        const sMS: ISMS = sampleWithRequiredData;
        expectedResult = service.addSMSToCollectionIfMissing([], sMS);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sMS);
      });

      it('should not add a SMS to an array that contains it', () => {
        const sMS: ISMS = sampleWithRequiredData;
        const sMSCollection: ISMS[] = [
          {
            ...sMS,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addSMSToCollectionIfMissing(sMSCollection, sMS);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a SMS to an array that doesn't contain it", () => {
        const sMS: ISMS = sampleWithRequiredData;
        const sMSCollection: ISMS[] = [sampleWithPartialData];
        expectedResult = service.addSMSToCollectionIfMissing(sMSCollection, sMS);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sMS);
      });

      it('should add only unique SMS to an array', () => {
        const sMSArray: ISMS[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const sMSCollection: ISMS[] = [sampleWithRequiredData];
        expectedResult = service.addSMSToCollectionIfMissing(sMSCollection, ...sMSArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const sMS: ISMS = sampleWithRequiredData;
        const sMS2: ISMS = sampleWithPartialData;
        expectedResult = service.addSMSToCollectionIfMissing([], sMS, sMS2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sMS);
        expect(expectedResult).toContain(sMS2);
      });

      it('should accept null and undefined values', () => {
        const sMS: ISMS = sampleWithRequiredData;
        expectedResult = service.addSMSToCollectionIfMissing([], null, sMS, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sMS);
      });

      it('should return initial array if no SMS is added', () => {
        const sMSCollection: ISMS[] = [sampleWithRequiredData];
        expectedResult = service.addSMSToCollectionIfMissing(sMSCollection, undefined, null);
        expect(expectedResult).toEqual(sMSCollection);
      });
    });

    describe('compareSMS', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareSMS(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareSMS(entity1, entity2);
        const compareResult2 = service.compareSMS(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareSMS(entity1, entity2);
        const compareResult2 = service.compareSMS(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareSMS(entity1, entity2);
        const compareResult2 = service.compareSMS(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
