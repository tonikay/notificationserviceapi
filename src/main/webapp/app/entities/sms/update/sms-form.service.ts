import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ISMS, NewSMS } from '../sms.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ISMS for edit and NewSMSFormGroupInput for create.
 */
type SMSFormGroupInput = ISMS | PartialWithRequiredKeyOf<NewSMS>;

type SMSFormDefaults = Pick<NewSMS, 'id'>;

type SMSFormGroupContent = {
  id: FormControl<ISMS['id'] | NewSMS['id']>;
  recipent: FormControl<ISMS['recipent']>;
  message: FormControl<ISMS['message']>;
};

export type SMSFormGroup = FormGroup<SMSFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class SMSFormService {
  createSMSFormGroup(sMS: SMSFormGroupInput = { id: null }): SMSFormGroup {
    const sMSRawValue = {
      ...this.getFormDefaults(),
      ...sMS,
    };
    return new FormGroup<SMSFormGroupContent>({
      id: new FormControl(
        { value: sMSRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      recipent: new FormControl(sMSRawValue.recipent),
      message: new FormControl(sMSRawValue.message),
    });
  }

  getSMS(form: SMSFormGroup): ISMS | NewSMS {
    return form.getRawValue() as ISMS | NewSMS;
  }

  resetForm(form: SMSFormGroup, sMS: SMSFormGroupInput): void {
    const sMSRawValue = { ...this.getFormDefaults(), ...sMS };
    form.reset(
      {
        ...sMSRawValue,
        id: { value: sMSRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): SMSFormDefaults {
    return {
      id: null,
    };
  }
}
