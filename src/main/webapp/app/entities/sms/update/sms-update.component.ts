import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { SMSFormService, SMSFormGroup } from './sms-form.service';
import { ISMS } from '../sms.model';
import { SMSService } from '../service/sms.service';

@Component({
  selector: 'jhi-sms-update',
  templateUrl: './sms-update.component.html',
})
export class SMSUpdateComponent implements OnInit {
  isSaving = false;
  sMS: ISMS | null = null;

  editForm: SMSFormGroup = this.sMSFormService.createSMSFormGroup();

  constructor(protected sMSService: SMSService, protected sMSFormService: SMSFormService, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sMS }) => {
      this.sMS = sMS;
      if (sMS) {
        this.updateForm(sMS);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sMS = this.sMSFormService.getSMS(this.editForm);
    if (sMS.id !== null) {
      this.subscribeToSaveResponse(this.sMSService.update(sMS));
    } else {
      this.subscribeToSaveResponse(this.sMSService.create(sMS));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISMS>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(sMS: ISMS): void {
    this.sMS = sMS;
    this.sMSFormService.resetForm(this.editForm, sMS);
  }
}
