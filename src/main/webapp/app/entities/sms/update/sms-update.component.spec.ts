import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { SMSFormService } from './sms-form.service';
import { SMSService } from '../service/sms.service';
import { ISMS } from '../sms.model';

import { SMSUpdateComponent } from './sms-update.component';

describe('SMS Management Update Component', () => {
  let comp: SMSUpdateComponent;
  let fixture: ComponentFixture<SMSUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let sMSFormService: SMSFormService;
  let sMSService: SMSService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [SMSUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SMSUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SMSUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    sMSFormService = TestBed.inject(SMSFormService);
    sMSService = TestBed.inject(SMSService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const sMS: ISMS = { id: 456 };

      activatedRoute.data = of({ sMS });
      comp.ngOnInit();

      expect(comp.sMS).toEqual(sMS);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISMS>>();
      const sMS = { id: 123 };
      jest.spyOn(sMSFormService, 'getSMS').mockReturnValue(sMS);
      jest.spyOn(sMSService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sMS });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: sMS }));
      saveSubject.complete();

      // THEN
      expect(sMSFormService.getSMS).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(sMSService.update).toHaveBeenCalledWith(expect.objectContaining(sMS));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISMS>>();
      const sMS = { id: 123 };
      jest.spyOn(sMSFormService, 'getSMS').mockReturnValue({ id: null });
      jest.spyOn(sMSService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sMS: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: sMS }));
      saveSubject.complete();

      // THEN
      expect(sMSFormService.getSMS).toHaveBeenCalled();
      expect(sMSService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISMS>>();
      const sMS = { id: 123 };
      jest.spyOn(sMSService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sMS });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(sMSService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
