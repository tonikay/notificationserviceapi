import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../sms.test-samples';

import { SMSFormService } from './sms-form.service';

describe('SMS Form Service', () => {
  let service: SMSFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SMSFormService);
  });

  describe('Service methods', () => {
    describe('createSMSFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createSMSFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            recipent: expect.any(Object),
            message: expect.any(Object),
          })
        );
      });

      it('passing ISMS should create a new form with FormGroup', () => {
        const formGroup = service.createSMSFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            recipent: expect.any(Object),
            message: expect.any(Object),
          })
        );
      });
    });

    describe('getSMS', () => {
      it('should return NewSMS for default SMS initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createSMSFormGroup(sampleWithNewData);

        const sMS = service.getSMS(formGroup) as any;

        expect(sMS).toMatchObject(sampleWithNewData);
      });

      it('should return NewSMS for empty SMS initial value', () => {
        const formGroup = service.createSMSFormGroup();

        const sMS = service.getSMS(formGroup) as any;

        expect(sMS).toMatchObject({});
      });

      it('should return ISMS', () => {
        const formGroup = service.createSMSFormGroup(sampleWithRequiredData);

        const sMS = service.getSMS(formGroup) as any;

        expect(sMS).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ISMS should not enable id FormControl', () => {
        const formGroup = service.createSMSFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewSMS should disable id FormControl', () => {
        const formGroup = service.createSMSFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
