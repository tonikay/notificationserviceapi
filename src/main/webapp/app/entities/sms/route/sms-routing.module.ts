import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { SMSComponent } from '../list/sms.component';
import { SMSDetailComponent } from '../detail/sms-detail.component';
import { SMSUpdateComponent } from '../update/sms-update.component';
import { SMSRoutingResolveService } from './sms-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const sMSRoute: Routes = [
  {
    path: '',
    component: SMSComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SMSDetailComponent,
    resolve: {
      sMS: SMSRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SMSUpdateComponent,
    resolve: {
      sMS: SMSRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SMSUpdateComponent,
    resolve: {
      sMS: SMSRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(sMSRoute)],
  exports: [RouterModule],
})
export class SMSRoutingModule {}
