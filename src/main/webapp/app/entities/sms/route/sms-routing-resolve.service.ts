import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISMS } from '../sms.model';
import { SMSService } from '../service/sms.service';

@Injectable({ providedIn: 'root' })
export class SMSRoutingResolveService implements Resolve<ISMS | null> {
  constructor(protected service: SMSService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISMS | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((sMS: HttpResponse<ISMS>) => {
          if (sMS.body) {
            return of(sMS.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
