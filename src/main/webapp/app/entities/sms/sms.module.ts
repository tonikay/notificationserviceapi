import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { SMSComponent } from './list/sms.component';
import { SMSDetailComponent } from './detail/sms-detail.component';
import { SMSUpdateComponent } from './update/sms-update.component';
import { SMSDeleteDialogComponent } from './delete/sms-delete-dialog.component';
import { SMSRoutingModule } from './route/sms-routing.module';

@NgModule({
  imports: [SharedModule, SMSRoutingModule],
  declarations: [SMSComponent, SMSDetailComponent, SMSUpdateComponent, SMSDeleteDialogComponent],
})
export class SMSModule {}
