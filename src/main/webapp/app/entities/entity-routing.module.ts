import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'api-token',
        data: { pageTitle: 'notificationserviceApiApp.apiToken.home.title' },
        loadChildren: () => import('./api-token/api-token.module').then(m => m.ApiTokenModule),
      },
      {
        path: 'app-logging',
        data: { pageTitle: 'notificationserviceApiApp.appLogging.home.title' },
        loadChildren: () => import('./app-logging/app-logging.module').then(m => m.AppLoggingModule),
      },
      {
        path: 'mda',
        data: { pageTitle: 'notificationserviceApiApp.mDA.home.title' },
        loadChildren: () => import('./mda/mda.module').then(m => m.MDAModule),
      },
      {
        path: 'application',
        data: { pageTitle: 'notificationserviceApiApp.application.home.title' },
        loadChildren: () => import('./application/application.module').then(m => m.ApplicationModule),
      },
      {
        path: 'email',
        data: { pageTitle: 'notificationserviceApiApp.email.home.title' },
        loadChildren: () => import('./email/email.module').then(m => m.EmailModule),
      },
      {
        path: 'email-host-config',
        data: { pageTitle: 'notificationserviceApiApp.emailHostConfig.home.title' },
        loadChildren: () => import('./email-host-config/email-host-config.module').then(m => m.EmailHostConfigModule),
      },
      {
        path: 'sms',
        data: { pageTitle: 'notificationserviceApiApp.sMS.home.title' },
        loadChildren: () => import('./sms/sms.module').then(m => m.SMSModule),
      },
      {
        path: 'sms-host-config',
        data: { pageTitle: 'notificationserviceApiApp.sMSHostConfig.home.title' },
        loadChildren: () => import('./sms-host-config/sms-host-config.module').then(m => m.SMSHostConfigModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
