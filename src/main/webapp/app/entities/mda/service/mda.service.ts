import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMDA, NewMDA } from '../mda.model';

export type PartialUpdateMDA = Partial<IMDA> & Pick<IMDA, 'id'>;

type RestOf<T extends IMDA | NewMDA> = Omit<T, 'createdDate' | 'updatedDate'> & {
  createdDate?: string | null;
  updatedDate?: string | null;
};

export type RestMDA = RestOf<IMDA>;

export type NewRestMDA = RestOf<NewMDA>;

export type PartialUpdateRestMDA = RestOf<PartialUpdateMDA>;

export type EntityResponseType = HttpResponse<IMDA>;
export type EntityArrayResponseType = HttpResponse<IMDA[]>;

@Injectable({ providedIn: 'root' })
export class MDAService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/mdas');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(mDA: NewMDA): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mDA);
    return this.http.post<RestMDA>(this.resourceUrl, copy, { observe: 'response' }).pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(mDA: IMDA): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mDA);
    return this.http
      .put<RestMDA>(`${this.resourceUrl}/${this.getMDAIdentifier(mDA)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(mDA: PartialUpdateMDA): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mDA);
    return this.http
      .patch<RestMDA>(`${this.resourceUrl}/${this.getMDAIdentifier(mDA)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestMDA>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestMDA[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getMDAIdentifier(mDA: Pick<IMDA, 'id'>): number {
    return mDA.id;
  }

  compareMDA(o1: Pick<IMDA, 'id'> | null, o2: Pick<IMDA, 'id'> | null): boolean {
    return o1 && o2 ? this.getMDAIdentifier(o1) === this.getMDAIdentifier(o2) : o1 === o2;
  }

  addMDAToCollectionIfMissing<Type extends Pick<IMDA, 'id'>>(mDACollection: Type[], ...mDASToCheck: (Type | null | undefined)[]): Type[] {
    const mDAS: Type[] = mDASToCheck.filter(isPresent);
    if (mDAS.length > 0) {
      const mDACollectionIdentifiers = mDACollection.map(mDAItem => this.getMDAIdentifier(mDAItem)!);
      const mDASToAdd = mDAS.filter(mDAItem => {
        const mDAIdentifier = this.getMDAIdentifier(mDAItem);
        if (mDACollectionIdentifiers.includes(mDAIdentifier)) {
          return false;
        }
        mDACollectionIdentifiers.push(mDAIdentifier);
        return true;
      });
      return [...mDASToAdd, ...mDACollection];
    }
    return mDACollection;
  }

  protected convertDateFromClient<T extends IMDA | NewMDA | PartialUpdateMDA>(mDA: T): RestOf<T> {
    return {
      ...mDA,
      createdDate: mDA.createdDate?.toJSON() ?? null,
      updatedDate: mDA.updatedDate?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restMDA: RestMDA): IMDA {
    return {
      ...restMDA,
      createdDate: restMDA.createdDate ? dayjs(restMDA.createdDate) : undefined,
      updatedDate: restMDA.updatedDate ? dayjs(restMDA.updatedDate) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestMDA>): HttpResponse<IMDA> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestMDA[]>): HttpResponse<IMDA[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
