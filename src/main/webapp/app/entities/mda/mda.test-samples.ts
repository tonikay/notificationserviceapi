import dayjs from 'dayjs/esm';

import { IMDA, NewMDA } from './mda.model';

export const sampleWithRequiredData: IMDA = {
  id: 4569,
};

export const sampleWithPartialData: IMDA = {
  id: 67996,
  name: 'index yellow viral',
  updatedDate: dayjs('2022-12-07T22:47'),
  updatedBy: 'interface Card',
};

export const sampleWithFullData: IMDA = {
  id: 29678,
  name: 'transmitting',
  createdDate: dayjs('2022-12-08T06:15'),
  createdBy: 'fuchsia Borders',
  updatedDate: dayjs('2022-12-07T19:35'),
  updatedBy: 'Missouri',
};

export const sampleWithNewData: NewMDA = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
