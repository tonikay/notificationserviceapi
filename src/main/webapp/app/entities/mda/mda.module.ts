import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MDAComponent } from './list/mda.component';
import { MDADetailComponent } from './detail/mda-detail.component';
import { MDAUpdateComponent } from './update/mda-update.component';
import { MDADeleteDialogComponent } from './delete/mda-delete-dialog.component';
import { MDARoutingModule } from './route/mda-routing.module';

@NgModule({
  imports: [SharedModule, MDARoutingModule],
  declarations: [MDAComponent, MDADetailComponent, MDAUpdateComponent, MDADeleteDialogComponent],
})
export class MDAModule {}
