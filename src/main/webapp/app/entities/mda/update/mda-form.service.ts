import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IMDA, NewMDA } from '../mda.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IMDA for edit and NewMDAFormGroupInput for create.
 */
type MDAFormGroupInput = IMDA | PartialWithRequiredKeyOf<NewMDA>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IMDA | NewMDA> = Omit<T, 'createdDate' | 'updatedDate'> & {
  createdDate?: string | null;
  updatedDate?: string | null;
};

type MDAFormRawValue = FormValueOf<IMDA>;

type NewMDAFormRawValue = FormValueOf<NewMDA>;

type MDAFormDefaults = Pick<NewMDA, 'id' | 'createdDate' | 'updatedDate'>;

type MDAFormGroupContent = {
  id: FormControl<MDAFormRawValue['id'] | NewMDA['id']>;
  name: FormControl<MDAFormRawValue['name']>;
  createdDate: FormControl<MDAFormRawValue['createdDate']>;
  createdBy: FormControl<MDAFormRawValue['createdBy']>;
  updatedDate: FormControl<MDAFormRawValue['updatedDate']>;
  updatedBy: FormControl<MDAFormRawValue['updatedBy']>;
  userAccount: FormControl<MDAFormRawValue['userAccount']>;
};

export type MDAFormGroup = FormGroup<MDAFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class MDAFormService {
  createMDAFormGroup(mDA: MDAFormGroupInput = { id: null }): MDAFormGroup {
    const mDARawValue = this.convertMDAToMDARawValue({
      ...this.getFormDefaults(),
      ...mDA,
    });
    return new FormGroup<MDAFormGroupContent>({
      id: new FormControl(
        { value: mDARawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(mDARawValue.name),
      createdDate: new FormControl(mDARawValue.createdDate),
      createdBy: new FormControl(mDARawValue.createdBy),
      updatedDate: new FormControl(mDARawValue.updatedDate),
      updatedBy: new FormControl(mDARawValue.updatedBy),
      userAccount: new FormControl(mDARawValue.userAccount),
    });
  }

  getMDA(form: MDAFormGroup): IMDA | NewMDA {
    return this.convertMDARawValueToMDA(form.getRawValue() as MDAFormRawValue | NewMDAFormRawValue);
  }

  resetForm(form: MDAFormGroup, mDA: MDAFormGroupInput): void {
    const mDARawValue = this.convertMDAToMDARawValue({ ...this.getFormDefaults(), ...mDA });
    form.reset(
      {
        ...mDARawValue,
        id: { value: mDARawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): MDAFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      createdDate: currentTime,
      updatedDate: currentTime,
    };
  }

  private convertMDARawValueToMDA(rawMDA: MDAFormRawValue | NewMDAFormRawValue): IMDA | NewMDA {
    return {
      ...rawMDA,
      createdDate: dayjs(rawMDA.createdDate, DATE_TIME_FORMAT),
      updatedDate: dayjs(rawMDA.updatedDate, DATE_TIME_FORMAT),
    };
  }

  private convertMDAToMDARawValue(
    mDA: IMDA | (Partial<NewMDA> & MDAFormDefaults)
  ): MDAFormRawValue | PartialWithRequiredKeyOf<NewMDAFormRawValue> {
    return {
      ...mDA,
      createdDate: mDA.createdDate ? mDA.createdDate.format(DATE_TIME_FORMAT) : undefined,
      updatedDate: mDA.updatedDate ? mDA.updatedDate.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
