import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../mda.test-samples';

import { MDAFormService } from './mda-form.service';

describe('MDA Form Service', () => {
  let service: MDAFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MDAFormService);
  });

  describe('Service methods', () => {
    describe('createMDAFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createMDAFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            createdDate: expect.any(Object),
            createdBy: expect.any(Object),
            updatedDate: expect.any(Object),
            updatedBy: expect.any(Object),
            userAccount: expect.any(Object),
          })
        );
      });

      it('passing IMDA should create a new form with FormGroup', () => {
        const formGroup = service.createMDAFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            createdDate: expect.any(Object),
            createdBy: expect.any(Object),
            updatedDate: expect.any(Object),
            updatedBy: expect.any(Object),
            userAccount: expect.any(Object),
          })
        );
      });
    });

    describe('getMDA', () => {
      it('should return NewMDA for default MDA initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createMDAFormGroup(sampleWithNewData);

        const mDA = service.getMDA(formGroup) as any;

        expect(mDA).toMatchObject(sampleWithNewData);
      });

      it('should return NewMDA for empty MDA initial value', () => {
        const formGroup = service.createMDAFormGroup();

        const mDA = service.getMDA(formGroup) as any;

        expect(mDA).toMatchObject({});
      });

      it('should return IMDA', () => {
        const formGroup = service.createMDAFormGroup(sampleWithRequiredData);

        const mDA = service.getMDA(formGroup) as any;

        expect(mDA).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IMDA should not enable id FormControl', () => {
        const formGroup = service.createMDAFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewMDA should disable id FormControl', () => {
        const formGroup = service.createMDAFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
