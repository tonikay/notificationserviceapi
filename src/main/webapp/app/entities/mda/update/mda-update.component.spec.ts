import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { MDAFormService } from './mda-form.service';
import { MDAService } from '../service/mda.service';
import { IMDA } from '../mda.model';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

import { MDAUpdateComponent } from './mda-update.component';

describe('MDA Management Update Component', () => {
  let comp: MDAUpdateComponent;
  let fixture: ComponentFixture<MDAUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let mDAFormService: MDAFormService;
  let mDAService: MDAService;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [MDAUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(MDAUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MDAUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    mDAFormService = TestBed.inject(MDAFormService);
    mDAService = TestBed.inject(MDAService);
    userService = TestBed.inject(UserService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call User query and add missing value', () => {
      const mDA: IMDA = { id: 456 };
      const userAccount: IUser = { id: 20107 };
      mDA.userAccount = userAccount;

      const userCollection: IUser[] = [{ id: 97080 }];
      jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
      const additionalUsers = [userAccount];
      const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
      jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ mDA });
      comp.ngOnInit();

      expect(userService.query).toHaveBeenCalled();
      expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(
        userCollection,
        ...additionalUsers.map(expect.objectContaining)
      );
      expect(comp.usersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const mDA: IMDA = { id: 456 };
      const userAccount: IUser = { id: 6367 };
      mDA.userAccount = userAccount;

      activatedRoute.data = of({ mDA });
      comp.ngOnInit();

      expect(comp.usersSharedCollection).toContain(userAccount);
      expect(comp.mDA).toEqual(mDA);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IMDA>>();
      const mDA = { id: 123 };
      jest.spyOn(mDAFormService, 'getMDA').mockReturnValue(mDA);
      jest.spyOn(mDAService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mDA });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: mDA }));
      saveSubject.complete();

      // THEN
      expect(mDAFormService.getMDA).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(mDAService.update).toHaveBeenCalledWith(expect.objectContaining(mDA));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IMDA>>();
      const mDA = { id: 123 };
      jest.spyOn(mDAFormService, 'getMDA').mockReturnValue({ id: null });
      jest.spyOn(mDAService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mDA: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: mDA }));
      saveSubject.complete();

      // THEN
      expect(mDAFormService.getMDA).toHaveBeenCalled();
      expect(mDAService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IMDA>>();
      const mDA = { id: 123 };
      jest.spyOn(mDAService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mDA });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(mDAService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareUser', () => {
      it('Should forward to userService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(userService, 'compareUser');
        comp.compareUser(entity, entity2);
        expect(userService.compareUser).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
