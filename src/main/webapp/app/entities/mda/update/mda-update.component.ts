import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { MDAFormService, MDAFormGroup } from './mda-form.service';
import { IMDA } from '../mda.model';
import { MDAService } from '../service/mda.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

@Component({
  selector: 'jhi-mda-update',
  templateUrl: './mda-update.component.html',
})
export class MDAUpdateComponent implements OnInit {
  isSaving = false;
  mDA: IMDA | null = null;

  usersSharedCollection: IUser[] = [];

  editForm: MDAFormGroup = this.mDAFormService.createMDAFormGroup();

  constructor(
    protected mDAService: MDAService,
    protected mDAFormService: MDAFormService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareUser = (o1: IUser | null, o2: IUser | null): boolean => this.userService.compareUser(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mDA }) => {
      this.mDA = mDA;
      if (mDA) {
        this.updateForm(mDA);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const mDA = this.mDAFormService.getMDA(this.editForm);
    if (mDA.id !== null) {
      this.subscribeToSaveResponse(this.mDAService.update(mDA));
    } else {
      this.subscribeToSaveResponse(this.mDAService.create(mDA));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMDA>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(mDA: IMDA): void {
    this.mDA = mDA;
    this.mDAFormService.resetForm(this.editForm, mDA);

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing<IUser>(this.usersSharedCollection, mDA.userAccount);
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing<IUser>(users, this.mDA?.userAccount)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }
}
