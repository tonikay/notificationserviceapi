import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMDA } from '../mda.model';
import { MDAService } from '../service/mda.service';

@Injectable({ providedIn: 'root' })
export class MDARoutingResolveService implements Resolve<IMDA | null> {
  constructor(protected service: MDAService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMDA | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((mDA: HttpResponse<IMDA>) => {
          if (mDA.body) {
            return of(mDA.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
