import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MDAComponent } from '../list/mda.component';
import { MDADetailComponent } from '../detail/mda-detail.component';
import { MDAUpdateComponent } from '../update/mda-update.component';
import { MDARoutingResolveService } from './mda-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const mDARoute: Routes = [
  {
    path: '',
    component: MDAComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MDADetailComponent,
    resolve: {
      mDA: MDARoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MDAUpdateComponent,
    resolve: {
      mDA: MDARoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MDAUpdateComponent,
    resolve: {
      mDA: MDARoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(mDARoute)],
  exports: [RouterModule],
})
export class MDARoutingModule {}
