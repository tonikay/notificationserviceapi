import dayjs from 'dayjs/esm';
import { IUser } from 'app/entities/user/user.model';

export interface IMDA {
  id: number;
  name?: string | null;
  createdDate?: dayjs.Dayjs | null;
  createdBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  userAccount?: Pick<IUser, 'id'> | null;
}

export type NewMDA = Omit<IMDA, 'id'> & { id: null };
