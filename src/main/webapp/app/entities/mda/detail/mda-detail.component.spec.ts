import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MDADetailComponent } from './mda-detail.component';

describe('MDA Management Detail Component', () => {
  let comp: MDADetailComponent;
  let fixture: ComponentFixture<MDADetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MDADetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ mDA: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(MDADetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(MDADetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load mDA on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.mDA).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
