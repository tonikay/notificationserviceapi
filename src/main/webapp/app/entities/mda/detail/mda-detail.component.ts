import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMDA } from '../mda.model';

@Component({
  selector: 'jhi-mda-detail',
  templateUrl: './mda-detail.component.html',
})
export class MDADetailComponent implements OnInit {
  mDA: IMDA | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mDA }) => {
      this.mDA = mDA;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
