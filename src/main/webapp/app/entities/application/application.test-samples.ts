import dayjs from 'dayjs/esm';

import { IApplication, NewApplication } from './application.model';

export const sampleWithRequiredData: IApplication = {
  id: 99506,
};

export const sampleWithPartialData: IApplication = {
  id: 82505,
};

export const sampleWithFullData: IApplication = {
  id: 25773,
  appName: 'Common expedite',
  createdDate: dayjs('2022-12-08T10:53'),
  createdBy: 'Health sensor',
  updatedDate: dayjs('2022-12-08T03:57'),
  updatedBy: 'Plastic program bandwidth',
};

export const sampleWithNewData: NewApplication = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
