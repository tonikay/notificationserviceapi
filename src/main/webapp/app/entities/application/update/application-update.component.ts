import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ApplicationFormService, ApplicationFormGroup } from './application-form.service';
import { IApplication } from '../application.model';
import { ApplicationService } from '../service/application.service';
import { IApiToken } from 'app/entities/api-token/api-token.model';
import { ApiTokenService } from 'app/entities/api-token/service/api-token.service';
import { IMDA } from 'app/entities/mda/mda.model';
import { MDAService } from 'app/entities/mda/service/mda.service';

@Component({
  selector: 'jhi-application-update',
  templateUrl: './application-update.component.html',
})
export class ApplicationUpdateComponent implements OnInit {
  isSaving = false;
  application: IApplication | null = null;

  apiTokensSharedCollection: IApiToken[] = [];
  mDASSharedCollection: IMDA[] = [];

  editForm: ApplicationFormGroup = this.applicationFormService.createApplicationFormGroup();

  constructor(
    protected applicationService: ApplicationService,
    protected applicationFormService: ApplicationFormService,
    protected apiTokenService: ApiTokenService,
    protected mDAService: MDAService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareApiToken = (o1: IApiToken | null, o2: IApiToken | null): boolean => this.apiTokenService.compareApiToken(o1, o2);

  compareMDA = (o1: IMDA | null, o2: IMDA | null): boolean => this.mDAService.compareMDA(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ application }) => {
      this.application = application;
      if (application) {
        this.updateForm(application);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const application = this.applicationFormService.getApplication(this.editForm);
    if (application.id !== null) {
      this.subscribeToSaveResponse(this.applicationService.update(application));
    } else {
      this.subscribeToSaveResponse(this.applicationService.create(application));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApplication>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(application: IApplication): void {
    this.application = application;
    this.applicationFormService.resetForm(this.editForm, application);

    this.apiTokensSharedCollection = this.apiTokenService.addApiTokenToCollectionIfMissing<IApiToken>(
      this.apiTokensSharedCollection,
      application.apiToken
    );
    this.mDASSharedCollection = this.mDAService.addMDAToCollectionIfMissing<IMDA>(this.mDASSharedCollection, application.mDA);
  }

  protected loadRelationshipsOptions(): void {
    this.apiTokenService
      .query()
      .pipe(map((res: HttpResponse<IApiToken[]>) => res.body ?? []))
      .pipe(
        map((apiTokens: IApiToken[]) =>
          this.apiTokenService.addApiTokenToCollectionIfMissing<IApiToken>(apiTokens, this.application?.apiToken)
        )
      )
      .subscribe((apiTokens: IApiToken[]) => (this.apiTokensSharedCollection = apiTokens));

    this.mDAService
      .query()
      .pipe(map((res: HttpResponse<IMDA[]>) => res.body ?? []))
      .pipe(map((mDAS: IMDA[]) => this.mDAService.addMDAToCollectionIfMissing<IMDA>(mDAS, this.application?.mDA)))
      .subscribe((mDAS: IMDA[]) => (this.mDASSharedCollection = mDAS));
  }
}
