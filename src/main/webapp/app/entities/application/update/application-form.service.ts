import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IApplication, NewApplication } from '../application.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IApplication for edit and NewApplicationFormGroupInput for create.
 */
type ApplicationFormGroupInput = IApplication | PartialWithRequiredKeyOf<NewApplication>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IApplication | NewApplication> = Omit<T, 'createdDate' | 'updatedDate'> & {
  createdDate?: string | null;
  updatedDate?: string | null;
};

type ApplicationFormRawValue = FormValueOf<IApplication>;

type NewApplicationFormRawValue = FormValueOf<NewApplication>;

type ApplicationFormDefaults = Pick<NewApplication, 'id' | 'createdDate' | 'updatedDate'>;

type ApplicationFormGroupContent = {
  id: FormControl<ApplicationFormRawValue['id'] | NewApplication['id']>;
  appName: FormControl<ApplicationFormRawValue['appName']>;
  createdDate: FormControl<ApplicationFormRawValue['createdDate']>;
  createdBy: FormControl<ApplicationFormRawValue['createdBy']>;
  updatedDate: FormControl<ApplicationFormRawValue['updatedDate']>;
  updatedBy: FormControl<ApplicationFormRawValue['updatedBy']>;
  apiToken: FormControl<ApplicationFormRawValue['apiToken']>;
  mDA: FormControl<ApplicationFormRawValue['mDA']>;
};

export type ApplicationFormGroup = FormGroup<ApplicationFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ApplicationFormService {
  createApplicationFormGroup(application: ApplicationFormGroupInput = { id: null }): ApplicationFormGroup {
    const applicationRawValue = this.convertApplicationToApplicationRawValue({
      ...this.getFormDefaults(),
      ...application,
    });
    return new FormGroup<ApplicationFormGroupContent>({
      id: new FormControl(
        { value: applicationRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      appName: new FormControl(applicationRawValue.appName),
      createdDate: new FormControl(applicationRawValue.createdDate),
      createdBy: new FormControl(applicationRawValue.createdBy),
      updatedDate: new FormControl(applicationRawValue.updatedDate),
      updatedBy: new FormControl(applicationRawValue.updatedBy),
      apiToken: new FormControl(applicationRawValue.apiToken),
      mDA: new FormControl(applicationRawValue.mDA),
    });
  }

  getApplication(form: ApplicationFormGroup): IApplication | NewApplication {
    return this.convertApplicationRawValueToApplication(form.getRawValue() as ApplicationFormRawValue | NewApplicationFormRawValue);
  }

  resetForm(form: ApplicationFormGroup, application: ApplicationFormGroupInput): void {
    const applicationRawValue = this.convertApplicationToApplicationRawValue({ ...this.getFormDefaults(), ...application });
    form.reset(
      {
        ...applicationRawValue,
        id: { value: applicationRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ApplicationFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      createdDate: currentTime,
      updatedDate: currentTime,
    };
  }

  private convertApplicationRawValueToApplication(
    rawApplication: ApplicationFormRawValue | NewApplicationFormRawValue
  ): IApplication | NewApplication {
    return {
      ...rawApplication,
      createdDate: dayjs(rawApplication.createdDate, DATE_TIME_FORMAT),
      updatedDate: dayjs(rawApplication.updatedDate, DATE_TIME_FORMAT),
    };
  }

  private convertApplicationToApplicationRawValue(
    application: IApplication | (Partial<NewApplication> & ApplicationFormDefaults)
  ): ApplicationFormRawValue | PartialWithRequiredKeyOf<NewApplicationFormRawValue> {
    return {
      ...application,
      createdDate: application.createdDate ? application.createdDate.format(DATE_TIME_FORMAT) : undefined,
      updatedDate: application.updatedDate ? application.updatedDate.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
