import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ApplicationFormService } from './application-form.service';
import { ApplicationService } from '../service/application.service';
import { IApplication } from '../application.model';
import { IApiToken } from 'app/entities/api-token/api-token.model';
import { ApiTokenService } from 'app/entities/api-token/service/api-token.service';
import { IMDA } from 'app/entities/mda/mda.model';
import { MDAService } from 'app/entities/mda/service/mda.service';

import { ApplicationUpdateComponent } from './application-update.component';

describe('Application Management Update Component', () => {
  let comp: ApplicationUpdateComponent;
  let fixture: ComponentFixture<ApplicationUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let applicationFormService: ApplicationFormService;
  let applicationService: ApplicationService;
  let apiTokenService: ApiTokenService;
  let mDAService: MDAService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ApplicationUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ApplicationUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ApplicationUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    applicationFormService = TestBed.inject(ApplicationFormService);
    applicationService = TestBed.inject(ApplicationService);
    apiTokenService = TestBed.inject(ApiTokenService);
    mDAService = TestBed.inject(MDAService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call ApiToken query and add missing value', () => {
      const application: IApplication = { id: 456 };
      const apiToken: IApiToken = { id: 63265 };
      application.apiToken = apiToken;

      const apiTokenCollection: IApiToken[] = [{ id: 65818 }];
      jest.spyOn(apiTokenService, 'query').mockReturnValue(of(new HttpResponse({ body: apiTokenCollection })));
      const additionalApiTokens = [apiToken];
      const expectedCollection: IApiToken[] = [...additionalApiTokens, ...apiTokenCollection];
      jest.spyOn(apiTokenService, 'addApiTokenToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ application });
      comp.ngOnInit();

      expect(apiTokenService.query).toHaveBeenCalled();
      expect(apiTokenService.addApiTokenToCollectionIfMissing).toHaveBeenCalledWith(
        apiTokenCollection,
        ...additionalApiTokens.map(expect.objectContaining)
      );
      expect(comp.apiTokensSharedCollection).toEqual(expectedCollection);
    });

    it('Should call MDA query and add missing value', () => {
      const application: IApplication = { id: 456 };
      const mDA: IMDA = { id: 54118 };
      application.mDA = mDA;

      const mDACollection: IMDA[] = [{ id: 82641 }];
      jest.spyOn(mDAService, 'query').mockReturnValue(of(new HttpResponse({ body: mDACollection })));
      const additionalMDAS = [mDA];
      const expectedCollection: IMDA[] = [...additionalMDAS, ...mDACollection];
      jest.spyOn(mDAService, 'addMDAToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ application });
      comp.ngOnInit();

      expect(mDAService.query).toHaveBeenCalled();
      expect(mDAService.addMDAToCollectionIfMissing).toHaveBeenCalledWith(mDACollection, ...additionalMDAS.map(expect.objectContaining));
      expect(comp.mDASSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const application: IApplication = { id: 456 };
      const apiToken: IApiToken = { id: 33688 };
      application.apiToken = apiToken;
      const mDA: IMDA = { id: 22180 };
      application.mDA = mDA;

      activatedRoute.data = of({ application });
      comp.ngOnInit();

      expect(comp.apiTokensSharedCollection).toContain(apiToken);
      expect(comp.mDASSharedCollection).toContain(mDA);
      expect(comp.application).toEqual(application);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IApplication>>();
      const application = { id: 123 };
      jest.spyOn(applicationFormService, 'getApplication').mockReturnValue(application);
      jest.spyOn(applicationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ application });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: application }));
      saveSubject.complete();

      // THEN
      expect(applicationFormService.getApplication).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(applicationService.update).toHaveBeenCalledWith(expect.objectContaining(application));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IApplication>>();
      const application = { id: 123 };
      jest.spyOn(applicationFormService, 'getApplication').mockReturnValue({ id: null });
      jest.spyOn(applicationService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ application: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: application }));
      saveSubject.complete();

      // THEN
      expect(applicationFormService.getApplication).toHaveBeenCalled();
      expect(applicationService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IApplication>>();
      const application = { id: 123 };
      jest.spyOn(applicationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ application });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(applicationService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareApiToken', () => {
      it('Should forward to apiTokenService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(apiTokenService, 'compareApiToken');
        comp.compareApiToken(entity, entity2);
        expect(apiTokenService.compareApiToken).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareMDA', () => {
      it('Should forward to mDAService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(mDAService, 'compareMDA');
        comp.compareMDA(entity, entity2);
        expect(mDAService.compareMDA).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
