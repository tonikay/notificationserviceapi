import dayjs from 'dayjs/esm';
import { IApiToken } from 'app/entities/api-token/api-token.model';
import { IMDA } from 'app/entities/mda/mda.model';

export interface IApplication {
  id: number;
  appName?: string | null;
  createdDate?: dayjs.Dayjs | null;
  createdBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  apiToken?: Pick<IApiToken, 'id'> | null;
  mDA?: Pick<IMDA, 'id'> | null;
}

export type NewApplication = Omit<IApplication, 'id'> & { id: null };
