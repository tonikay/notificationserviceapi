import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EmailHostConfigComponent } from '../list/email-host-config.component';
import { EmailHostConfigDetailComponent } from '../detail/email-host-config-detail.component';
import { EmailHostConfigUpdateComponent } from '../update/email-host-config-update.component';
import { EmailHostConfigRoutingResolveService } from './email-host-config-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const emailHostConfigRoute: Routes = [
  {
    path: '',
    component: EmailHostConfigComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EmailHostConfigDetailComponent,
    resolve: {
      emailHostConfig: EmailHostConfigRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EmailHostConfigUpdateComponent,
    resolve: {
      emailHostConfig: EmailHostConfigRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EmailHostConfigUpdateComponent,
    resolve: {
      emailHostConfig: EmailHostConfigRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(emailHostConfigRoute)],
  exports: [RouterModule],
})
export class EmailHostConfigRoutingModule {}
