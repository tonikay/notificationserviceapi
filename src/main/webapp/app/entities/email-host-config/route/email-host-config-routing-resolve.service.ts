import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEmailHostConfig } from '../email-host-config.model';
import { EmailHostConfigService } from '../service/email-host-config.service';

@Injectable({ providedIn: 'root' })
export class EmailHostConfigRoutingResolveService implements Resolve<IEmailHostConfig | null> {
  constructor(protected service: EmailHostConfigService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEmailHostConfig | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((emailHostConfig: HttpResponse<IEmailHostConfig>) => {
          if (emailHostConfig.body) {
            return of(emailHostConfig.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
