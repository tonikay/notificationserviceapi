import dayjs from 'dayjs/esm';
import { IApplication } from 'app/entities/application/application.model';
import { SecurityProtocol } from 'app/entities/enumerations/security-protocol.model';

export interface IEmailHostConfig {
  id: number;
  senderEmail?: string | null;
  host?: string | null;
  port?: number | null;
  protocol?: SecurityProtocol | null;
  username?: string | null;
  password?: string | null;
  createdDate?: dayjs.Dayjs | null;
  createdBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  application?: Pick<IApplication, 'id'> | null;
}

export type NewEmailHostConfig = Omit<IEmailHostConfig, 'id'> & { id: null };
