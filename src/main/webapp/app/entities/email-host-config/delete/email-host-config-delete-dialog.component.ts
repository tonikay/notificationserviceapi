import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEmailHostConfig } from '../email-host-config.model';
import { EmailHostConfigService } from '../service/email-host-config.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './email-host-config-delete-dialog.component.html',
})
export class EmailHostConfigDeleteDialogComponent {
  emailHostConfig?: IEmailHostConfig;

  constructor(protected emailHostConfigService: EmailHostConfigService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.emailHostConfigService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
