import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../email-host-config.test-samples';

import { EmailHostConfigFormService } from './email-host-config-form.service';

describe('EmailHostConfig Form Service', () => {
  let service: EmailHostConfigFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmailHostConfigFormService);
  });

  describe('Service methods', () => {
    describe('createEmailHostConfigFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createEmailHostConfigFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            senderEmail: expect.any(Object),
            host: expect.any(Object),
            port: expect.any(Object),
            protocol: expect.any(Object),
            username: expect.any(Object),
            password: expect.any(Object),
            createdDate: expect.any(Object),
            createdBy: expect.any(Object),
            updatedDate: expect.any(Object),
            updatedBy: expect.any(Object),
            application: expect.any(Object),
          })
        );
      });

      it('passing IEmailHostConfig should create a new form with FormGroup', () => {
        const formGroup = service.createEmailHostConfigFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            senderEmail: expect.any(Object),
            host: expect.any(Object),
            port: expect.any(Object),
            protocol: expect.any(Object),
            username: expect.any(Object),
            password: expect.any(Object),
            createdDate: expect.any(Object),
            createdBy: expect.any(Object),
            updatedDate: expect.any(Object),
            updatedBy: expect.any(Object),
            application: expect.any(Object),
          })
        );
      });
    });

    describe('getEmailHostConfig', () => {
      it('should return NewEmailHostConfig for default EmailHostConfig initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createEmailHostConfigFormGroup(sampleWithNewData);

        const emailHostConfig = service.getEmailHostConfig(formGroup) as any;

        expect(emailHostConfig).toMatchObject(sampleWithNewData);
      });

      it('should return NewEmailHostConfig for empty EmailHostConfig initial value', () => {
        const formGroup = service.createEmailHostConfigFormGroup();

        const emailHostConfig = service.getEmailHostConfig(formGroup) as any;

        expect(emailHostConfig).toMatchObject({});
      });

      it('should return IEmailHostConfig', () => {
        const formGroup = service.createEmailHostConfigFormGroup(sampleWithRequiredData);

        const emailHostConfig = service.getEmailHostConfig(formGroup) as any;

        expect(emailHostConfig).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IEmailHostConfig should not enable id FormControl', () => {
        const formGroup = service.createEmailHostConfigFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewEmailHostConfig should disable id FormControl', () => {
        const formGroup = service.createEmailHostConfigFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
