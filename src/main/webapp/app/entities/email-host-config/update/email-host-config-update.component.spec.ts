import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { EmailHostConfigFormService } from './email-host-config-form.service';
import { EmailHostConfigService } from '../service/email-host-config.service';
import { IEmailHostConfig } from '../email-host-config.model';
import { IApplication } from 'app/entities/application/application.model';
import { ApplicationService } from 'app/entities/application/service/application.service';

import { EmailHostConfigUpdateComponent } from './email-host-config-update.component';

describe('EmailHostConfig Management Update Component', () => {
  let comp: EmailHostConfigUpdateComponent;
  let fixture: ComponentFixture<EmailHostConfigUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let emailHostConfigFormService: EmailHostConfigFormService;
  let emailHostConfigService: EmailHostConfigService;
  let applicationService: ApplicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [EmailHostConfigUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EmailHostConfigUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EmailHostConfigUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    emailHostConfigFormService = TestBed.inject(EmailHostConfigFormService);
    emailHostConfigService = TestBed.inject(EmailHostConfigService);
    applicationService = TestBed.inject(ApplicationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Application query and add missing value', () => {
      const emailHostConfig: IEmailHostConfig = { id: 456 };
      const application: IApplication = { id: 37556 };
      emailHostConfig.application = application;

      const applicationCollection: IApplication[] = [{ id: 71132 }];
      jest.spyOn(applicationService, 'query').mockReturnValue(of(new HttpResponse({ body: applicationCollection })));
      const additionalApplications = [application];
      const expectedCollection: IApplication[] = [...additionalApplications, ...applicationCollection];
      jest.spyOn(applicationService, 'addApplicationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ emailHostConfig });
      comp.ngOnInit();

      expect(applicationService.query).toHaveBeenCalled();
      expect(applicationService.addApplicationToCollectionIfMissing).toHaveBeenCalledWith(
        applicationCollection,
        ...additionalApplications.map(expect.objectContaining)
      );
      expect(comp.applicationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const emailHostConfig: IEmailHostConfig = { id: 456 };
      const application: IApplication = { id: 7604 };
      emailHostConfig.application = application;

      activatedRoute.data = of({ emailHostConfig });
      comp.ngOnInit();

      expect(comp.applicationsSharedCollection).toContain(application);
      expect(comp.emailHostConfig).toEqual(emailHostConfig);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmailHostConfig>>();
      const emailHostConfig = { id: 123 };
      jest.spyOn(emailHostConfigFormService, 'getEmailHostConfig').mockReturnValue(emailHostConfig);
      jest.spyOn(emailHostConfigService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ emailHostConfig });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: emailHostConfig }));
      saveSubject.complete();

      // THEN
      expect(emailHostConfigFormService.getEmailHostConfig).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(emailHostConfigService.update).toHaveBeenCalledWith(expect.objectContaining(emailHostConfig));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmailHostConfig>>();
      const emailHostConfig = { id: 123 };
      jest.spyOn(emailHostConfigFormService, 'getEmailHostConfig').mockReturnValue({ id: null });
      jest.spyOn(emailHostConfigService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ emailHostConfig: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: emailHostConfig }));
      saveSubject.complete();

      // THEN
      expect(emailHostConfigFormService.getEmailHostConfig).toHaveBeenCalled();
      expect(emailHostConfigService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmailHostConfig>>();
      const emailHostConfig = { id: 123 };
      jest.spyOn(emailHostConfigService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ emailHostConfig });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(emailHostConfigService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareApplication', () => {
      it('Should forward to applicationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(applicationService, 'compareApplication');
        comp.compareApplication(entity, entity2);
        expect(applicationService.compareApplication).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
