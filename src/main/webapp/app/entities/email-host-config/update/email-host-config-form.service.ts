import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IEmailHostConfig, NewEmailHostConfig } from '../email-host-config.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IEmailHostConfig for edit and NewEmailHostConfigFormGroupInput for create.
 */
type EmailHostConfigFormGroupInput = IEmailHostConfig | PartialWithRequiredKeyOf<NewEmailHostConfig>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IEmailHostConfig | NewEmailHostConfig> = Omit<T, 'createdDate' | 'updatedDate'> & {
  createdDate?: string | null;
  updatedDate?: string | null;
};

type EmailHostConfigFormRawValue = FormValueOf<IEmailHostConfig>;

type NewEmailHostConfigFormRawValue = FormValueOf<NewEmailHostConfig>;

type EmailHostConfigFormDefaults = Pick<NewEmailHostConfig, 'id' | 'createdDate' | 'updatedDate'>;

type EmailHostConfigFormGroupContent = {
  id: FormControl<EmailHostConfigFormRawValue['id'] | NewEmailHostConfig['id']>;
  senderEmail: FormControl<EmailHostConfigFormRawValue['senderEmail']>;
  host: FormControl<EmailHostConfigFormRawValue['host']>;
  port: FormControl<EmailHostConfigFormRawValue['port']>;
  protocol: FormControl<EmailHostConfigFormRawValue['protocol']>;
  username: FormControl<EmailHostConfigFormRawValue['username']>;
  password: FormControl<EmailHostConfigFormRawValue['password']>;
  createdDate: FormControl<EmailHostConfigFormRawValue['createdDate']>;
  createdBy: FormControl<EmailHostConfigFormRawValue['createdBy']>;
  updatedDate: FormControl<EmailHostConfigFormRawValue['updatedDate']>;
  updatedBy: FormControl<EmailHostConfigFormRawValue['updatedBy']>;
  application: FormControl<EmailHostConfigFormRawValue['application']>;
};

export type EmailHostConfigFormGroup = FormGroup<EmailHostConfigFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class EmailHostConfigFormService {
  createEmailHostConfigFormGroup(emailHostConfig: EmailHostConfigFormGroupInput = { id: null }): EmailHostConfigFormGroup {
    const emailHostConfigRawValue = this.convertEmailHostConfigToEmailHostConfigRawValue({
      ...this.getFormDefaults(),
      ...emailHostConfig,
    });
    return new FormGroup<EmailHostConfigFormGroupContent>({
      id: new FormControl(
        { value: emailHostConfigRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      senderEmail: new FormControl(emailHostConfigRawValue.senderEmail, {
        validators: [Validators.required],
      }),
      host: new FormControl(emailHostConfigRawValue.host, {
        validators: [Validators.required],
      }),
      port: new FormControl(emailHostConfigRawValue.port, {
        validators: [Validators.required],
      }),
      protocol: new FormControl(emailHostConfigRawValue.protocol, {
        validators: [Validators.required],
      }),
      username: new FormControl(emailHostConfigRawValue.username),
      password: new FormControl(emailHostConfigRawValue.password),
      createdDate: new FormControl(emailHostConfigRawValue.createdDate),
      createdBy: new FormControl(emailHostConfigRawValue.createdBy),
      updatedDate: new FormControl(emailHostConfigRawValue.updatedDate),
      updatedBy: new FormControl(emailHostConfigRawValue.updatedBy),
      application: new FormControl(emailHostConfigRawValue.application),
    });
  }

  getEmailHostConfig(form: EmailHostConfigFormGroup): IEmailHostConfig | NewEmailHostConfig {
    return this.convertEmailHostConfigRawValueToEmailHostConfig(
      form.getRawValue() as EmailHostConfigFormRawValue | NewEmailHostConfigFormRawValue
    );
  }

  resetForm(form: EmailHostConfigFormGroup, emailHostConfig: EmailHostConfigFormGroupInput): void {
    const emailHostConfigRawValue = this.convertEmailHostConfigToEmailHostConfigRawValue({ ...this.getFormDefaults(), ...emailHostConfig });
    form.reset(
      {
        ...emailHostConfigRawValue,
        id: { value: emailHostConfigRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): EmailHostConfigFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      createdDate: currentTime,
      updatedDate: currentTime,
    };
  }

  private convertEmailHostConfigRawValueToEmailHostConfig(
    rawEmailHostConfig: EmailHostConfigFormRawValue | NewEmailHostConfigFormRawValue
  ): IEmailHostConfig | NewEmailHostConfig {
    return {
      ...rawEmailHostConfig,
      createdDate: dayjs(rawEmailHostConfig.createdDate, DATE_TIME_FORMAT),
      updatedDate: dayjs(rawEmailHostConfig.updatedDate, DATE_TIME_FORMAT),
    };
  }

  private convertEmailHostConfigToEmailHostConfigRawValue(
    emailHostConfig: IEmailHostConfig | (Partial<NewEmailHostConfig> & EmailHostConfigFormDefaults)
  ): EmailHostConfigFormRawValue | PartialWithRequiredKeyOf<NewEmailHostConfigFormRawValue> {
    return {
      ...emailHostConfig,
      createdDate: emailHostConfig.createdDate ? emailHostConfig.createdDate.format(DATE_TIME_FORMAT) : undefined,
      updatedDate: emailHostConfig.updatedDate ? emailHostConfig.updatedDate.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
