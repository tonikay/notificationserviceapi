import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { EmailHostConfigFormService, EmailHostConfigFormGroup } from './email-host-config-form.service';
import { IEmailHostConfig } from '../email-host-config.model';
import { EmailHostConfigService } from '../service/email-host-config.service';
import { IApplication } from 'app/entities/application/application.model';
import { ApplicationService } from 'app/entities/application/service/application.service';
import { SecurityProtocol } from 'app/entities/enumerations/security-protocol.model';

@Component({
  selector: 'jhi-email-host-config-update',
  templateUrl: './email-host-config-update.component.html',
})
export class EmailHostConfigUpdateComponent implements OnInit {
  isSaving = false;
  emailHostConfig: IEmailHostConfig | null = null;
  securityProtocolValues = Object.keys(SecurityProtocol);

  applicationsSharedCollection: IApplication[] = [];

  editForm: EmailHostConfigFormGroup = this.emailHostConfigFormService.createEmailHostConfigFormGroup();

  constructor(
    protected emailHostConfigService: EmailHostConfigService,
    protected emailHostConfigFormService: EmailHostConfigFormService,
    protected applicationService: ApplicationService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareApplication = (o1: IApplication | null, o2: IApplication | null): boolean => this.applicationService.compareApplication(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ emailHostConfig }) => {
      this.emailHostConfig = emailHostConfig;
      if (emailHostConfig) {
        this.updateForm(emailHostConfig);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const emailHostConfig = this.emailHostConfigFormService.getEmailHostConfig(this.editForm);
    if (emailHostConfig.id !== null) {
      this.subscribeToSaveResponse(this.emailHostConfigService.update(emailHostConfig));
    } else {
      this.subscribeToSaveResponse(this.emailHostConfigService.create(emailHostConfig));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmailHostConfig>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(emailHostConfig: IEmailHostConfig): void {
    this.emailHostConfig = emailHostConfig;
    this.emailHostConfigFormService.resetForm(this.editForm, emailHostConfig);

    this.applicationsSharedCollection = this.applicationService.addApplicationToCollectionIfMissing<IApplication>(
      this.applicationsSharedCollection,
      emailHostConfig.application
    );
  }

  protected loadRelationshipsOptions(): void {
    this.applicationService
      .query()
      .pipe(map((res: HttpResponse<IApplication[]>) => res.body ?? []))
      .pipe(
        map((applications: IApplication[]) =>
          this.applicationService.addApplicationToCollectionIfMissing<IApplication>(applications, this.emailHostConfig?.application)
        )
      )
      .subscribe((applications: IApplication[]) => (this.applicationsSharedCollection = applications));
  }
}
