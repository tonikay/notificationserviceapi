import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEmailHostConfig, NewEmailHostConfig } from '../email-host-config.model';

export type PartialUpdateEmailHostConfig = Partial<IEmailHostConfig> & Pick<IEmailHostConfig, 'id'>;

type RestOf<T extends IEmailHostConfig | NewEmailHostConfig> = Omit<T, 'createdDate' | 'updatedDate'> & {
  createdDate?: string | null;
  updatedDate?: string | null;
};

export type RestEmailHostConfig = RestOf<IEmailHostConfig>;

export type NewRestEmailHostConfig = RestOf<NewEmailHostConfig>;

export type PartialUpdateRestEmailHostConfig = RestOf<PartialUpdateEmailHostConfig>;

export type EntityResponseType = HttpResponse<IEmailHostConfig>;
export type EntityArrayResponseType = HttpResponse<IEmailHostConfig[]>;

@Injectable({ providedIn: 'root' })
export class EmailHostConfigService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/email-host-configs');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(emailHostConfig: NewEmailHostConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(emailHostConfig);
    return this.http
      .post<RestEmailHostConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(emailHostConfig: IEmailHostConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(emailHostConfig);
    return this.http
      .put<RestEmailHostConfig>(`${this.resourceUrl}/${this.getEmailHostConfigIdentifier(emailHostConfig)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(emailHostConfig: PartialUpdateEmailHostConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(emailHostConfig);
    return this.http
      .patch<RestEmailHostConfig>(`${this.resourceUrl}/${this.getEmailHostConfigIdentifier(emailHostConfig)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestEmailHostConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestEmailHostConfig[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getEmailHostConfigIdentifier(emailHostConfig: Pick<IEmailHostConfig, 'id'>): number {
    return emailHostConfig.id;
  }

  compareEmailHostConfig(o1: Pick<IEmailHostConfig, 'id'> | null, o2: Pick<IEmailHostConfig, 'id'> | null): boolean {
    return o1 && o2 ? this.getEmailHostConfigIdentifier(o1) === this.getEmailHostConfigIdentifier(o2) : o1 === o2;
  }

  addEmailHostConfigToCollectionIfMissing<Type extends Pick<IEmailHostConfig, 'id'>>(
    emailHostConfigCollection: Type[],
    ...emailHostConfigsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const emailHostConfigs: Type[] = emailHostConfigsToCheck.filter(isPresent);
    if (emailHostConfigs.length > 0) {
      const emailHostConfigCollectionIdentifiers = emailHostConfigCollection.map(
        emailHostConfigItem => this.getEmailHostConfigIdentifier(emailHostConfigItem)!
      );
      const emailHostConfigsToAdd = emailHostConfigs.filter(emailHostConfigItem => {
        const emailHostConfigIdentifier = this.getEmailHostConfigIdentifier(emailHostConfigItem);
        if (emailHostConfigCollectionIdentifiers.includes(emailHostConfigIdentifier)) {
          return false;
        }
        emailHostConfigCollectionIdentifiers.push(emailHostConfigIdentifier);
        return true;
      });
      return [...emailHostConfigsToAdd, ...emailHostConfigCollection];
    }
    return emailHostConfigCollection;
  }

  protected convertDateFromClient<T extends IEmailHostConfig | NewEmailHostConfig | PartialUpdateEmailHostConfig>(
    emailHostConfig: T
  ): RestOf<T> {
    return {
      ...emailHostConfig,
      createdDate: emailHostConfig.createdDate?.toJSON() ?? null,
      updatedDate: emailHostConfig.updatedDate?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restEmailHostConfig: RestEmailHostConfig): IEmailHostConfig {
    return {
      ...restEmailHostConfig,
      createdDate: restEmailHostConfig.createdDate ? dayjs(restEmailHostConfig.createdDate) : undefined,
      updatedDate: restEmailHostConfig.updatedDate ? dayjs(restEmailHostConfig.updatedDate) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestEmailHostConfig>): HttpResponse<IEmailHostConfig> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestEmailHostConfig[]>): HttpResponse<IEmailHostConfig[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
