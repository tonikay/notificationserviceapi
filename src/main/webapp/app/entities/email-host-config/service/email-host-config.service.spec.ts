import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IEmailHostConfig } from '../email-host-config.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../email-host-config.test-samples';

import { EmailHostConfigService, RestEmailHostConfig } from './email-host-config.service';

const requireRestSample: RestEmailHostConfig = {
  ...sampleWithRequiredData,
  createdDate: sampleWithRequiredData.createdDate?.toJSON(),
  updatedDate: sampleWithRequiredData.updatedDate?.toJSON(),
};

describe('EmailHostConfig Service', () => {
  let service: EmailHostConfigService;
  let httpMock: HttpTestingController;
  let expectedResult: IEmailHostConfig | IEmailHostConfig[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EmailHostConfigService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a EmailHostConfig', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const emailHostConfig = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(emailHostConfig).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a EmailHostConfig', () => {
      const emailHostConfig = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(emailHostConfig).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a EmailHostConfig', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of EmailHostConfig', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a EmailHostConfig', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addEmailHostConfigToCollectionIfMissing', () => {
      it('should add a EmailHostConfig to an empty array', () => {
        const emailHostConfig: IEmailHostConfig = sampleWithRequiredData;
        expectedResult = service.addEmailHostConfigToCollectionIfMissing([], emailHostConfig);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(emailHostConfig);
      });

      it('should not add a EmailHostConfig to an array that contains it', () => {
        const emailHostConfig: IEmailHostConfig = sampleWithRequiredData;
        const emailHostConfigCollection: IEmailHostConfig[] = [
          {
            ...emailHostConfig,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addEmailHostConfigToCollectionIfMissing(emailHostConfigCollection, emailHostConfig);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a EmailHostConfig to an array that doesn't contain it", () => {
        const emailHostConfig: IEmailHostConfig = sampleWithRequiredData;
        const emailHostConfigCollection: IEmailHostConfig[] = [sampleWithPartialData];
        expectedResult = service.addEmailHostConfigToCollectionIfMissing(emailHostConfigCollection, emailHostConfig);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(emailHostConfig);
      });

      it('should add only unique EmailHostConfig to an array', () => {
        const emailHostConfigArray: IEmailHostConfig[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const emailHostConfigCollection: IEmailHostConfig[] = [sampleWithRequiredData];
        expectedResult = service.addEmailHostConfigToCollectionIfMissing(emailHostConfigCollection, ...emailHostConfigArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const emailHostConfig: IEmailHostConfig = sampleWithRequiredData;
        const emailHostConfig2: IEmailHostConfig = sampleWithPartialData;
        expectedResult = service.addEmailHostConfigToCollectionIfMissing([], emailHostConfig, emailHostConfig2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(emailHostConfig);
        expect(expectedResult).toContain(emailHostConfig2);
      });

      it('should accept null and undefined values', () => {
        const emailHostConfig: IEmailHostConfig = sampleWithRequiredData;
        expectedResult = service.addEmailHostConfigToCollectionIfMissing([], null, emailHostConfig, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(emailHostConfig);
      });

      it('should return initial array if no EmailHostConfig is added', () => {
        const emailHostConfigCollection: IEmailHostConfig[] = [sampleWithRequiredData];
        expectedResult = service.addEmailHostConfigToCollectionIfMissing(emailHostConfigCollection, undefined, null);
        expect(expectedResult).toEqual(emailHostConfigCollection);
      });
    });

    describe('compareEmailHostConfig', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareEmailHostConfig(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareEmailHostConfig(entity1, entity2);
        const compareResult2 = service.compareEmailHostConfig(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareEmailHostConfig(entity1, entity2);
        const compareResult2 = service.compareEmailHostConfig(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareEmailHostConfig(entity1, entity2);
        const compareResult2 = service.compareEmailHostConfig(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
