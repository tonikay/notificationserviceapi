import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EmailHostConfigComponent } from './list/email-host-config.component';
import { EmailHostConfigDetailComponent } from './detail/email-host-config-detail.component';
import { EmailHostConfigUpdateComponent } from './update/email-host-config-update.component';
import { EmailHostConfigDeleteDialogComponent } from './delete/email-host-config-delete-dialog.component';
import { EmailHostConfigRoutingModule } from './route/email-host-config-routing.module';

@NgModule({
  imports: [SharedModule, EmailHostConfigRoutingModule],
  declarations: [
    EmailHostConfigComponent,
    EmailHostConfigDetailComponent,
    EmailHostConfigUpdateComponent,
    EmailHostConfigDeleteDialogComponent,
  ],
})
export class EmailHostConfigModule {}
