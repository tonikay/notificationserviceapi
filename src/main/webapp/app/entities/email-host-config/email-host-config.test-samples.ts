import dayjs from 'dayjs/esm';

import { SecurityProtocol } from 'app/entities/enumerations/security-protocol.model';

import { IEmailHostConfig, NewEmailHostConfig } from './email-host-config.model';

export const sampleWithRequiredData: IEmailHostConfig = {
  id: 25571,
  senderEmail: 'indexing Horizontal',
  host: 'Reverse-engineered drive orchid',
  port: 16389,
  protocol: SecurityProtocol['TLS'],
};

export const sampleWithPartialData: IEmailHostConfig = {
  id: 6332,
  senderEmail: 'end-to-end Algeria',
  host: 'e-commerce Multi-layered Regional',
  port: 21750,
  protocol: SecurityProtocol['TLS'],
  username: 'Greenland Kansas Arkansas',
  updatedDate: dayjs('2022-12-08T04:45'),
};

export const sampleWithFullData: IEmailHostConfig = {
  id: 9307,
  senderEmail: 'Movies services',
  host: 'Tactics responsive web-enabled',
  port: 89125,
  protocol: SecurityProtocol['SSL'],
  username: 'Small Analyst',
  password: 'impactful Borders',
  createdDate: dayjs('2022-12-07T18:54'),
  createdBy: 'Chicken Customer communities',
  updatedDate: dayjs('2022-12-07T16:49'),
  updatedBy: 'Tenge Alabama Gorgeous',
};

export const sampleWithNewData: NewEmailHostConfig = {
  senderEmail: 'sexy models',
  host: 'homogeneous generate',
  port: 75025,
  protocol: SecurityProtocol['SSL'],
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
