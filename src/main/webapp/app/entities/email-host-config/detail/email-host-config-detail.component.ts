import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEmailHostConfig } from '../email-host-config.model';

@Component({
  selector: 'jhi-email-host-config-detail',
  templateUrl: './email-host-config-detail.component.html',
})
export class EmailHostConfigDetailComponent implements OnInit {
  emailHostConfig: IEmailHostConfig | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ emailHostConfig }) => {
      this.emailHostConfig = emailHostConfig;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
