import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EmailHostConfigDetailComponent } from './email-host-config-detail.component';

describe('EmailHostConfig Management Detail Component', () => {
  let comp: EmailHostConfigDetailComponent;
  let fixture: ComponentFixture<EmailHostConfigDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EmailHostConfigDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ emailHostConfig: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(EmailHostConfigDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(EmailHostConfigDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load emailHostConfig on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.emailHostConfig).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
