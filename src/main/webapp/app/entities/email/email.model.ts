export interface IEmail {
  id: number;
  to?: string | null;
  bcc?: string | null;
  cc?: string | null;
  body?: string | null;
  subject?: string | null;
}

export type NewEmail = Omit<IEmail, 'id'> & { id: null };
