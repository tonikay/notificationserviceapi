import { IEmail, NewEmail } from './email.model';

export const sampleWithRequiredData: IEmail = {
  id: 42618,
};

export const sampleWithPartialData: IEmail = {
  id: 31974,
  to: 'BCEAO payment',
  bcc: 'Idaho Producer',
  subject: 'Synergized',
};

export const sampleWithFullData: IEmail = {
  id: 4051,
  to: 'input Home',
  bcc: 'cultivate Cotton',
  cc: 'Tonga Cotton',
  body: 'Cambridgeshire',
  subject: 'Manager Fields',
};

export const sampleWithNewData: NewEmail = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
