import dayjs from 'dayjs/esm';

import { Status } from 'app/entities/enumerations/status.model';

import { IApiToken, NewApiToken } from './api-token.model';

export const sampleWithRequiredData: IApiToken = {
  id: 71155,
};

export const sampleWithPartialData: IApiToken = {
  id: 76426,
  expDate: dayjs('2022-12-08T04:55'),
  createdBy: 'reboot Loan Lesotho',
  updatedBy: 'Chief Rhode Fantastic',
};

export const sampleWithFullData: IApiToken = {
  id: 80760,
  token: 'improvement Open-source',
  status: Status['EXPIRED'],
  expDate: dayjs('2022-12-08T07:39'),
  createdDate: dayjs('2022-12-07T22:19'),
  updatedDate: dayjs('2022-12-08T11:59'),
  createdBy: 'Keyboard payment',
  updatedBy: 'Shoes Usability interface',
};

export const sampleWithNewData: NewApiToken = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
