import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IApiToken } from '../api-token.model';
import { ApiTokenService } from '../service/api-token.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './api-token-delete-dialog.component.html',
})
export class ApiTokenDeleteDialogComponent {
  apiToken?: IApiToken;

  constructor(protected apiTokenService: ApiTokenService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.apiTokenService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
