import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IApiToken } from '../api-token.model';

@Component({
  selector: 'jhi-api-token-detail',
  templateUrl: './api-token-detail.component.html',
})
export class ApiTokenDetailComponent implements OnInit {
  apiToken: IApiToken | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ apiToken }) => {
      this.apiToken = apiToken;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
