import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ApiTokenDetailComponent } from './api-token-detail.component';

describe('ApiToken Management Detail Component', () => {
  let comp: ApiTokenDetailComponent;
  let fixture: ComponentFixture<ApiTokenDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ApiTokenDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ apiToken: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ApiTokenDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ApiTokenDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load apiToken on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.apiToken).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
