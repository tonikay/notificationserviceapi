import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IApiToken } from '../api-token.model';
import { ApiTokenService } from '../service/api-token.service';

@Injectable({ providedIn: 'root' })
export class ApiTokenRoutingResolveService implements Resolve<IApiToken | null> {
  constructor(protected service: ApiTokenService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IApiToken | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((apiToken: HttpResponse<IApiToken>) => {
          if (apiToken.body) {
            return of(apiToken.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
