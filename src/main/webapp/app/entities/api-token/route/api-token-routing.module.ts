import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ApiTokenComponent } from '../list/api-token.component';
import { ApiTokenDetailComponent } from '../detail/api-token-detail.component';
import { ApiTokenUpdateComponent } from '../update/api-token-update.component';
import { ApiTokenRoutingResolveService } from './api-token-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const apiTokenRoute: Routes = [
  {
    path: '',
    component: ApiTokenComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ApiTokenDetailComponent,
    resolve: {
      apiToken: ApiTokenRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ApiTokenUpdateComponent,
    resolve: {
      apiToken: ApiTokenRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ApiTokenUpdateComponent,
    resolve: {
      apiToken: ApiTokenRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(apiTokenRoute)],
  exports: [RouterModule],
})
export class ApiTokenRoutingModule {}
