import dayjs from 'dayjs/esm';
import { Status } from 'app/entities/enumerations/status.model';

export interface IApiToken {
  id: number;
  token?: string | null;
  status?: Status | null;
  expDate?: dayjs.Dayjs | null;
  createdDate?: dayjs.Dayjs | null;
  updatedDate?: dayjs.Dayjs | null;
  createdBy?: string | null;
  updatedBy?: string | null;
}

export type NewApiToken = Omit<IApiToken, 'id'> & { id: null };
