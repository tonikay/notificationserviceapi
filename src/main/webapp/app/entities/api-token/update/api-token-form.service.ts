import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IApiToken, NewApiToken } from '../api-token.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IApiToken for edit and NewApiTokenFormGroupInput for create.
 */
type ApiTokenFormGroupInput = IApiToken | PartialWithRequiredKeyOf<NewApiToken>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IApiToken | NewApiToken> = Omit<T, 'expDate' | 'createdDate' | 'updatedDate'> & {
  expDate?: string | null;
  createdDate?: string | null;
  updatedDate?: string | null;
};

type ApiTokenFormRawValue = FormValueOf<IApiToken>;

type NewApiTokenFormRawValue = FormValueOf<NewApiToken>;

type ApiTokenFormDefaults = Pick<NewApiToken, 'id' | 'expDate' | 'createdDate' | 'updatedDate'>;

type ApiTokenFormGroupContent = {
  id: FormControl<ApiTokenFormRawValue['id'] | NewApiToken['id']>;
  token: FormControl<ApiTokenFormRawValue['token']>;
  status: FormControl<ApiTokenFormRawValue['status']>;
  expDate: FormControl<ApiTokenFormRawValue['expDate']>;
  createdDate: FormControl<ApiTokenFormRawValue['createdDate']>;
  updatedDate: FormControl<ApiTokenFormRawValue['updatedDate']>;
  createdBy: FormControl<ApiTokenFormRawValue['createdBy']>;
  updatedBy: FormControl<ApiTokenFormRawValue['updatedBy']>;
};

export type ApiTokenFormGroup = FormGroup<ApiTokenFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ApiTokenFormService {
  createApiTokenFormGroup(apiToken: ApiTokenFormGroupInput = { id: null }): ApiTokenFormGroup {
    const apiTokenRawValue = this.convertApiTokenToApiTokenRawValue({
      ...this.getFormDefaults(),
      ...apiToken,
    });
    return new FormGroup<ApiTokenFormGroupContent>({
      id: new FormControl(
        { value: apiTokenRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      token: new FormControl(apiTokenRawValue.token),
      status: new FormControl(apiTokenRawValue.status),
      expDate: new FormControl(apiTokenRawValue.expDate),
      createdDate: new FormControl(apiTokenRawValue.createdDate),
      updatedDate: new FormControl(apiTokenRawValue.updatedDate),
      createdBy: new FormControl(apiTokenRawValue.createdBy),
      updatedBy: new FormControl(apiTokenRawValue.updatedBy),
    });
  }

  getApiToken(form: ApiTokenFormGroup): IApiToken | NewApiToken {
    return this.convertApiTokenRawValueToApiToken(form.getRawValue() as ApiTokenFormRawValue | NewApiTokenFormRawValue);
  }

  resetForm(form: ApiTokenFormGroup, apiToken: ApiTokenFormGroupInput): void {
    const apiTokenRawValue = this.convertApiTokenToApiTokenRawValue({ ...this.getFormDefaults(), ...apiToken });
    form.reset(
      {
        ...apiTokenRawValue,
        id: { value: apiTokenRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ApiTokenFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      expDate: currentTime,
      createdDate: currentTime,
      updatedDate: currentTime,
    };
  }

  private convertApiTokenRawValueToApiToken(rawApiToken: ApiTokenFormRawValue | NewApiTokenFormRawValue): IApiToken | NewApiToken {
    return {
      ...rawApiToken,
      expDate: dayjs(rawApiToken.expDate, DATE_TIME_FORMAT),
      createdDate: dayjs(rawApiToken.createdDate, DATE_TIME_FORMAT),
      updatedDate: dayjs(rawApiToken.updatedDate, DATE_TIME_FORMAT),
    };
  }

  private convertApiTokenToApiTokenRawValue(
    apiToken: IApiToken | (Partial<NewApiToken> & ApiTokenFormDefaults)
  ): ApiTokenFormRawValue | PartialWithRequiredKeyOf<NewApiTokenFormRawValue> {
    return {
      ...apiToken,
      expDate: apiToken.expDate ? apiToken.expDate.format(DATE_TIME_FORMAT) : undefined,
      createdDate: apiToken.createdDate ? apiToken.createdDate.format(DATE_TIME_FORMAT) : undefined,
      updatedDate: apiToken.updatedDate ? apiToken.updatedDate.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
