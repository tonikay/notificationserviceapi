import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../api-token.test-samples';

import { ApiTokenFormService } from './api-token-form.service';

describe('ApiToken Form Service', () => {
  let service: ApiTokenFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiTokenFormService);
  });

  describe('Service methods', () => {
    describe('createApiTokenFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createApiTokenFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            token: expect.any(Object),
            status: expect.any(Object),
            expDate: expect.any(Object),
            createdDate: expect.any(Object),
            updatedDate: expect.any(Object),
            createdBy: expect.any(Object),
            updatedBy: expect.any(Object),
          })
        );
      });

      it('passing IApiToken should create a new form with FormGroup', () => {
        const formGroup = service.createApiTokenFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            token: expect.any(Object),
            status: expect.any(Object),
            expDate: expect.any(Object),
            createdDate: expect.any(Object),
            updatedDate: expect.any(Object),
            createdBy: expect.any(Object),
            updatedBy: expect.any(Object),
          })
        );
      });
    });

    describe('getApiToken', () => {
      it('should return NewApiToken for default ApiToken initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createApiTokenFormGroup(sampleWithNewData);

        const apiToken = service.getApiToken(formGroup) as any;

        expect(apiToken).toMatchObject(sampleWithNewData);
      });

      it('should return NewApiToken for empty ApiToken initial value', () => {
        const formGroup = service.createApiTokenFormGroup();

        const apiToken = service.getApiToken(formGroup) as any;

        expect(apiToken).toMatchObject({});
      });

      it('should return IApiToken', () => {
        const formGroup = service.createApiTokenFormGroup(sampleWithRequiredData);

        const apiToken = service.getApiToken(formGroup) as any;

        expect(apiToken).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IApiToken should not enable id FormControl', () => {
        const formGroup = service.createApiTokenFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewApiToken should disable id FormControl', () => {
        const formGroup = service.createApiTokenFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
