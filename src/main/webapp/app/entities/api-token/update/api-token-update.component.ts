import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ApiTokenFormService, ApiTokenFormGroup } from './api-token-form.service';
import { IApiToken } from '../api-token.model';
import { ApiTokenService } from '../service/api-token.service';
import { Status } from 'app/entities/enumerations/status.model';

@Component({
  selector: 'jhi-api-token-update',
  templateUrl: './api-token-update.component.html',
})
export class ApiTokenUpdateComponent implements OnInit {
  isSaving = false;
  apiToken: IApiToken | null = null;
  statusValues = Object.keys(Status);

  editForm: ApiTokenFormGroup = this.apiTokenFormService.createApiTokenFormGroup();

  constructor(
    protected apiTokenService: ApiTokenService,
    protected apiTokenFormService: ApiTokenFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ apiToken }) => {
      this.apiToken = apiToken;
      if (apiToken) {
        this.updateForm(apiToken);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const apiToken = this.apiTokenFormService.getApiToken(this.editForm);
    if (apiToken.id !== null) {
      this.subscribeToSaveResponse(this.apiTokenService.update(apiToken));
    } else {
      this.subscribeToSaveResponse(this.apiTokenService.create(apiToken));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApiToken>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(apiToken: IApiToken): void {
    this.apiToken = apiToken;
    this.apiTokenFormService.resetForm(this.editForm, apiToken);
  }
}
