import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ApiTokenFormService } from './api-token-form.service';
import { ApiTokenService } from '../service/api-token.service';
import { IApiToken } from '../api-token.model';

import { ApiTokenUpdateComponent } from './api-token-update.component';

describe('ApiToken Management Update Component', () => {
  let comp: ApiTokenUpdateComponent;
  let fixture: ComponentFixture<ApiTokenUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let apiTokenFormService: ApiTokenFormService;
  let apiTokenService: ApiTokenService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ApiTokenUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ApiTokenUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ApiTokenUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    apiTokenFormService = TestBed.inject(ApiTokenFormService);
    apiTokenService = TestBed.inject(ApiTokenService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const apiToken: IApiToken = { id: 456 };

      activatedRoute.data = of({ apiToken });
      comp.ngOnInit();

      expect(comp.apiToken).toEqual(apiToken);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IApiToken>>();
      const apiToken = { id: 123 };
      jest.spyOn(apiTokenFormService, 'getApiToken').mockReturnValue(apiToken);
      jest.spyOn(apiTokenService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ apiToken });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: apiToken }));
      saveSubject.complete();

      // THEN
      expect(apiTokenFormService.getApiToken).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(apiTokenService.update).toHaveBeenCalledWith(expect.objectContaining(apiToken));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IApiToken>>();
      const apiToken = { id: 123 };
      jest.spyOn(apiTokenFormService, 'getApiToken').mockReturnValue({ id: null });
      jest.spyOn(apiTokenService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ apiToken: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: apiToken }));
      saveSubject.complete();

      // THEN
      expect(apiTokenFormService.getApiToken).toHaveBeenCalled();
      expect(apiTokenService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IApiToken>>();
      const apiToken = { id: 123 };
      jest.spyOn(apiTokenService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ apiToken });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(apiTokenService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
