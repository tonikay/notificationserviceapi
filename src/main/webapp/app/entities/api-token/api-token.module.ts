import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ApiTokenComponent } from './list/api-token.component';
import { ApiTokenDetailComponent } from './detail/api-token-detail.component';
import { ApiTokenUpdateComponent } from './update/api-token-update.component';
import { ApiTokenDeleteDialogComponent } from './delete/api-token-delete-dialog.component';
import { ApiTokenRoutingModule } from './route/api-token-routing.module';

@NgModule({
  imports: [SharedModule, ApiTokenRoutingModule],
  declarations: [ApiTokenComponent, ApiTokenDetailComponent, ApiTokenUpdateComponent, ApiTokenDeleteDialogComponent],
})
export class ApiTokenModule {}
