import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IApiToken, NewApiToken } from '../api-token.model';

export type PartialUpdateApiToken = Partial<IApiToken> & Pick<IApiToken, 'id'>;

type RestOf<T extends IApiToken | NewApiToken> = Omit<T, 'expDate' | 'createdDate' | 'updatedDate'> & {
  expDate?: string | null;
  createdDate?: string | null;
  updatedDate?: string | null;
};

export type RestApiToken = RestOf<IApiToken>;

export type NewRestApiToken = RestOf<NewApiToken>;

export type PartialUpdateRestApiToken = RestOf<PartialUpdateApiToken>;

export type EntityResponseType = HttpResponse<IApiToken>;
export type EntityArrayResponseType = HttpResponse<IApiToken[]>;

@Injectable({ providedIn: 'root' })
export class ApiTokenService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/api-tokens');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(apiToken: NewApiToken): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(apiToken);
    return this.http
      .post<RestApiToken>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(apiToken: IApiToken): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(apiToken);
    return this.http
      .put<RestApiToken>(`${this.resourceUrl}/${this.getApiTokenIdentifier(apiToken)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(apiToken: PartialUpdateApiToken): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(apiToken);
    return this.http
      .patch<RestApiToken>(`${this.resourceUrl}/${this.getApiTokenIdentifier(apiToken)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestApiToken>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestApiToken[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getApiTokenIdentifier(apiToken: Pick<IApiToken, 'id'>): number {
    return apiToken.id;
  }

  compareApiToken(o1: Pick<IApiToken, 'id'> | null, o2: Pick<IApiToken, 'id'> | null): boolean {
    return o1 && o2 ? this.getApiTokenIdentifier(o1) === this.getApiTokenIdentifier(o2) : o1 === o2;
  }

  addApiTokenToCollectionIfMissing<Type extends Pick<IApiToken, 'id'>>(
    apiTokenCollection: Type[],
    ...apiTokensToCheck: (Type | null | undefined)[]
  ): Type[] {
    const apiTokens: Type[] = apiTokensToCheck.filter(isPresent);
    if (apiTokens.length > 0) {
      const apiTokenCollectionIdentifiers = apiTokenCollection.map(apiTokenItem => this.getApiTokenIdentifier(apiTokenItem)!);
      const apiTokensToAdd = apiTokens.filter(apiTokenItem => {
        const apiTokenIdentifier = this.getApiTokenIdentifier(apiTokenItem);
        if (apiTokenCollectionIdentifiers.includes(apiTokenIdentifier)) {
          return false;
        }
        apiTokenCollectionIdentifiers.push(apiTokenIdentifier);
        return true;
      });
      return [...apiTokensToAdd, ...apiTokenCollection];
    }
    return apiTokenCollection;
  }

  protected convertDateFromClient<T extends IApiToken | NewApiToken | PartialUpdateApiToken>(apiToken: T): RestOf<T> {
    return {
      ...apiToken,
      expDate: apiToken.expDate?.toJSON() ?? null,
      createdDate: apiToken.createdDate?.toJSON() ?? null,
      updatedDate: apiToken.updatedDate?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restApiToken: RestApiToken): IApiToken {
    return {
      ...restApiToken,
      expDate: restApiToken.expDate ? dayjs(restApiToken.expDate) : undefined,
      createdDate: restApiToken.createdDate ? dayjs(restApiToken.createdDate) : undefined,
      updatedDate: restApiToken.updatedDate ? dayjs(restApiToken.updatedDate) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestApiToken>): HttpResponse<IApiToken> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestApiToken[]>): HttpResponse<IApiToken[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
