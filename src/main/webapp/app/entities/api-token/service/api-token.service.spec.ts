import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IApiToken } from '../api-token.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../api-token.test-samples';

import { ApiTokenService, RestApiToken } from './api-token.service';

const requireRestSample: RestApiToken = {
  ...sampleWithRequiredData,
  expDate: sampleWithRequiredData.expDate?.toJSON(),
  createdDate: sampleWithRequiredData.createdDate?.toJSON(),
  updatedDate: sampleWithRequiredData.updatedDate?.toJSON(),
};

describe('ApiToken Service', () => {
  let service: ApiTokenService;
  let httpMock: HttpTestingController;
  let expectedResult: IApiToken | IApiToken[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ApiTokenService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a ApiToken', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const apiToken = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(apiToken).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ApiToken', () => {
      const apiToken = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(apiToken).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ApiToken', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ApiToken', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a ApiToken', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addApiTokenToCollectionIfMissing', () => {
      it('should add a ApiToken to an empty array', () => {
        const apiToken: IApiToken = sampleWithRequiredData;
        expectedResult = service.addApiTokenToCollectionIfMissing([], apiToken);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(apiToken);
      });

      it('should not add a ApiToken to an array that contains it', () => {
        const apiToken: IApiToken = sampleWithRequiredData;
        const apiTokenCollection: IApiToken[] = [
          {
            ...apiToken,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addApiTokenToCollectionIfMissing(apiTokenCollection, apiToken);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ApiToken to an array that doesn't contain it", () => {
        const apiToken: IApiToken = sampleWithRequiredData;
        const apiTokenCollection: IApiToken[] = [sampleWithPartialData];
        expectedResult = service.addApiTokenToCollectionIfMissing(apiTokenCollection, apiToken);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(apiToken);
      });

      it('should add only unique ApiToken to an array', () => {
        const apiTokenArray: IApiToken[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const apiTokenCollection: IApiToken[] = [sampleWithRequiredData];
        expectedResult = service.addApiTokenToCollectionIfMissing(apiTokenCollection, ...apiTokenArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const apiToken: IApiToken = sampleWithRequiredData;
        const apiToken2: IApiToken = sampleWithPartialData;
        expectedResult = service.addApiTokenToCollectionIfMissing([], apiToken, apiToken2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(apiToken);
        expect(expectedResult).toContain(apiToken2);
      });

      it('should accept null and undefined values', () => {
        const apiToken: IApiToken = sampleWithRequiredData;
        expectedResult = service.addApiTokenToCollectionIfMissing([], null, apiToken, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(apiToken);
      });

      it('should return initial array if no ApiToken is added', () => {
        const apiTokenCollection: IApiToken[] = [sampleWithRequiredData];
        expectedResult = service.addApiTokenToCollectionIfMissing(apiTokenCollection, undefined, null);
        expect(expectedResult).toEqual(apiTokenCollection);
      });
    });

    describe('compareApiToken', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareApiToken(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareApiToken(entity1, entity2);
        const compareResult2 = service.compareApiToken(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareApiToken(entity1, entity2);
        const compareResult2 = service.compareApiToken(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareApiToken(entity1, entity2);
        const compareResult2 = service.compareApiToken(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
