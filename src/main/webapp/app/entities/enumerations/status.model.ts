export enum Status {
  EXPIRED = 'EXPIRED',

  ACTIVE = 'ACTIVE',

  INACTIVE = 'INACTIVE',
}
