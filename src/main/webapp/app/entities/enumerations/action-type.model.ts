export enum ActionType {
  SMS = 'SMS',

  EMAIL = 'EMAIL',

  PUSH_NOTIFICATION = 'PUSH_NOTIFICATION',
}
