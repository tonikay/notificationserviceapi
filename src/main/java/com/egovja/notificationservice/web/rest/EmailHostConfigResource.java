package com.egovja.notificationservice.web.rest;

import com.egovja.notificationservice.domain.EmailHostConfig;
import com.egovja.notificationservice.repository.EmailHostConfigRepository;
import com.egovja.notificationservice.service.EmailHostConfigService;
import com.egovja.notificationservice.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.egovja.notificationservice.domain.EmailHostConfig}.
 */
@RestController
@RequestMapping("/api")
public class EmailHostConfigResource {

    private final Logger log = LoggerFactory.getLogger(EmailHostConfigResource.class);

    private static final String ENTITY_NAME = "emailHostConfig";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmailHostConfigService emailHostConfigService;

    private final EmailHostConfigRepository emailHostConfigRepository;

    public EmailHostConfigResource(EmailHostConfigService emailHostConfigService, EmailHostConfigRepository emailHostConfigRepository) {
        this.emailHostConfigService = emailHostConfigService;
        this.emailHostConfigRepository = emailHostConfigRepository;
    }

    /**
     * {@code POST  /email-host-configs} : Create a new emailHostConfig.
     *
     * @param emailHostConfig the emailHostConfig to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new emailHostConfig, or with status {@code 400 (Bad Request)} if the emailHostConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/email-host-configs")
    public ResponseEntity<EmailHostConfig> createEmailHostConfig(@Valid @RequestBody EmailHostConfig emailHostConfig)
        throws URISyntaxException {
        log.debug("REST request to save EmailHostConfig : {}", emailHostConfig);
        if (emailHostConfig.getId() != null) {
            throw new BadRequestAlertException("A new emailHostConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmailHostConfig result = emailHostConfigService.save(emailHostConfig);
        return ResponseEntity
            .created(new URI("/api/email-host-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /email-host-configs/:id} : Updates an existing emailHostConfig.
     *
     * @param id the id of the emailHostConfig to save.
     * @param emailHostConfig the emailHostConfig to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated emailHostConfig,
     * or with status {@code 400 (Bad Request)} if the emailHostConfig is not valid,
     * or with status {@code 500 (Internal Server Error)} if the emailHostConfig couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/email-host-configs/{id}")
    public ResponseEntity<EmailHostConfig> updateEmailHostConfig(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody EmailHostConfig emailHostConfig
    ) throws URISyntaxException {
        log.debug("REST request to update EmailHostConfig : {}, {}", id, emailHostConfig);
        if (emailHostConfig.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, emailHostConfig.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!emailHostConfigRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EmailHostConfig result = emailHostConfigService.update(emailHostConfig);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, emailHostConfig.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /email-host-configs/:id} : Partial updates given fields of an existing emailHostConfig, field will ignore if it is null
     *
     * @param id the id of the emailHostConfig to save.
     * @param emailHostConfig the emailHostConfig to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated emailHostConfig,
     * or with status {@code 400 (Bad Request)} if the emailHostConfig is not valid,
     * or with status {@code 404 (Not Found)} if the emailHostConfig is not found,
     * or with status {@code 500 (Internal Server Error)} if the emailHostConfig couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/email-host-configs/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<EmailHostConfig> partialUpdateEmailHostConfig(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody EmailHostConfig emailHostConfig
    ) throws URISyntaxException {
        log.debug("REST request to partial update EmailHostConfig partially : {}, {}", id, emailHostConfig);
        if (emailHostConfig.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, emailHostConfig.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!emailHostConfigRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EmailHostConfig> result = emailHostConfigService.partialUpdate(emailHostConfig);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, emailHostConfig.getId().toString())
        );
    }

    /**
     * {@code GET  /email-host-configs} : get all the emailHostConfigs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of emailHostConfigs in body.
     */
    @GetMapping("/email-host-configs")
    public ResponseEntity<List<EmailHostConfig>> getAllEmailHostConfigs(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of EmailHostConfigs");
        Page<EmailHostConfig> page = emailHostConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /email-host-configs/:id} : get the "id" emailHostConfig.
     *
     * @param id the id of the emailHostConfig to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the emailHostConfig, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/email-host-configs/{id}")
    public ResponseEntity<EmailHostConfig> getEmailHostConfig(@PathVariable Long id) {
        log.debug("REST request to get EmailHostConfig : {}", id);
        Optional<EmailHostConfig> emailHostConfig = emailHostConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(emailHostConfig);
    }

    /**
     * {@code DELETE  /email-host-configs/:id} : delete the "id" emailHostConfig.
     *
     * @param id the id of the emailHostConfig to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/email-host-configs/{id}")
    public ResponseEntity<Void> deleteEmailHostConfig(@PathVariable Long id) {
        log.debug("REST request to delete EmailHostConfig : {}", id);
        emailHostConfigService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
