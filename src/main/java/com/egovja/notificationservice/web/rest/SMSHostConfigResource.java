package com.egovja.notificationservice.web.rest;

import com.egovja.notificationservice.domain.SMSHostConfig;
import com.egovja.notificationservice.repository.SMSHostConfigRepository;
import com.egovja.notificationservice.service.SMSHostConfigService;
import com.egovja.notificationservice.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.egovja.notificationservice.domain.SMSHostConfig}.
 */
@RestController
@RequestMapping("/api")
public class SMSHostConfigResource {

    private final Logger log = LoggerFactory.getLogger(SMSHostConfigResource.class);

    private static final String ENTITY_NAME = "sMSHostConfig";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SMSHostConfigService sMSHostConfigService;

    private final SMSHostConfigRepository sMSHostConfigRepository;

    public SMSHostConfigResource(SMSHostConfigService sMSHostConfigService, SMSHostConfigRepository sMSHostConfigRepository) {
        this.sMSHostConfigService = sMSHostConfigService;
        this.sMSHostConfigRepository = sMSHostConfigRepository;
    }

    /**
     * {@code POST  /sms-host-configs} : Create a new sMSHostConfig.
     *
     * @param sMSHostConfig the sMSHostConfig to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sMSHostConfig, or with status {@code 400 (Bad Request)} if the sMSHostConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sms-host-configs")
    public ResponseEntity<SMSHostConfig> createSMSHostConfig(@Valid @RequestBody SMSHostConfig sMSHostConfig) throws URISyntaxException {
        log.debug("REST request to save SMSHostConfig : {}", sMSHostConfig);
        if (sMSHostConfig.getId() != null) {
            throw new BadRequestAlertException("A new sMSHostConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SMSHostConfig result = sMSHostConfigService.save(sMSHostConfig);
        return ResponseEntity
            .created(new URI("/api/sms-host-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sms-host-configs/:id} : Updates an existing sMSHostConfig.
     *
     * @param id the id of the sMSHostConfig to save.
     * @param sMSHostConfig the sMSHostConfig to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sMSHostConfig,
     * or with status {@code 400 (Bad Request)} if the sMSHostConfig is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sMSHostConfig couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sms-host-configs/{id}")
    public ResponseEntity<SMSHostConfig> updateSMSHostConfig(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody SMSHostConfig sMSHostConfig
    ) throws URISyntaxException {
        log.debug("REST request to update SMSHostConfig : {}, {}", id, sMSHostConfig);
        if (sMSHostConfig.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sMSHostConfig.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sMSHostConfigRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SMSHostConfig result = sMSHostConfigService.update(sMSHostConfig);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, sMSHostConfig.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /sms-host-configs/:id} : Partial updates given fields of an existing sMSHostConfig, field will ignore if it is null
     *
     * @param id the id of the sMSHostConfig to save.
     * @param sMSHostConfig the sMSHostConfig to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sMSHostConfig,
     * or with status {@code 400 (Bad Request)} if the sMSHostConfig is not valid,
     * or with status {@code 404 (Not Found)} if the sMSHostConfig is not found,
     * or with status {@code 500 (Internal Server Error)} if the sMSHostConfig couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/sms-host-configs/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SMSHostConfig> partialUpdateSMSHostConfig(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody SMSHostConfig sMSHostConfig
    ) throws URISyntaxException {
        log.debug("REST request to partial update SMSHostConfig partially : {}, {}", id, sMSHostConfig);
        if (sMSHostConfig.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sMSHostConfig.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sMSHostConfigRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SMSHostConfig> result = sMSHostConfigService.partialUpdate(sMSHostConfig);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, sMSHostConfig.getId().toString())
        );
    }

    /**
     * {@code GET  /sms-host-configs} : get all the sMSHostConfigs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sMSHostConfigs in body.
     */
    @GetMapping("/sms-host-configs")
    public ResponseEntity<List<SMSHostConfig>> getAllSMSHostConfigs(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of SMSHostConfigs");
        Page<SMSHostConfig> page = sMSHostConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /sms-host-configs/:id} : get the "id" sMSHostConfig.
     *
     * @param id the id of the sMSHostConfig to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sMSHostConfig, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sms-host-configs/{id}")
    public ResponseEntity<SMSHostConfig> getSMSHostConfig(@PathVariable Long id) {
        log.debug("REST request to get SMSHostConfig : {}", id);
        Optional<SMSHostConfig> sMSHostConfig = sMSHostConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sMSHostConfig);
    }

    /**
     * {@code DELETE  /sms-host-configs/:id} : delete the "id" sMSHostConfig.
     *
     * @param id the id of the sMSHostConfig to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sms-host-configs/{id}")
    public ResponseEntity<Void> deleteSMSHostConfig(@PathVariable Long id) {
        log.debug("REST request to delete SMSHostConfig : {}", id);
        sMSHostConfigService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
