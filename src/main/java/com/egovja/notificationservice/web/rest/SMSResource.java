package com.egovja.notificationservice.web.rest;

import com.egovja.notificationservice.domain.SMS;
import com.egovja.notificationservice.repository.SMSRepository;
import com.egovja.notificationservice.service.SMSService;
import com.egovja.notificationservice.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.egovja.notificationservice.domain.SMS}.
 */
@RestController
@RequestMapping("/api")
public class SMSResource {

    private final Logger log = LoggerFactory.getLogger(SMSResource.class);

    private static final String ENTITY_NAME = "sMS";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SMSService sMSService;

    private final SMSRepository sMSRepository;

    public SMSResource(SMSService sMSService, SMSRepository sMSRepository) {
        this.sMSService = sMSService;
        this.sMSRepository = sMSRepository;
    }

    /**
     * {@code POST  /sms} : Create a new sMS.
     *
     * @param sMS the sMS to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sMS, or with status {@code 400 (Bad Request)} if the sMS has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sms")
    public ResponseEntity<SMS> createSMS(@RequestBody SMS sMS) throws URISyntaxException {
        log.debug("REST request to save SMS : {}", sMS);
        if (sMS.getId() != null) {
            throw new BadRequestAlertException("A new sMS cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SMS result = sMSService.save(sMS);
        return ResponseEntity
            .created(new URI("/api/sms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sms/:id} : Updates an existing sMS.
     *
     * @param id the id of the sMS to save.
     * @param sMS the sMS to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sMS,
     * or with status {@code 400 (Bad Request)} if the sMS is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sMS couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sms/{id}")
    public ResponseEntity<SMS> updateSMS(@PathVariable(value = "id", required = false) final Long id, @RequestBody SMS sMS)
        throws URISyntaxException {
        log.debug("REST request to update SMS : {}, {}", id, sMS);
        if (sMS.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sMS.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sMSRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SMS result = sMSService.update(sMS);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, sMS.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /sms/:id} : Partial updates given fields of an existing sMS, field will ignore if it is null
     *
     * @param id the id of the sMS to save.
     * @param sMS the sMS to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sMS,
     * or with status {@code 400 (Bad Request)} if the sMS is not valid,
     * or with status {@code 404 (Not Found)} if the sMS is not found,
     * or with status {@code 500 (Internal Server Error)} if the sMS couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/sms/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SMS> partialUpdateSMS(@PathVariable(value = "id", required = false) final Long id, @RequestBody SMS sMS)
        throws URISyntaxException {
        log.debug("REST request to partial update SMS partially : {}, {}", id, sMS);
        if (sMS.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sMS.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sMSRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SMS> result = sMSService.partialUpdate(sMS);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, sMS.getId().toString())
        );
    }

    /**
     * {@code GET  /sms} : get all the sMS.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sMS in body.
     */
    @GetMapping("/sms")
    public ResponseEntity<List<SMS>> getAllSMS(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of SMS");
        Page<SMS> page = sMSService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /sms/:id} : get the "id" sMS.
     *
     * @param id the id of the sMS to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sMS, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sms/{id}")
    public ResponseEntity<SMS> getSMS(@PathVariable Long id) {
        log.debug("REST request to get SMS : {}", id);
        Optional<SMS> sMS = sMSService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sMS);
    }

    /**
     * {@code DELETE  /sms/:id} : delete the "id" sMS.
     *
     * @param id the id of the sMS to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sms/{id}")
    public ResponseEntity<Void> deleteSMS(@PathVariable Long id) {
        log.debug("REST request to delete SMS : {}", id);
        sMSService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
