package com.egovja.notificationservice.web.rest;

import com.egovja.notificationservice.domain.ApiToken;
import com.egovja.notificationservice.repository.ApiTokenRepository;
import com.egovja.notificationservice.service.ApiTokenService;
import com.egovja.notificationservice.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.egovja.notificationservice.domain.ApiToken}.
 */
@RestController
@RequestMapping("/api")
public class ApiTokenResource {

    private final Logger log = LoggerFactory.getLogger(ApiTokenResource.class);

    private static final String ENTITY_NAME = "apiToken";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApiTokenService apiTokenService;

    private final ApiTokenRepository apiTokenRepository;

    public ApiTokenResource(ApiTokenService apiTokenService, ApiTokenRepository apiTokenRepository) {
        this.apiTokenService = apiTokenService;
        this.apiTokenRepository = apiTokenRepository;
    }

    /**
     * {@code POST  /api-tokens} : Create a new apiToken.
     *
     * @param apiToken the apiToken to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new apiToken, or with status {@code 400 (Bad Request)} if the apiToken has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api-tokens")
    public ResponseEntity<ApiToken> createApiToken(@RequestBody ApiToken apiToken) throws URISyntaxException {
        log.debug("REST request to save ApiToken : {}", apiToken);
        if (apiToken.getId() != null) {
            throw new BadRequestAlertException("A new apiToken cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ApiToken result = apiTokenService.save(apiToken);
        return ResponseEntity
            .created(new URI("/api/api-tokens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /api-tokens/:id} : Updates an existing apiToken.
     *
     * @param id the id of the apiToken to save.
     * @param apiToken the apiToken to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated apiToken,
     * or with status {@code 400 (Bad Request)} if the apiToken is not valid,
     * or with status {@code 500 (Internal Server Error)} if the apiToken couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api-tokens/{id}")
    public ResponseEntity<ApiToken> updateApiToken(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ApiToken apiToken
    ) throws URISyntaxException {
        log.debug("REST request to update ApiToken : {}, {}", id, apiToken);
        if (apiToken.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, apiToken.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!apiTokenRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ApiToken result = apiTokenService.update(apiToken);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, apiToken.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /api-tokens/:id} : Partial updates given fields of an existing apiToken, field will ignore if it is null
     *
     * @param id the id of the apiToken to save.
     * @param apiToken the apiToken to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated apiToken,
     * or with status {@code 400 (Bad Request)} if the apiToken is not valid,
     * or with status {@code 404 (Not Found)} if the apiToken is not found,
     * or with status {@code 500 (Internal Server Error)} if the apiToken couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/api-tokens/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ApiToken> partialUpdateApiToken(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ApiToken apiToken
    ) throws URISyntaxException {
        log.debug("REST request to partial update ApiToken partially : {}, {}", id, apiToken);
        if (apiToken.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, apiToken.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!apiTokenRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ApiToken> result = apiTokenService.partialUpdate(apiToken);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, apiToken.getId().toString())
        );
    }

    /**
     * {@code GET  /api-tokens} : get all the apiTokens.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of apiTokens in body.
     */
    @GetMapping("/api-tokens")
    public ResponseEntity<List<ApiToken>> getAllApiTokens(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ApiTokens");
        Page<ApiToken> page = apiTokenService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /api-tokens/:id} : get the "id" apiToken.
     *
     * @param id the id of the apiToken to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the apiToken, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api-tokens/{id}")
    public ResponseEntity<ApiToken> getApiToken(@PathVariable Long id) {
        log.debug("REST request to get ApiToken : {}", id);
        Optional<ApiToken> apiToken = apiTokenService.findOne(id);
        return ResponseUtil.wrapOrNotFound(apiToken);
    }

    /**
     * {@code DELETE  /api-tokens/:id} : delete the "id" apiToken.
     *
     * @param id the id of the apiToken to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api-tokens/{id}")
    public ResponseEntity<Void> deleteApiToken(@PathVariable Long id) {
        log.debug("REST request to delete ApiToken : {}", id);
        apiTokenService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
