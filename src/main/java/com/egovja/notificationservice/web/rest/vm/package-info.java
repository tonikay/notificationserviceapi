/**
 * View Models used by Spring MVC REST controllers.
 */
package com.egovja.notificationservice.web.rest.vm;
