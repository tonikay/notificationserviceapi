package com.egovja.notificationservice.web.rest;

import com.egovja.notificationservice.domain.MDA;
import com.egovja.notificationservice.repository.MDARepository;
import com.egovja.notificationservice.service.MDAService;
import com.egovja.notificationservice.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.egovja.notificationservice.domain.MDA}.
 */
@RestController
@RequestMapping("/api")
public class MDAResource {

    private final Logger log = LoggerFactory.getLogger(MDAResource.class);

    private static final String ENTITY_NAME = "mDA";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MDAService mDAService;

    private final MDARepository mDARepository;

    public MDAResource(MDAService mDAService, MDARepository mDARepository) {
        this.mDAService = mDAService;
        this.mDARepository = mDARepository;
    }

    /**
     * {@code POST  /mdas} : Create a new mDA.
     *
     * @param mDA the mDA to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mDA, or with status {@code 400 (Bad Request)} if the mDA has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mdas")
    public ResponseEntity<MDA> createMDA(@RequestBody MDA mDA) throws URISyntaxException {
        log.debug("REST request to save MDA : {}", mDA);
        if (mDA.getId() != null) {
            throw new BadRequestAlertException("A new mDA cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MDA result = mDAService.save(mDA);
        return ResponseEntity
            .created(new URI("/api/mdas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mdas/:id} : Updates an existing mDA.
     *
     * @param id the id of the mDA to save.
     * @param mDA the mDA to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mDA,
     * or with status {@code 400 (Bad Request)} if the mDA is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mDA couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mdas/{id}")
    public ResponseEntity<MDA> updateMDA(@PathVariable(value = "id", required = false) final Long id, @RequestBody MDA mDA)
        throws URISyntaxException {
        log.debug("REST request to update MDA : {}, {}", id, mDA);
        if (mDA.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, mDA.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!mDARepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MDA result = mDAService.update(mDA);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mDA.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /mdas/:id} : Partial updates given fields of an existing mDA, field will ignore if it is null
     *
     * @param id the id of the mDA to save.
     * @param mDA the mDA to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mDA,
     * or with status {@code 400 (Bad Request)} if the mDA is not valid,
     * or with status {@code 404 (Not Found)} if the mDA is not found,
     * or with status {@code 500 (Internal Server Error)} if the mDA couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/mdas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MDA> partialUpdateMDA(@PathVariable(value = "id", required = false) final Long id, @RequestBody MDA mDA)
        throws URISyntaxException {
        log.debug("REST request to partial update MDA partially : {}, {}", id, mDA);
        if (mDA.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, mDA.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!mDARepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MDA> result = mDAService.partialUpdate(mDA);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mDA.getId().toString())
        );
    }

    /**
     * {@code GET  /mdas} : get all the mDAS.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mDAS in body.
     */
    @GetMapping("/mdas")
    public ResponseEntity<List<MDA>> getAllMDAS(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of MDAS");
        Page<MDA> page = mDAService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /mdas/:id} : get the "id" mDA.
     *
     * @param id the id of the mDA to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mDA, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mdas/{id}")
    public ResponseEntity<MDA> getMDA(@PathVariable Long id) {
        log.debug("REST request to get MDA : {}", id);
        Optional<MDA> mDA = mDAService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mDA);
    }

    /**
     * {@code DELETE  /mdas/:id} : delete the "id" mDA.
     *
     * @param id the id of the mDA to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mdas/{id}")
    public ResponseEntity<Void> deleteMDA(@PathVariable Long id) {
        log.debug("REST request to delete MDA : {}", id);
        mDAService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
