package com.egovja.notificationservice.web.rest;

import com.egovja.notificationservice.domain.AppLogging;
import com.egovja.notificationservice.repository.AppLoggingRepository;
import com.egovja.notificationservice.service.AppLoggingService;
import com.egovja.notificationservice.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.egovja.notificationservice.domain.AppLogging}.
 */
@RestController
@RequestMapping("/api")
public class AppLoggingResource {

    private final Logger log = LoggerFactory.getLogger(AppLoggingResource.class);

    private static final String ENTITY_NAME = "appLogging";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AppLoggingService appLoggingService;

    private final AppLoggingRepository appLoggingRepository;

    public AppLoggingResource(AppLoggingService appLoggingService, AppLoggingRepository appLoggingRepository) {
        this.appLoggingService = appLoggingService;
        this.appLoggingRepository = appLoggingRepository;
    }

    /**
     * {@code POST  /app-loggings} : Create a new appLogging.
     *
     * @param appLogging the appLogging to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new appLogging, or with status {@code 400 (Bad Request)} if the appLogging has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/app-loggings")
    public ResponseEntity<AppLogging> createAppLogging(@RequestBody AppLogging appLogging) throws URISyntaxException {
        log.debug("REST request to save AppLogging : {}", appLogging);
        if (appLogging.getId() != null) {
            throw new BadRequestAlertException("A new appLogging cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AppLogging result = appLoggingService.save(appLogging);
        return ResponseEntity
            .created(new URI("/api/app-loggings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /app-loggings/:id} : Updates an existing appLogging.
     *
     * @param id the id of the appLogging to save.
     * @param appLogging the appLogging to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated appLogging,
     * or with status {@code 400 (Bad Request)} if the appLogging is not valid,
     * or with status {@code 500 (Internal Server Error)} if the appLogging couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/app-loggings/{id}")
    public ResponseEntity<AppLogging> updateAppLogging(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AppLogging appLogging
    ) throws URISyntaxException {
        log.debug("REST request to update AppLogging : {}, {}", id, appLogging);
        if (appLogging.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, appLogging.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!appLoggingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AppLogging result = appLoggingService.update(appLogging);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, appLogging.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /app-loggings/:id} : Partial updates given fields of an existing appLogging, field will ignore if it is null
     *
     * @param id the id of the appLogging to save.
     * @param appLogging the appLogging to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated appLogging,
     * or with status {@code 400 (Bad Request)} if the appLogging is not valid,
     * or with status {@code 404 (Not Found)} if the appLogging is not found,
     * or with status {@code 500 (Internal Server Error)} if the appLogging couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/app-loggings/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AppLogging> partialUpdateAppLogging(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AppLogging appLogging
    ) throws URISyntaxException {
        log.debug("REST request to partial update AppLogging partially : {}, {}", id, appLogging);
        if (appLogging.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, appLogging.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!appLoggingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AppLogging> result = appLoggingService.partialUpdate(appLogging);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, appLogging.getId().toString())
        );
    }

    /**
     * {@code GET  /app-loggings} : get all the appLoggings.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of appLoggings in body.
     */
    @GetMapping("/app-loggings")
    public ResponseEntity<List<AppLogging>> getAllAppLoggings(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of AppLoggings");
        Page<AppLogging> page = appLoggingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /app-loggings/:id} : get the "id" appLogging.
     *
     * @param id the id of the appLogging to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the appLogging, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/app-loggings/{id}")
    public ResponseEntity<AppLogging> getAppLogging(@PathVariable Long id) {
        log.debug("REST request to get AppLogging : {}", id);
        Optional<AppLogging> appLogging = appLoggingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(appLogging);
    }

    /**
     * {@code DELETE  /app-loggings/:id} : delete the "id" appLogging.
     *
     * @param id the id of the appLogging to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/app-loggings/{id}")
    public ResponseEntity<Void> deleteAppLogging(@PathVariable Long id) {
        log.debug("REST request to delete AppLogging : {}", id);
        appLoggingService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
