package com.egovja.notificationservice.domain.enumeration;

/**
 * The SecurityProtocol enumeration.
 */
public enum SecurityProtocol {
    TLS,
    SSL,
}
