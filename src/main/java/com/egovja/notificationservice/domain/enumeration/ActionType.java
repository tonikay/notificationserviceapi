package com.egovja.notificationservice.domain.enumeration;

/**
 * The ActionType enumeration.
 */
public enum ActionType {
    SMS,
    EMAIL,
    PUSH_NOTIFICATION,
}
