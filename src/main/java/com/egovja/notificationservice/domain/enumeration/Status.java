package com.egovja.notificationservice.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    EXPIRED,
    ACTIVE,
    INACTIVE,
}
