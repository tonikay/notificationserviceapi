package com.egovja.notificationservice.domain;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Email.
 */
@Entity
@Table(name = "email")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Email implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "jhi_to")
    private String to;

    @Column(name = "bcc")
    private String bcc;

    @Column(name = "cc")
    private String cc;

    @Column(name = "body")
    private String body;

    @Column(name = "subject")
    private String subject;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Email id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTo() {
        return this.to;
    }

    public Email to(String to) {
        this.setTo(to);
        return this;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBcc() {
        return this.bcc;
    }

    public Email bcc(String bcc) {
        this.setBcc(bcc);
        return this;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getCc() {
        return this.cc;
    }

    public Email cc(String cc) {
        this.setCc(cc);
        return this;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBody() {
        return this.body;
    }

    public Email body(String body) {
        this.setBody(body);
        return this;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return this.subject;
    }

    public Email subject(String subject) {
        this.setSubject(subject);
        return this;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Email)) {
            return false;
        }
        return id != null && id.equals(((Email) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Email{" +
            "id=" + getId() +
            ", to='" + getTo() + "'" +
            ", bcc='" + getBcc() + "'" +
            ", cc='" + getCc() + "'" +
            ", body='" + getBody() + "'" +
            ", subject='" + getSubject() + "'" +
            "}";
    }
}
