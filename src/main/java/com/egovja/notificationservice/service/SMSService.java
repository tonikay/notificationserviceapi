package com.egovja.notificationservice.service;

import com.egovja.notificationservice.domain.SMS;
import com.egovja.notificationservice.repository.SMSRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link SMS}.
 */
@Service
@Transactional
public class SMSService {

    private final Logger log = LoggerFactory.getLogger(SMSService.class);

    private final SMSRepository sMSRepository;

    public SMSService(SMSRepository sMSRepository) {
        this.sMSRepository = sMSRepository;
    }

    /**
     * Save a sMS.
     *
     * @param sMS the entity to save.
     * @return the persisted entity.
     */
    public SMS save(SMS sMS) {
        log.debug("Request to save SMS : {}", sMS);
        return sMSRepository.save(sMS);
    }

    /**
     * Update a sMS.
     *
     * @param sMS the entity to save.
     * @return the persisted entity.
     */
    public SMS update(SMS sMS) {
        log.debug("Request to update SMS : {}", sMS);
        return sMSRepository.save(sMS);
    }

    /**
     * Partially update a sMS.
     *
     * @param sMS the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<SMS> partialUpdate(SMS sMS) {
        log.debug("Request to partially update SMS : {}", sMS);

        return sMSRepository
            .findById(sMS.getId())
            .map(existingSMS -> {
                if (sMS.getRecipent() != null) {
                    existingSMS.setRecipent(sMS.getRecipent());
                }
                if (sMS.getMessage() != null) {
                    existingSMS.setMessage(sMS.getMessage());
                }

                return existingSMS;
            })
            .map(sMSRepository::save);
    }

    /**
     * Get all the sMS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SMS> findAll(Pageable pageable) {
        log.debug("Request to get all SMS");
        return sMSRepository.findAll(pageable);
    }

    /**
     * Get one sMS by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SMS> findOne(Long id) {
        log.debug("Request to get SMS : {}", id);
        return sMSRepository.findById(id);
    }

    /**
     * Delete the sMS by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SMS : {}", id);
        sMSRepository.deleteById(id);
    }
}
