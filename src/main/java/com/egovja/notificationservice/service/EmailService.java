package com.egovja.notificationservice.service;

import com.egovja.notificationservice.domain.Email;
import com.egovja.notificationservice.repository.EmailRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Email}.
 */
@Service
@Transactional
public class EmailService {

    private final Logger log = LoggerFactory.getLogger(EmailService.class);

    private final EmailRepository emailRepository;

    public EmailService(EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    /**
     * Save a email.
     *
     * @param email the entity to save.
     * @return the persisted entity.
     */
    public Email save(Email email) {
        log.debug("Request to save Email : {}", email);
        return emailRepository.save(email);
    }

    /**
     * Update a email.
     *
     * @param email the entity to save.
     * @return the persisted entity.
     */
    public Email update(Email email) {
        log.debug("Request to update Email : {}", email);
        return emailRepository.save(email);
    }

    /**
     * Partially update a email.
     *
     * @param email the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Email> partialUpdate(Email email) {
        log.debug("Request to partially update Email : {}", email);

        return emailRepository
            .findById(email.getId())
            .map(existingEmail -> {
                if (email.getTo() != null) {
                    existingEmail.setTo(email.getTo());
                }
                if (email.getBcc() != null) {
                    existingEmail.setBcc(email.getBcc());
                }
                if (email.getCc() != null) {
                    existingEmail.setCc(email.getCc());
                }
                if (email.getBody() != null) {
                    existingEmail.setBody(email.getBody());
                }
                if (email.getSubject() != null) {
                    existingEmail.setSubject(email.getSubject());
                }

                return existingEmail;
            })
            .map(emailRepository::save);
    }

    /**
     * Get all the emails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Email> findAll(Pageable pageable) {
        log.debug("Request to get all Emails");
        return emailRepository.findAll(pageable);
    }

    /**
     * Get one email by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Email> findOne(Long id) {
        log.debug("Request to get Email : {}", id);
        return emailRepository.findById(id);
    }

    /**
     * Delete the email by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Email : {}", id);
        emailRepository.deleteById(id);
    }
}
