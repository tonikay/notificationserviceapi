package com.egovja.notificationservice.service;

import com.egovja.notificationservice.domain.MDA;
import com.egovja.notificationservice.repository.MDARepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link MDA}.
 */
@Service
@Transactional
public class MDAService {

    private final Logger log = LoggerFactory.getLogger(MDAService.class);

    private final MDARepository mDARepository;

    public MDAService(MDARepository mDARepository) {
        this.mDARepository = mDARepository;
    }

    /**
     * Save a mDA.
     *
     * @param mDA the entity to save.
     * @return the persisted entity.
     */
    public MDA save(MDA mDA) {
        log.debug("Request to save MDA : {}", mDA);
        return mDARepository.save(mDA);
    }

    /**
     * Update a mDA.
     *
     * @param mDA the entity to save.
     * @return the persisted entity.
     */
    public MDA update(MDA mDA) {
        log.debug("Request to update MDA : {}", mDA);
        return mDARepository.save(mDA);
    }

    /**
     * Partially update a mDA.
     *
     * @param mDA the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MDA> partialUpdate(MDA mDA) {
        log.debug("Request to partially update MDA : {}", mDA);

        return mDARepository
            .findById(mDA.getId())
            .map(existingMDA -> {
                if (mDA.getName() != null) {
                    existingMDA.setName(mDA.getName());
                }
                if (mDA.getCreatedDate() != null) {
                    existingMDA.setCreatedDate(mDA.getCreatedDate());
                }
                if (mDA.getCreatedBy() != null) {
                    existingMDA.setCreatedBy(mDA.getCreatedBy());
                }
                if (mDA.getUpdatedDate() != null) {
                    existingMDA.setUpdatedDate(mDA.getUpdatedDate());
                }
                if (mDA.getUpdatedBy() != null) {
                    existingMDA.setUpdatedBy(mDA.getUpdatedBy());
                }

                return existingMDA;
            })
            .map(mDARepository::save);
    }

    /**
     * Get all the mDAS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MDA> findAll(Pageable pageable) {
        log.debug("Request to get all MDAS");
        return mDARepository.findAll(pageable);
    }

    /**
     * Get one mDA by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MDA> findOne(Long id) {
        log.debug("Request to get MDA : {}", id);
        return mDARepository.findById(id);
    }

    /**
     * Delete the mDA by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MDA : {}", id);
        mDARepository.deleteById(id);
    }
}
