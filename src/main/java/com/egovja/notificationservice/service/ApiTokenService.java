package com.egovja.notificationservice.service;

import com.egovja.notificationservice.domain.ApiToken;
import com.egovja.notificationservice.repository.ApiTokenRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ApiToken}.
 */
@Service
@Transactional
public class ApiTokenService {

    private final Logger log = LoggerFactory.getLogger(ApiTokenService.class);

    private final ApiTokenRepository apiTokenRepository;

    public ApiTokenService(ApiTokenRepository apiTokenRepository) {
        this.apiTokenRepository = apiTokenRepository;
    }

    /**
     * Save a apiToken.
     *
     * @param apiToken the entity to save.
     * @return the persisted entity.
     */
    public ApiToken save(ApiToken apiToken) {
        log.debug("Request to save ApiToken : {}", apiToken);
        return apiTokenRepository.save(apiToken);
    }

    /**
     * Update a apiToken.
     *
     * @param apiToken the entity to save.
     * @return the persisted entity.
     */
    public ApiToken update(ApiToken apiToken) {
        log.debug("Request to update ApiToken : {}", apiToken);
        return apiTokenRepository.save(apiToken);
    }

    /**
     * Partially update a apiToken.
     *
     * @param apiToken the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ApiToken> partialUpdate(ApiToken apiToken) {
        log.debug("Request to partially update ApiToken : {}", apiToken);

        return apiTokenRepository
            .findById(apiToken.getId())
            .map(existingApiToken -> {
                if (apiToken.getToken() != null) {
                    existingApiToken.setToken(apiToken.getToken());
                }
                if (apiToken.getStatus() != null) {
                    existingApiToken.setStatus(apiToken.getStatus());
                }
                if (apiToken.getExpDate() != null) {
                    existingApiToken.setExpDate(apiToken.getExpDate());
                }
                if (apiToken.getCreatedDate() != null) {
                    existingApiToken.setCreatedDate(apiToken.getCreatedDate());
                }
                if (apiToken.getUpdatedDate() != null) {
                    existingApiToken.setUpdatedDate(apiToken.getUpdatedDate());
                }
                if (apiToken.getCreatedBy() != null) {
                    existingApiToken.setCreatedBy(apiToken.getCreatedBy());
                }
                if (apiToken.getUpdatedBy() != null) {
                    existingApiToken.setUpdatedBy(apiToken.getUpdatedBy());
                }

                return existingApiToken;
            })
            .map(apiTokenRepository::save);
    }

    /**
     * Get all the apiTokens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ApiToken> findAll(Pageable pageable) {
        log.debug("Request to get all ApiTokens");
        return apiTokenRepository.findAll(pageable);
    }

    /**
     * Get one apiToken by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ApiToken> findOne(Long id) {
        log.debug("Request to get ApiToken : {}", id);
        return apiTokenRepository.findById(id);
    }

    /**
     * Delete the apiToken by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ApiToken : {}", id);
        apiTokenRepository.deleteById(id);
    }
}
