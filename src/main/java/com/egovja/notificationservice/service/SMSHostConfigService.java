package com.egovja.notificationservice.service;

import com.egovja.notificationservice.domain.SMSHostConfig;
import com.egovja.notificationservice.repository.SMSHostConfigRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link SMSHostConfig}.
 */
@Service
@Transactional
public class SMSHostConfigService {

    private final Logger log = LoggerFactory.getLogger(SMSHostConfigService.class);

    private final SMSHostConfigRepository sMSHostConfigRepository;

    public SMSHostConfigService(SMSHostConfigRepository sMSHostConfigRepository) {
        this.sMSHostConfigRepository = sMSHostConfigRepository;
    }

    /**
     * Save a sMSHostConfig.
     *
     * @param sMSHostConfig the entity to save.
     * @return the persisted entity.
     */
    public SMSHostConfig save(SMSHostConfig sMSHostConfig) {
        log.debug("Request to save SMSHostConfig : {}", sMSHostConfig);
        return sMSHostConfigRepository.save(sMSHostConfig);
    }

    /**
     * Update a sMSHostConfig.
     *
     * @param sMSHostConfig the entity to save.
     * @return the persisted entity.
     */
    public SMSHostConfig update(SMSHostConfig sMSHostConfig) {
        log.debug("Request to update SMSHostConfig : {}", sMSHostConfig);
        return sMSHostConfigRepository.save(sMSHostConfig);
    }

    /**
     * Partially update a sMSHostConfig.
     *
     * @param sMSHostConfig the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<SMSHostConfig> partialUpdate(SMSHostConfig sMSHostConfig) {
        log.debug("Request to partially update SMSHostConfig : {}", sMSHostConfig);

        return sMSHostConfigRepository
            .findById(sMSHostConfig.getId())
            .map(existingSMSHostConfig -> {
                if (sMSHostConfig.getHostEndpoint() != null) {
                    existingSMSHostConfig.setHostEndpoint(sMSHostConfig.getHostEndpoint());
                }
                if (sMSHostConfig.getToken() != null) {
                    existingSMSHostConfig.setToken(sMSHostConfig.getToken());
                }
                if (sMSHostConfig.getSenderNumber() != null) {
                    existingSMSHostConfig.setSenderNumber(sMSHostConfig.getSenderNumber());
                }
                if (sMSHostConfig.getCreatedDate() != null) {
                    existingSMSHostConfig.setCreatedDate(sMSHostConfig.getCreatedDate());
                }
                if (sMSHostConfig.getCreatedBy() != null) {
                    existingSMSHostConfig.setCreatedBy(sMSHostConfig.getCreatedBy());
                }
                if (sMSHostConfig.getUpdatedDate() != null) {
                    existingSMSHostConfig.setUpdatedDate(sMSHostConfig.getUpdatedDate());
                }
                if (sMSHostConfig.getUpdatedBy() != null) {
                    existingSMSHostConfig.setUpdatedBy(sMSHostConfig.getUpdatedBy());
                }

                return existingSMSHostConfig;
            })
            .map(sMSHostConfigRepository::save);
    }

    /**
     * Get all the sMSHostConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SMSHostConfig> findAll(Pageable pageable) {
        log.debug("Request to get all SMSHostConfigs");
        return sMSHostConfigRepository.findAll(pageable);
    }

    /**
     * Get one sMSHostConfig by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SMSHostConfig> findOne(Long id) {
        log.debug("Request to get SMSHostConfig : {}", id);
        return sMSHostConfigRepository.findById(id);
    }

    /**
     * Delete the sMSHostConfig by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SMSHostConfig : {}", id);
        sMSHostConfigRepository.deleteById(id);
    }
}
