package com.egovja.notificationservice.service;

import com.egovja.notificationservice.domain.AppLogging;
import com.egovja.notificationservice.repository.AppLoggingRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link AppLogging}.
 */
@Service
@Transactional
public class AppLoggingService {

    private final Logger log = LoggerFactory.getLogger(AppLoggingService.class);

    private final AppLoggingRepository appLoggingRepository;

    public AppLoggingService(AppLoggingRepository appLoggingRepository) {
        this.appLoggingRepository = appLoggingRepository;
    }

    /**
     * Save a appLogging.
     *
     * @param appLogging the entity to save.
     * @return the persisted entity.
     */
    public AppLogging save(AppLogging appLogging) {
        log.debug("Request to save AppLogging : {}", appLogging);
        return appLoggingRepository.save(appLogging);
    }

    /**
     * Update a appLogging.
     *
     * @param appLogging the entity to save.
     * @return the persisted entity.
     */
    public AppLogging update(AppLogging appLogging) {
        log.debug("Request to update AppLogging : {}", appLogging);
        return appLoggingRepository.save(appLogging);
    }

    /**
     * Partially update a appLogging.
     *
     * @param appLogging the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<AppLogging> partialUpdate(AppLogging appLogging) {
        log.debug("Request to partially update AppLogging : {}", appLogging);

        return appLoggingRepository
            .findById(appLogging.getId())
            .map(existingAppLogging -> {
                if (appLogging.getComments() != null) {
                    existingAppLogging.setComments(appLogging.getComments());
                }
                if (appLogging.getActionType() != null) {
                    existingAppLogging.setActionType(appLogging.getActionType());
                }
                if (appLogging.getCreatedDate() != null) {
                    existingAppLogging.setCreatedDate(appLogging.getCreatedDate());
                }

                return existingAppLogging;
            })
            .map(appLoggingRepository::save);
    }

    /**
     * Get all the appLoggings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AppLogging> findAll(Pageable pageable) {
        log.debug("Request to get all AppLoggings");
        return appLoggingRepository.findAll(pageable);
    }

    /**
     * Get one appLogging by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AppLogging> findOne(Long id) {
        log.debug("Request to get AppLogging : {}", id);
        return appLoggingRepository.findById(id);
    }

    /**
     * Delete the appLogging by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AppLogging : {}", id);
        appLoggingRepository.deleteById(id);
    }
}
