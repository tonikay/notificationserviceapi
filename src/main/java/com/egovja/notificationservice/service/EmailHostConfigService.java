package com.egovja.notificationservice.service;

import com.egovja.notificationservice.domain.EmailHostConfig;
import com.egovja.notificationservice.repository.EmailHostConfigRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link EmailHostConfig}.
 */
@Service
@Transactional
public class EmailHostConfigService {

    private final Logger log = LoggerFactory.getLogger(EmailHostConfigService.class);

    private final EmailHostConfigRepository emailHostConfigRepository;

    public EmailHostConfigService(EmailHostConfigRepository emailHostConfigRepository) {
        this.emailHostConfigRepository = emailHostConfigRepository;
    }

    /**
     * Save a emailHostConfig.
     *
     * @param emailHostConfig the entity to save.
     * @return the persisted entity.
     */
    public EmailHostConfig save(EmailHostConfig emailHostConfig) {
        log.debug("Request to save EmailHostConfig : {}", emailHostConfig);
        return emailHostConfigRepository.save(emailHostConfig);
    }

    /**
     * Update a emailHostConfig.
     *
     * @param emailHostConfig the entity to save.
     * @return the persisted entity.
     */
    public EmailHostConfig update(EmailHostConfig emailHostConfig) {
        log.debug("Request to update EmailHostConfig : {}", emailHostConfig);
        return emailHostConfigRepository.save(emailHostConfig);
    }

    /**
     * Partially update a emailHostConfig.
     *
     * @param emailHostConfig the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<EmailHostConfig> partialUpdate(EmailHostConfig emailHostConfig) {
        log.debug("Request to partially update EmailHostConfig : {}", emailHostConfig);

        return emailHostConfigRepository
            .findById(emailHostConfig.getId())
            .map(existingEmailHostConfig -> {
                if (emailHostConfig.getSenderEmail() != null) {
                    existingEmailHostConfig.setSenderEmail(emailHostConfig.getSenderEmail());
                }
                if (emailHostConfig.getHost() != null) {
                    existingEmailHostConfig.setHost(emailHostConfig.getHost());
                }
                if (emailHostConfig.getPort() != null) {
                    existingEmailHostConfig.setPort(emailHostConfig.getPort());
                }
                if (emailHostConfig.getProtocol() != null) {
                    existingEmailHostConfig.setProtocol(emailHostConfig.getProtocol());
                }
                if (emailHostConfig.getUsername() != null) {
                    existingEmailHostConfig.setUsername(emailHostConfig.getUsername());
                }
                if (emailHostConfig.getPassword() != null) {
                    existingEmailHostConfig.setPassword(emailHostConfig.getPassword());
                }
                if (emailHostConfig.getCreatedDate() != null) {
                    existingEmailHostConfig.setCreatedDate(emailHostConfig.getCreatedDate());
                }
                if (emailHostConfig.getCreatedBy() != null) {
                    existingEmailHostConfig.setCreatedBy(emailHostConfig.getCreatedBy());
                }
                if (emailHostConfig.getUpdatedDate() != null) {
                    existingEmailHostConfig.setUpdatedDate(emailHostConfig.getUpdatedDate());
                }
                if (emailHostConfig.getUpdatedBy() != null) {
                    existingEmailHostConfig.setUpdatedBy(emailHostConfig.getUpdatedBy());
                }

                return existingEmailHostConfig;
            })
            .map(emailHostConfigRepository::save);
    }

    /**
     * Get all the emailHostConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<EmailHostConfig> findAll(Pageable pageable) {
        log.debug("Request to get all EmailHostConfigs");
        return emailHostConfigRepository.findAll(pageable);
    }

    /**
     * Get one emailHostConfig by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EmailHostConfig> findOne(Long id) {
        log.debug("Request to get EmailHostConfig : {}", id);
        return emailHostConfigRepository.findById(id);
    }

    /**
     * Delete the emailHostConfig by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EmailHostConfig : {}", id);
        emailHostConfigRepository.deleteById(id);
    }
}
