package com.egovja.notificationservice.repository;

import com.egovja.notificationservice.domain.EmailHostConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the EmailHostConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmailHostConfigRepository extends JpaRepository<EmailHostConfig, Long> {}
