package com.egovja.notificationservice.repository;

import com.egovja.notificationservice.domain.SMS;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the SMS entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SMSRepository extends JpaRepository<SMS, Long> {}
