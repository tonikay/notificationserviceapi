package com.egovja.notificationservice.repository;

import com.egovja.notificationservice.domain.SMSHostConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the SMSHostConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SMSHostConfigRepository extends JpaRepository<SMSHostConfig, Long> {}
