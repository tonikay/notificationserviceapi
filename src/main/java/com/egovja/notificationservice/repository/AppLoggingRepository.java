package com.egovja.notificationservice.repository;

import com.egovja.notificationservice.domain.AppLogging;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AppLogging entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AppLoggingRepository extends JpaRepository<AppLogging, Long> {}
