package com.egovja.notificationservice.repository;

import com.egovja.notificationservice.domain.ApiToken;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ApiToken entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApiTokenRepository extends JpaRepository<ApiToken, Long> {}
