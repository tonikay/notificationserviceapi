package com.egovja.notificationservice.repository;

import com.egovja.notificationservice.domain.MDA;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the MDA entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MDARepository extends JpaRepository<MDA, Long> {}
