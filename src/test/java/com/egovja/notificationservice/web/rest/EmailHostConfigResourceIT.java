package com.egovja.notificationservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.egovja.notificationservice.IntegrationTest;
import com.egovja.notificationservice.domain.EmailHostConfig;
import com.egovja.notificationservice.domain.enumeration.SecurityProtocol;
import com.egovja.notificationservice.repository.EmailHostConfigRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EmailHostConfigResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EmailHostConfigResourceIT {

    private static final String DEFAULT_SENDER_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_HOST = "AAAAAAAAAA";
    private static final String UPDATED_HOST = "BBBBBBBBBB";

    private static final Integer DEFAULT_PORT = 1;
    private static final Integer UPDATED_PORT = 2;

    private static final SecurityProtocol DEFAULT_PROTOCOL = SecurityProtocol.TLS;
    private static final SecurityProtocol UPDATED_PROTOCOL = SecurityProtocol.SSL;

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/email-host-configs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EmailHostConfigRepository emailHostConfigRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEmailHostConfigMockMvc;

    private EmailHostConfig emailHostConfig;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmailHostConfig createEntity(EntityManager em) {
        EmailHostConfig emailHostConfig = new EmailHostConfig()
            .senderEmail(DEFAULT_SENDER_EMAIL)
            .host(DEFAULT_HOST)
            .port(DEFAULT_PORT)
            .protocol(DEFAULT_PROTOCOL)
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY);
        return emailHostConfig;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmailHostConfig createUpdatedEntity(EntityManager em) {
        EmailHostConfig emailHostConfig = new EmailHostConfig()
            .senderEmail(UPDATED_SENDER_EMAIL)
            .host(UPDATED_HOST)
            .port(UPDATED_PORT)
            .protocol(UPDATED_PROTOCOL)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);
        return emailHostConfig;
    }

    @BeforeEach
    public void initTest() {
        emailHostConfig = createEntity(em);
    }

    @Test
    @Transactional
    void createEmailHostConfig() throws Exception {
        int databaseSizeBeforeCreate = emailHostConfigRepository.findAll().size();
        // Create the EmailHostConfig
        restEmailHostConfigMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isCreated());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeCreate + 1);
        EmailHostConfig testEmailHostConfig = emailHostConfigList.get(emailHostConfigList.size() - 1);
        assertThat(testEmailHostConfig.getSenderEmail()).isEqualTo(DEFAULT_SENDER_EMAIL);
        assertThat(testEmailHostConfig.getHost()).isEqualTo(DEFAULT_HOST);
        assertThat(testEmailHostConfig.getPort()).isEqualTo(DEFAULT_PORT);
        assertThat(testEmailHostConfig.getProtocol()).isEqualTo(DEFAULT_PROTOCOL);
        assertThat(testEmailHostConfig.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testEmailHostConfig.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testEmailHostConfig.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testEmailHostConfig.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testEmailHostConfig.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testEmailHostConfig.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createEmailHostConfigWithExistingId() throws Exception {
        // Create the EmailHostConfig with an existing ID
        emailHostConfig.setId(1L);

        int databaseSizeBeforeCreate = emailHostConfigRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmailHostConfigMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkSenderEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailHostConfigRepository.findAll().size();
        // set the field null
        emailHostConfig.setSenderEmail(null);

        // Create the EmailHostConfig, which fails.

        restEmailHostConfigMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isBadRequest());

        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkHostIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailHostConfigRepository.findAll().size();
        // set the field null
        emailHostConfig.setHost(null);

        // Create the EmailHostConfig, which fails.

        restEmailHostConfigMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isBadRequest());

        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPortIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailHostConfigRepository.findAll().size();
        // set the field null
        emailHostConfig.setPort(null);

        // Create the EmailHostConfig, which fails.

        restEmailHostConfigMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isBadRequest());

        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkProtocolIsRequired() throws Exception {
        int databaseSizeBeforeTest = emailHostConfigRepository.findAll().size();
        // set the field null
        emailHostConfig.setProtocol(null);

        // Create the EmailHostConfig, which fails.

        restEmailHostConfigMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isBadRequest());

        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEmailHostConfigs() throws Exception {
        // Initialize the database
        emailHostConfigRepository.saveAndFlush(emailHostConfig);

        // Get all the emailHostConfigList
        restEmailHostConfigMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(emailHostConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].senderEmail").value(hasItem(DEFAULT_SENDER_EMAIL)))
            .andExpect(jsonPath("$.[*].host").value(hasItem(DEFAULT_HOST)))
            .andExpect(jsonPath("$.[*].port").value(hasItem(DEFAULT_PORT)))
            .andExpect(jsonPath("$.[*].protocol").value(hasItem(DEFAULT_PROTOCOL.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getEmailHostConfig() throws Exception {
        // Initialize the database
        emailHostConfigRepository.saveAndFlush(emailHostConfig);

        // Get the emailHostConfig
        restEmailHostConfigMockMvc
            .perform(get(ENTITY_API_URL_ID, emailHostConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(emailHostConfig.getId().intValue()))
            .andExpect(jsonPath("$.senderEmail").value(DEFAULT_SENDER_EMAIL))
            .andExpect(jsonPath("$.host").value(DEFAULT_HOST))
            .andExpect(jsonPath("$.port").value(DEFAULT_PORT))
            .andExpect(jsonPath("$.protocol").value(DEFAULT_PROTOCOL.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingEmailHostConfig() throws Exception {
        // Get the emailHostConfig
        restEmailHostConfigMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingEmailHostConfig() throws Exception {
        // Initialize the database
        emailHostConfigRepository.saveAndFlush(emailHostConfig);

        int databaseSizeBeforeUpdate = emailHostConfigRepository.findAll().size();

        // Update the emailHostConfig
        EmailHostConfig updatedEmailHostConfig = emailHostConfigRepository.findById(emailHostConfig.getId()).get();
        // Disconnect from session so that the updates on updatedEmailHostConfig are not directly saved in db
        em.detach(updatedEmailHostConfig);
        updatedEmailHostConfig
            .senderEmail(UPDATED_SENDER_EMAIL)
            .host(UPDATED_HOST)
            .port(UPDATED_PORT)
            .protocol(UPDATED_PROTOCOL)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restEmailHostConfigMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedEmailHostConfig.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedEmailHostConfig))
            )
            .andExpect(status().isOk());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeUpdate);
        EmailHostConfig testEmailHostConfig = emailHostConfigList.get(emailHostConfigList.size() - 1);
        assertThat(testEmailHostConfig.getSenderEmail()).isEqualTo(UPDATED_SENDER_EMAIL);
        assertThat(testEmailHostConfig.getHost()).isEqualTo(UPDATED_HOST);
        assertThat(testEmailHostConfig.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testEmailHostConfig.getProtocol()).isEqualTo(UPDATED_PROTOCOL);
        assertThat(testEmailHostConfig.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testEmailHostConfig.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testEmailHostConfig.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testEmailHostConfig.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testEmailHostConfig.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testEmailHostConfig.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingEmailHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = emailHostConfigRepository.findAll().size();
        emailHostConfig.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmailHostConfigMockMvc
            .perform(
                put(ENTITY_API_URL_ID, emailHostConfig.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEmailHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = emailHostConfigRepository.findAll().size();
        emailHostConfig.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmailHostConfigMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEmailHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = emailHostConfigRepository.findAll().size();
        emailHostConfig.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmailHostConfigMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEmailHostConfigWithPatch() throws Exception {
        // Initialize the database
        emailHostConfigRepository.saveAndFlush(emailHostConfig);

        int databaseSizeBeforeUpdate = emailHostConfigRepository.findAll().size();

        // Update the emailHostConfig using partial update
        EmailHostConfig partialUpdatedEmailHostConfig = new EmailHostConfig();
        partialUpdatedEmailHostConfig.setId(emailHostConfig.getId());

        partialUpdatedEmailHostConfig
            .host(UPDATED_HOST)
            .port(UPDATED_PORT)
            .protocol(UPDATED_PROTOCOL)
            .username(UPDATED_USERNAME)
            .updatedBy(UPDATED_UPDATED_BY);

        restEmailHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmailHostConfig.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEmailHostConfig))
            )
            .andExpect(status().isOk());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeUpdate);
        EmailHostConfig testEmailHostConfig = emailHostConfigList.get(emailHostConfigList.size() - 1);
        assertThat(testEmailHostConfig.getSenderEmail()).isEqualTo(DEFAULT_SENDER_EMAIL);
        assertThat(testEmailHostConfig.getHost()).isEqualTo(UPDATED_HOST);
        assertThat(testEmailHostConfig.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testEmailHostConfig.getProtocol()).isEqualTo(UPDATED_PROTOCOL);
        assertThat(testEmailHostConfig.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testEmailHostConfig.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testEmailHostConfig.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testEmailHostConfig.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testEmailHostConfig.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testEmailHostConfig.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateEmailHostConfigWithPatch() throws Exception {
        // Initialize the database
        emailHostConfigRepository.saveAndFlush(emailHostConfig);

        int databaseSizeBeforeUpdate = emailHostConfigRepository.findAll().size();

        // Update the emailHostConfig using partial update
        EmailHostConfig partialUpdatedEmailHostConfig = new EmailHostConfig();
        partialUpdatedEmailHostConfig.setId(emailHostConfig.getId());

        partialUpdatedEmailHostConfig
            .senderEmail(UPDATED_SENDER_EMAIL)
            .host(UPDATED_HOST)
            .port(UPDATED_PORT)
            .protocol(UPDATED_PROTOCOL)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restEmailHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmailHostConfig.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEmailHostConfig))
            )
            .andExpect(status().isOk());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeUpdate);
        EmailHostConfig testEmailHostConfig = emailHostConfigList.get(emailHostConfigList.size() - 1);
        assertThat(testEmailHostConfig.getSenderEmail()).isEqualTo(UPDATED_SENDER_EMAIL);
        assertThat(testEmailHostConfig.getHost()).isEqualTo(UPDATED_HOST);
        assertThat(testEmailHostConfig.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testEmailHostConfig.getProtocol()).isEqualTo(UPDATED_PROTOCOL);
        assertThat(testEmailHostConfig.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testEmailHostConfig.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testEmailHostConfig.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testEmailHostConfig.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testEmailHostConfig.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testEmailHostConfig.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingEmailHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = emailHostConfigRepository.findAll().size();
        emailHostConfig.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmailHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, emailHostConfig.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEmailHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = emailHostConfigRepository.findAll().size();
        emailHostConfig.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmailHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEmailHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = emailHostConfigRepository.findAll().size();
        emailHostConfig.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmailHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(emailHostConfig))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EmailHostConfig in the database
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEmailHostConfig() throws Exception {
        // Initialize the database
        emailHostConfigRepository.saveAndFlush(emailHostConfig);

        int databaseSizeBeforeDelete = emailHostConfigRepository.findAll().size();

        // Delete the emailHostConfig
        restEmailHostConfigMockMvc
            .perform(delete(ENTITY_API_URL_ID, emailHostConfig.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EmailHostConfig> emailHostConfigList = emailHostConfigRepository.findAll();
        assertThat(emailHostConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
