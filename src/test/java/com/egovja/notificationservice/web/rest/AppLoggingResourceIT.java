package com.egovja.notificationservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.egovja.notificationservice.IntegrationTest;
import com.egovja.notificationservice.domain.AppLogging;
import com.egovja.notificationservice.domain.enumeration.ActionType;
import com.egovja.notificationservice.repository.AppLoggingRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AppLoggingResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AppLoggingResourceIT {

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final ActionType DEFAULT_ACTION_TYPE = ActionType.SMS;
    private static final ActionType UPDATED_ACTION_TYPE = ActionType.EMAIL;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/app-loggings";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AppLoggingRepository appLoggingRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAppLoggingMockMvc;

    private AppLogging appLogging;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AppLogging createEntity(EntityManager em) {
        AppLogging appLogging = new AppLogging()
            .comments(DEFAULT_COMMENTS)
            .actionType(DEFAULT_ACTION_TYPE)
            .createdDate(DEFAULT_CREATED_DATE);
        return appLogging;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AppLogging createUpdatedEntity(EntityManager em) {
        AppLogging appLogging = new AppLogging()
            .comments(UPDATED_COMMENTS)
            .actionType(UPDATED_ACTION_TYPE)
            .createdDate(UPDATED_CREATED_DATE);
        return appLogging;
    }

    @BeforeEach
    public void initTest() {
        appLogging = createEntity(em);
    }

    @Test
    @Transactional
    void createAppLogging() throws Exception {
        int databaseSizeBeforeCreate = appLoggingRepository.findAll().size();
        // Create the AppLogging
        restAppLoggingMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(appLogging)))
            .andExpect(status().isCreated());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeCreate + 1);
        AppLogging testAppLogging = appLoggingList.get(appLoggingList.size() - 1);
        assertThat(testAppLogging.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testAppLogging.getActionType()).isEqualTo(DEFAULT_ACTION_TYPE);
        assertThat(testAppLogging.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
    }

    @Test
    @Transactional
    void createAppLoggingWithExistingId() throws Exception {
        // Create the AppLogging with an existing ID
        appLogging.setId(1L);

        int databaseSizeBeforeCreate = appLoggingRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAppLoggingMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(appLogging)))
            .andExpect(status().isBadRequest());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAppLoggings() throws Exception {
        // Initialize the database
        appLoggingRepository.saveAndFlush(appLogging);

        // Get all the appLoggingList
        restAppLoggingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(appLogging.getId().intValue())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].actionType").value(hasItem(DEFAULT_ACTION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())));
    }

    @Test
    @Transactional
    void getAppLogging() throws Exception {
        // Initialize the database
        appLoggingRepository.saveAndFlush(appLogging);

        // Get the appLogging
        restAppLoggingMockMvc
            .perform(get(ENTITY_API_URL_ID, appLogging.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(appLogging.getId().intValue()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS))
            .andExpect(jsonPath("$.actionType").value(DEFAULT_ACTION_TYPE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingAppLogging() throws Exception {
        // Get the appLogging
        restAppLoggingMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingAppLogging() throws Exception {
        // Initialize the database
        appLoggingRepository.saveAndFlush(appLogging);

        int databaseSizeBeforeUpdate = appLoggingRepository.findAll().size();

        // Update the appLogging
        AppLogging updatedAppLogging = appLoggingRepository.findById(appLogging.getId()).get();
        // Disconnect from session so that the updates on updatedAppLogging are not directly saved in db
        em.detach(updatedAppLogging);
        updatedAppLogging.comments(UPDATED_COMMENTS).actionType(UPDATED_ACTION_TYPE).createdDate(UPDATED_CREATED_DATE);

        restAppLoggingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAppLogging.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAppLogging))
            )
            .andExpect(status().isOk());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeUpdate);
        AppLogging testAppLogging = appLoggingList.get(appLoggingList.size() - 1);
        assertThat(testAppLogging.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testAppLogging.getActionType()).isEqualTo(UPDATED_ACTION_TYPE);
        assertThat(testAppLogging.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void putNonExistingAppLogging() throws Exception {
        int databaseSizeBeforeUpdate = appLoggingRepository.findAll().size();
        appLogging.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAppLoggingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, appLogging.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(appLogging))
            )
            .andExpect(status().isBadRequest());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAppLogging() throws Exception {
        int databaseSizeBeforeUpdate = appLoggingRepository.findAll().size();
        appLogging.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAppLoggingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(appLogging))
            )
            .andExpect(status().isBadRequest());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAppLogging() throws Exception {
        int databaseSizeBeforeUpdate = appLoggingRepository.findAll().size();
        appLogging.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAppLoggingMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(appLogging)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAppLoggingWithPatch() throws Exception {
        // Initialize the database
        appLoggingRepository.saveAndFlush(appLogging);

        int databaseSizeBeforeUpdate = appLoggingRepository.findAll().size();

        // Update the appLogging using partial update
        AppLogging partialUpdatedAppLogging = new AppLogging();
        partialUpdatedAppLogging.setId(appLogging.getId());

        partialUpdatedAppLogging.actionType(UPDATED_ACTION_TYPE).createdDate(UPDATED_CREATED_DATE);

        restAppLoggingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAppLogging.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAppLogging))
            )
            .andExpect(status().isOk());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeUpdate);
        AppLogging testAppLogging = appLoggingList.get(appLoggingList.size() - 1);
        assertThat(testAppLogging.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testAppLogging.getActionType()).isEqualTo(UPDATED_ACTION_TYPE);
        assertThat(testAppLogging.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void fullUpdateAppLoggingWithPatch() throws Exception {
        // Initialize the database
        appLoggingRepository.saveAndFlush(appLogging);

        int databaseSizeBeforeUpdate = appLoggingRepository.findAll().size();

        // Update the appLogging using partial update
        AppLogging partialUpdatedAppLogging = new AppLogging();
        partialUpdatedAppLogging.setId(appLogging.getId());

        partialUpdatedAppLogging.comments(UPDATED_COMMENTS).actionType(UPDATED_ACTION_TYPE).createdDate(UPDATED_CREATED_DATE);

        restAppLoggingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAppLogging.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAppLogging))
            )
            .andExpect(status().isOk());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeUpdate);
        AppLogging testAppLogging = appLoggingList.get(appLoggingList.size() - 1);
        assertThat(testAppLogging.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testAppLogging.getActionType()).isEqualTo(UPDATED_ACTION_TYPE);
        assertThat(testAppLogging.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingAppLogging() throws Exception {
        int databaseSizeBeforeUpdate = appLoggingRepository.findAll().size();
        appLogging.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAppLoggingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, appLogging.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(appLogging))
            )
            .andExpect(status().isBadRequest());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAppLogging() throws Exception {
        int databaseSizeBeforeUpdate = appLoggingRepository.findAll().size();
        appLogging.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAppLoggingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(appLogging))
            )
            .andExpect(status().isBadRequest());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAppLogging() throws Exception {
        int databaseSizeBeforeUpdate = appLoggingRepository.findAll().size();
        appLogging.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAppLoggingMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(appLogging))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AppLogging in the database
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAppLogging() throws Exception {
        // Initialize the database
        appLoggingRepository.saveAndFlush(appLogging);

        int databaseSizeBeforeDelete = appLoggingRepository.findAll().size();

        // Delete the appLogging
        restAppLoggingMockMvc
            .perform(delete(ENTITY_API_URL_ID, appLogging.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AppLogging> appLoggingList = appLoggingRepository.findAll();
        assertThat(appLoggingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
