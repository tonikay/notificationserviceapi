package com.egovja.notificationservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.egovja.notificationservice.IntegrationTest;
import com.egovja.notificationservice.domain.SMS;
import com.egovja.notificationservice.repository.SMSRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SMSResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SMSResourceIT {

    private static final String DEFAULT_RECIPENT = "AAAAAAAAAA";
    private static final String UPDATED_RECIPENT = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/sms";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SMSRepository sMSRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSMSMockMvc;

    private SMS sMS;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SMS createEntity(EntityManager em) {
        SMS sMS = new SMS().recipent(DEFAULT_RECIPENT).message(DEFAULT_MESSAGE);
        return sMS;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SMS createUpdatedEntity(EntityManager em) {
        SMS sMS = new SMS().recipent(UPDATED_RECIPENT).message(UPDATED_MESSAGE);
        return sMS;
    }

    @BeforeEach
    public void initTest() {
        sMS = createEntity(em);
    }

    @Test
    @Transactional
    void createSMS() throws Exception {
        int databaseSizeBeforeCreate = sMSRepository.findAll().size();
        // Create the SMS
        restSMSMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMS)))
            .andExpect(status().isCreated());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeCreate + 1);
        SMS testSMS = sMSList.get(sMSList.size() - 1);
        assertThat(testSMS.getRecipent()).isEqualTo(DEFAULT_RECIPENT);
        assertThat(testSMS.getMessage()).isEqualTo(DEFAULT_MESSAGE);
    }

    @Test
    @Transactional
    void createSMSWithExistingId() throws Exception {
        // Create the SMS with an existing ID
        sMS.setId(1L);

        int databaseSizeBeforeCreate = sMSRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSMSMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMS)))
            .andExpect(status().isBadRequest());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSMS() throws Exception {
        // Initialize the database
        sMSRepository.saveAndFlush(sMS);

        // Get all the sMSList
        restSMSMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sMS.getId().intValue())))
            .andExpect(jsonPath("$.[*].recipent").value(hasItem(DEFAULT_RECIPENT)))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)));
    }

    @Test
    @Transactional
    void getSMS() throws Exception {
        // Initialize the database
        sMSRepository.saveAndFlush(sMS);

        // Get the sMS
        restSMSMockMvc
            .perform(get(ENTITY_API_URL_ID, sMS.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sMS.getId().intValue()))
            .andExpect(jsonPath("$.recipent").value(DEFAULT_RECIPENT))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE));
    }

    @Test
    @Transactional
    void getNonExistingSMS() throws Exception {
        // Get the sMS
        restSMSMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSMS() throws Exception {
        // Initialize the database
        sMSRepository.saveAndFlush(sMS);

        int databaseSizeBeforeUpdate = sMSRepository.findAll().size();

        // Update the sMS
        SMS updatedSMS = sMSRepository.findById(sMS.getId()).get();
        // Disconnect from session so that the updates on updatedSMS are not directly saved in db
        em.detach(updatedSMS);
        updatedSMS.recipent(UPDATED_RECIPENT).message(UPDATED_MESSAGE);

        restSMSMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedSMS.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedSMS))
            )
            .andExpect(status().isOk());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeUpdate);
        SMS testSMS = sMSList.get(sMSList.size() - 1);
        assertThat(testSMS.getRecipent()).isEqualTo(UPDATED_RECIPENT);
        assertThat(testSMS.getMessage()).isEqualTo(UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    void putNonExistingSMS() throws Exception {
        int databaseSizeBeforeUpdate = sMSRepository.findAll().size();
        sMS.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSMSMockMvc
            .perform(
                put(ENTITY_API_URL_ID, sMS.getId()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMS))
            )
            .andExpect(status().isBadRequest());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSMS() throws Exception {
        int databaseSizeBeforeUpdate = sMSRepository.findAll().size();
        sMS.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSMSMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sMS))
            )
            .andExpect(status().isBadRequest());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSMS() throws Exception {
        int databaseSizeBeforeUpdate = sMSRepository.findAll().size();
        sMS.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSMSMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMS)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSMSWithPatch() throws Exception {
        // Initialize the database
        sMSRepository.saveAndFlush(sMS);

        int databaseSizeBeforeUpdate = sMSRepository.findAll().size();

        // Update the sMS using partial update
        SMS partialUpdatedSMS = new SMS();
        partialUpdatedSMS.setId(sMS.getId());

        partialUpdatedSMS.recipent(UPDATED_RECIPENT).message(UPDATED_MESSAGE);

        restSMSMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSMS.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSMS))
            )
            .andExpect(status().isOk());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeUpdate);
        SMS testSMS = sMSList.get(sMSList.size() - 1);
        assertThat(testSMS.getRecipent()).isEqualTo(UPDATED_RECIPENT);
        assertThat(testSMS.getMessage()).isEqualTo(UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    void fullUpdateSMSWithPatch() throws Exception {
        // Initialize the database
        sMSRepository.saveAndFlush(sMS);

        int databaseSizeBeforeUpdate = sMSRepository.findAll().size();

        // Update the sMS using partial update
        SMS partialUpdatedSMS = new SMS();
        partialUpdatedSMS.setId(sMS.getId());

        partialUpdatedSMS.recipent(UPDATED_RECIPENT).message(UPDATED_MESSAGE);

        restSMSMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSMS.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSMS))
            )
            .andExpect(status().isOk());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeUpdate);
        SMS testSMS = sMSList.get(sMSList.size() - 1);
        assertThat(testSMS.getRecipent()).isEqualTo(UPDATED_RECIPENT);
        assertThat(testSMS.getMessage()).isEqualTo(UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    void patchNonExistingSMS() throws Exception {
        int databaseSizeBeforeUpdate = sMSRepository.findAll().size();
        sMS.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSMSMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, sMS.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sMS))
            )
            .andExpect(status().isBadRequest());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSMS() throws Exception {
        int databaseSizeBeforeUpdate = sMSRepository.findAll().size();
        sMS.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSMSMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sMS))
            )
            .andExpect(status().isBadRequest());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSMS() throws Exception {
        int databaseSizeBeforeUpdate = sMSRepository.findAll().size();
        sMS.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSMSMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(sMS)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the SMS in the database
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSMS() throws Exception {
        // Initialize the database
        sMSRepository.saveAndFlush(sMS);

        int databaseSizeBeforeDelete = sMSRepository.findAll().size();

        // Delete the sMS
        restSMSMockMvc.perform(delete(ENTITY_API_URL_ID, sMS.getId()).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SMS> sMSList = sMSRepository.findAll();
        assertThat(sMSList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
