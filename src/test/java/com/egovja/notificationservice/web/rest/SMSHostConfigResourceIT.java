package com.egovja.notificationservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.egovja.notificationservice.IntegrationTest;
import com.egovja.notificationservice.domain.SMSHostConfig;
import com.egovja.notificationservice.repository.SMSHostConfigRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SMSHostConfigResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SMSHostConfigResourceIT {

    private static final String DEFAULT_HOST_ENDPOINT = "AAAAAAAAAA";
    private static final String UPDATED_HOST_ENDPOINT = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN = "BBBBBBBBBB";

    private static final Long DEFAULT_SENDER_NUMBER = 1L;
    private static final Long UPDATED_SENDER_NUMBER = 2L;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/sms-host-configs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SMSHostConfigRepository sMSHostConfigRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSMSHostConfigMockMvc;

    private SMSHostConfig sMSHostConfig;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SMSHostConfig createEntity(EntityManager em) {
        SMSHostConfig sMSHostConfig = new SMSHostConfig()
            .hostEndpoint(DEFAULT_HOST_ENDPOINT)
            .token(DEFAULT_TOKEN)
            .senderNumber(DEFAULT_SENDER_NUMBER)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY);
        return sMSHostConfig;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SMSHostConfig createUpdatedEntity(EntityManager em) {
        SMSHostConfig sMSHostConfig = new SMSHostConfig()
            .hostEndpoint(UPDATED_HOST_ENDPOINT)
            .token(UPDATED_TOKEN)
            .senderNumber(UPDATED_SENDER_NUMBER)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);
        return sMSHostConfig;
    }

    @BeforeEach
    public void initTest() {
        sMSHostConfig = createEntity(em);
    }

    @Test
    @Transactional
    void createSMSHostConfig() throws Exception {
        int databaseSizeBeforeCreate = sMSHostConfigRepository.findAll().size();
        // Create the SMSHostConfig
        restSMSHostConfigMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMSHostConfig)))
            .andExpect(status().isCreated());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeCreate + 1);
        SMSHostConfig testSMSHostConfig = sMSHostConfigList.get(sMSHostConfigList.size() - 1);
        assertThat(testSMSHostConfig.getHostEndpoint()).isEqualTo(DEFAULT_HOST_ENDPOINT);
        assertThat(testSMSHostConfig.getToken()).isEqualTo(DEFAULT_TOKEN);
        assertThat(testSMSHostConfig.getSenderNumber()).isEqualTo(DEFAULT_SENDER_NUMBER);
        assertThat(testSMSHostConfig.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testSMSHostConfig.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testSMSHostConfig.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testSMSHostConfig.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createSMSHostConfigWithExistingId() throws Exception {
        // Create the SMSHostConfig with an existing ID
        sMSHostConfig.setId(1L);

        int databaseSizeBeforeCreate = sMSHostConfigRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSMSHostConfigMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMSHostConfig)))
            .andExpect(status().isBadRequest());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkHostEndpointIsRequired() throws Exception {
        int databaseSizeBeforeTest = sMSHostConfigRepository.findAll().size();
        // set the field null
        sMSHostConfig.setHostEndpoint(null);

        // Create the SMSHostConfig, which fails.

        restSMSHostConfigMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMSHostConfig)))
            .andExpect(status().isBadRequest());

        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTokenIsRequired() throws Exception {
        int databaseSizeBeforeTest = sMSHostConfigRepository.findAll().size();
        // set the field null
        sMSHostConfig.setToken(null);

        // Create the SMSHostConfig, which fails.

        restSMSHostConfigMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMSHostConfig)))
            .andExpect(status().isBadRequest());

        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSenderNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = sMSHostConfigRepository.findAll().size();
        // set the field null
        sMSHostConfig.setSenderNumber(null);

        // Create the SMSHostConfig, which fails.

        restSMSHostConfigMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMSHostConfig)))
            .andExpect(status().isBadRequest());

        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllSMSHostConfigs() throws Exception {
        // Initialize the database
        sMSHostConfigRepository.saveAndFlush(sMSHostConfig);

        // Get all the sMSHostConfigList
        restSMSHostConfigMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sMSHostConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].hostEndpoint").value(hasItem(DEFAULT_HOST_ENDPOINT)))
            .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_TOKEN)))
            .andExpect(jsonPath("$.[*].senderNumber").value(hasItem(DEFAULT_SENDER_NUMBER.intValue())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getSMSHostConfig() throws Exception {
        // Initialize the database
        sMSHostConfigRepository.saveAndFlush(sMSHostConfig);

        // Get the sMSHostConfig
        restSMSHostConfigMockMvc
            .perform(get(ENTITY_API_URL_ID, sMSHostConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sMSHostConfig.getId().intValue()))
            .andExpect(jsonPath("$.hostEndpoint").value(DEFAULT_HOST_ENDPOINT))
            .andExpect(jsonPath("$.token").value(DEFAULT_TOKEN))
            .andExpect(jsonPath("$.senderNumber").value(DEFAULT_SENDER_NUMBER.intValue()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingSMSHostConfig() throws Exception {
        // Get the sMSHostConfig
        restSMSHostConfigMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSMSHostConfig() throws Exception {
        // Initialize the database
        sMSHostConfigRepository.saveAndFlush(sMSHostConfig);

        int databaseSizeBeforeUpdate = sMSHostConfigRepository.findAll().size();

        // Update the sMSHostConfig
        SMSHostConfig updatedSMSHostConfig = sMSHostConfigRepository.findById(sMSHostConfig.getId()).get();
        // Disconnect from session so that the updates on updatedSMSHostConfig are not directly saved in db
        em.detach(updatedSMSHostConfig);
        updatedSMSHostConfig
            .hostEndpoint(UPDATED_HOST_ENDPOINT)
            .token(UPDATED_TOKEN)
            .senderNumber(UPDATED_SENDER_NUMBER)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restSMSHostConfigMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedSMSHostConfig.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedSMSHostConfig))
            )
            .andExpect(status().isOk());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeUpdate);
        SMSHostConfig testSMSHostConfig = sMSHostConfigList.get(sMSHostConfigList.size() - 1);
        assertThat(testSMSHostConfig.getHostEndpoint()).isEqualTo(UPDATED_HOST_ENDPOINT);
        assertThat(testSMSHostConfig.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testSMSHostConfig.getSenderNumber()).isEqualTo(UPDATED_SENDER_NUMBER);
        assertThat(testSMSHostConfig.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testSMSHostConfig.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSMSHostConfig.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testSMSHostConfig.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingSMSHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = sMSHostConfigRepository.findAll().size();
        sMSHostConfig.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSMSHostConfigMockMvc
            .perform(
                put(ENTITY_API_URL_ID, sMSHostConfig.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sMSHostConfig))
            )
            .andExpect(status().isBadRequest());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSMSHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = sMSHostConfigRepository.findAll().size();
        sMSHostConfig.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSMSHostConfigMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sMSHostConfig))
            )
            .andExpect(status().isBadRequest());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSMSHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = sMSHostConfigRepository.findAll().size();
        sMSHostConfig.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSMSHostConfigMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sMSHostConfig)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSMSHostConfigWithPatch() throws Exception {
        // Initialize the database
        sMSHostConfigRepository.saveAndFlush(sMSHostConfig);

        int databaseSizeBeforeUpdate = sMSHostConfigRepository.findAll().size();

        // Update the sMSHostConfig using partial update
        SMSHostConfig partialUpdatedSMSHostConfig = new SMSHostConfig();
        partialUpdatedSMSHostConfig.setId(sMSHostConfig.getId());

        partialUpdatedSMSHostConfig.token(UPDATED_TOKEN).updatedDate(UPDATED_UPDATED_DATE);

        restSMSHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSMSHostConfig.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSMSHostConfig))
            )
            .andExpect(status().isOk());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeUpdate);
        SMSHostConfig testSMSHostConfig = sMSHostConfigList.get(sMSHostConfigList.size() - 1);
        assertThat(testSMSHostConfig.getHostEndpoint()).isEqualTo(DEFAULT_HOST_ENDPOINT);
        assertThat(testSMSHostConfig.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testSMSHostConfig.getSenderNumber()).isEqualTo(DEFAULT_SENDER_NUMBER);
        assertThat(testSMSHostConfig.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testSMSHostConfig.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testSMSHostConfig.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testSMSHostConfig.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateSMSHostConfigWithPatch() throws Exception {
        // Initialize the database
        sMSHostConfigRepository.saveAndFlush(sMSHostConfig);

        int databaseSizeBeforeUpdate = sMSHostConfigRepository.findAll().size();

        // Update the sMSHostConfig using partial update
        SMSHostConfig partialUpdatedSMSHostConfig = new SMSHostConfig();
        partialUpdatedSMSHostConfig.setId(sMSHostConfig.getId());

        partialUpdatedSMSHostConfig
            .hostEndpoint(UPDATED_HOST_ENDPOINT)
            .token(UPDATED_TOKEN)
            .senderNumber(UPDATED_SENDER_NUMBER)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restSMSHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSMSHostConfig.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSMSHostConfig))
            )
            .andExpect(status().isOk());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeUpdate);
        SMSHostConfig testSMSHostConfig = sMSHostConfigList.get(sMSHostConfigList.size() - 1);
        assertThat(testSMSHostConfig.getHostEndpoint()).isEqualTo(UPDATED_HOST_ENDPOINT);
        assertThat(testSMSHostConfig.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testSMSHostConfig.getSenderNumber()).isEqualTo(UPDATED_SENDER_NUMBER);
        assertThat(testSMSHostConfig.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testSMSHostConfig.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSMSHostConfig.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testSMSHostConfig.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingSMSHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = sMSHostConfigRepository.findAll().size();
        sMSHostConfig.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSMSHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, sMSHostConfig.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sMSHostConfig))
            )
            .andExpect(status().isBadRequest());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSMSHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = sMSHostConfigRepository.findAll().size();
        sMSHostConfig.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSMSHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sMSHostConfig))
            )
            .andExpect(status().isBadRequest());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSMSHostConfig() throws Exception {
        int databaseSizeBeforeUpdate = sMSHostConfigRepository.findAll().size();
        sMSHostConfig.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSMSHostConfigMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(sMSHostConfig))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the SMSHostConfig in the database
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSMSHostConfig() throws Exception {
        // Initialize the database
        sMSHostConfigRepository.saveAndFlush(sMSHostConfig);

        int databaseSizeBeforeDelete = sMSHostConfigRepository.findAll().size();

        // Delete the sMSHostConfig
        restSMSHostConfigMockMvc
            .perform(delete(ENTITY_API_URL_ID, sMSHostConfig.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SMSHostConfig> sMSHostConfigList = sMSHostConfigRepository.findAll();
        assertThat(sMSHostConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
