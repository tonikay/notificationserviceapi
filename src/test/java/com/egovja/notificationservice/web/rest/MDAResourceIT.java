package com.egovja.notificationservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.egovja.notificationservice.IntegrationTest;
import com.egovja.notificationservice.domain.MDA;
import com.egovja.notificationservice.repository.MDARepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MDAResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MDAResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/mdas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MDARepository mDARepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMDAMockMvc;

    private MDA mDA;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MDA createEntity(EntityManager em) {
        MDA mDA = new MDA()
            .name(DEFAULT_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY);
        return mDA;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MDA createUpdatedEntity(EntityManager em) {
        MDA mDA = new MDA()
            .name(UPDATED_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);
        return mDA;
    }

    @BeforeEach
    public void initTest() {
        mDA = createEntity(em);
    }

    @Test
    @Transactional
    void createMDA() throws Exception {
        int databaseSizeBeforeCreate = mDARepository.findAll().size();
        // Create the MDA
        restMDAMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mDA)))
            .andExpect(status().isCreated());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeCreate + 1);
        MDA testMDA = mDAList.get(mDAList.size() - 1);
        assertThat(testMDA.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMDA.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testMDA.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMDA.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testMDA.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createMDAWithExistingId() throws Exception {
        // Create the MDA with an existing ID
        mDA.setId(1L);

        int databaseSizeBeforeCreate = mDARepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMDAMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mDA)))
            .andExpect(status().isBadRequest());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMDAS() throws Exception {
        // Initialize the database
        mDARepository.saveAndFlush(mDA);

        // Get all the mDAList
        restMDAMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mDA.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getMDA() throws Exception {
        // Initialize the database
        mDARepository.saveAndFlush(mDA);

        // Get the mDA
        restMDAMockMvc
            .perform(get(ENTITY_API_URL_ID, mDA.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(mDA.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingMDA() throws Exception {
        // Get the mDA
        restMDAMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingMDA() throws Exception {
        // Initialize the database
        mDARepository.saveAndFlush(mDA);

        int databaseSizeBeforeUpdate = mDARepository.findAll().size();

        // Update the mDA
        MDA updatedMDA = mDARepository.findById(mDA.getId()).get();
        // Disconnect from session so that the updates on updatedMDA are not directly saved in db
        em.detach(updatedMDA);
        updatedMDA
            .name(UPDATED_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restMDAMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMDA.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMDA))
            )
            .andExpect(status().isOk());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeUpdate);
        MDA testMDA = mDAList.get(mDAList.size() - 1);
        assertThat(testMDA.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMDA.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMDA.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMDA.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testMDA.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingMDA() throws Exception {
        int databaseSizeBeforeUpdate = mDARepository.findAll().size();
        mDA.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMDAMockMvc
            .perform(
                put(ENTITY_API_URL_ID, mDA.getId()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mDA))
            )
            .andExpect(status().isBadRequest());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMDA() throws Exception {
        int databaseSizeBeforeUpdate = mDARepository.findAll().size();
        mDA.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMDAMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(mDA))
            )
            .andExpect(status().isBadRequest());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMDA() throws Exception {
        int databaseSizeBeforeUpdate = mDARepository.findAll().size();
        mDA.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMDAMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mDA)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMDAWithPatch() throws Exception {
        // Initialize the database
        mDARepository.saveAndFlush(mDA);

        int databaseSizeBeforeUpdate = mDARepository.findAll().size();

        // Update the mDA using partial update
        MDA partialUpdatedMDA = new MDA();
        partialUpdatedMDA.setId(mDA.getId());

        partialUpdatedMDA.updatedBy(UPDATED_UPDATED_BY);

        restMDAMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMDA.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMDA))
            )
            .andExpect(status().isOk());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeUpdate);
        MDA testMDA = mDAList.get(mDAList.size() - 1);
        assertThat(testMDA.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMDA.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testMDA.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMDA.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testMDA.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateMDAWithPatch() throws Exception {
        // Initialize the database
        mDARepository.saveAndFlush(mDA);

        int databaseSizeBeforeUpdate = mDARepository.findAll().size();

        // Update the mDA using partial update
        MDA partialUpdatedMDA = new MDA();
        partialUpdatedMDA.setId(mDA.getId());

        partialUpdatedMDA
            .name(UPDATED_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restMDAMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMDA.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMDA))
            )
            .andExpect(status().isOk());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeUpdate);
        MDA testMDA = mDAList.get(mDAList.size() - 1);
        assertThat(testMDA.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMDA.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMDA.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMDA.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testMDA.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingMDA() throws Exception {
        int databaseSizeBeforeUpdate = mDARepository.findAll().size();
        mDA.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMDAMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, mDA.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mDA))
            )
            .andExpect(status().isBadRequest());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMDA() throws Exception {
        int databaseSizeBeforeUpdate = mDARepository.findAll().size();
        mDA.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMDAMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mDA))
            )
            .andExpect(status().isBadRequest());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMDA() throws Exception {
        int databaseSizeBeforeUpdate = mDARepository.findAll().size();
        mDA.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMDAMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(mDA)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MDA in the database
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMDA() throws Exception {
        // Initialize the database
        mDARepository.saveAndFlush(mDA);

        int databaseSizeBeforeDelete = mDARepository.findAll().size();

        // Delete the mDA
        restMDAMockMvc.perform(delete(ENTITY_API_URL_ID, mDA.getId()).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MDA> mDAList = mDARepository.findAll();
        assertThat(mDAList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
