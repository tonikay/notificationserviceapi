package com.egovja.notificationservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.egovja.notificationservice.IntegrationTest;
import com.egovja.notificationservice.domain.ApiToken;
import com.egovja.notificationservice.domain.enumeration.Status;
import com.egovja.notificationservice.repository.ApiTokenRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ApiTokenResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ApiTokenResourceIT {

    private static final String DEFAULT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.EXPIRED;
    private static final Status UPDATED_STATUS = Status.ACTIVE;

    private static final Instant DEFAULT_EXP_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_EXP_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/api-tokens";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ApiTokenRepository apiTokenRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restApiTokenMockMvc;

    private ApiToken apiToken;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApiToken createEntity(EntityManager em) {
        ApiToken apiToken = new ApiToken()
            .token(DEFAULT_TOKEN)
            .status(DEFAULT_STATUS)
            .expDate(DEFAULT_EXP_DATE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY);
        return apiToken;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApiToken createUpdatedEntity(EntityManager em) {
        ApiToken apiToken = new ApiToken()
            .token(UPDATED_TOKEN)
            .status(UPDATED_STATUS)
            .expDate(UPDATED_EXP_DATE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);
        return apiToken;
    }

    @BeforeEach
    public void initTest() {
        apiToken = createEntity(em);
    }

    @Test
    @Transactional
    void createApiToken() throws Exception {
        int databaseSizeBeforeCreate = apiTokenRepository.findAll().size();
        // Create the ApiToken
        restApiTokenMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(apiToken)))
            .andExpect(status().isCreated());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeCreate + 1);
        ApiToken testApiToken = apiTokenList.get(apiTokenList.size() - 1);
        assertThat(testApiToken.getToken()).isEqualTo(DEFAULT_TOKEN);
        assertThat(testApiToken.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testApiToken.getExpDate()).isEqualTo(DEFAULT_EXP_DATE);
        assertThat(testApiToken.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testApiToken.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testApiToken.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testApiToken.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createApiTokenWithExistingId() throws Exception {
        // Create the ApiToken with an existing ID
        apiToken.setId(1L);

        int databaseSizeBeforeCreate = apiTokenRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restApiTokenMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(apiToken)))
            .andExpect(status().isBadRequest());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllApiTokens() throws Exception {
        // Initialize the database
        apiTokenRepository.saveAndFlush(apiToken);

        // Get all the apiTokenList
        restApiTokenMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(apiToken.getId().intValue())))
            .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_TOKEN)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].expDate").value(hasItem(DEFAULT_EXP_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getApiToken() throws Exception {
        // Initialize the database
        apiTokenRepository.saveAndFlush(apiToken);

        // Get the apiToken
        restApiTokenMockMvc
            .perform(get(ENTITY_API_URL_ID, apiToken.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(apiToken.getId().intValue()))
            .andExpect(jsonPath("$.token").value(DEFAULT_TOKEN))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.expDate").value(DEFAULT_EXP_DATE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingApiToken() throws Exception {
        // Get the apiToken
        restApiTokenMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingApiToken() throws Exception {
        // Initialize the database
        apiTokenRepository.saveAndFlush(apiToken);

        int databaseSizeBeforeUpdate = apiTokenRepository.findAll().size();

        // Update the apiToken
        ApiToken updatedApiToken = apiTokenRepository.findById(apiToken.getId()).get();
        // Disconnect from session so that the updates on updatedApiToken are not directly saved in db
        em.detach(updatedApiToken);
        updatedApiToken
            .token(UPDATED_TOKEN)
            .status(UPDATED_STATUS)
            .expDate(UPDATED_EXP_DATE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restApiTokenMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedApiToken.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedApiToken))
            )
            .andExpect(status().isOk());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeUpdate);
        ApiToken testApiToken = apiTokenList.get(apiTokenList.size() - 1);
        assertThat(testApiToken.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testApiToken.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testApiToken.getExpDate()).isEqualTo(UPDATED_EXP_DATE);
        assertThat(testApiToken.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testApiToken.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testApiToken.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testApiToken.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingApiToken() throws Exception {
        int databaseSizeBeforeUpdate = apiTokenRepository.findAll().size();
        apiToken.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApiTokenMockMvc
            .perform(
                put(ENTITY_API_URL_ID, apiToken.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(apiToken))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchApiToken() throws Exception {
        int databaseSizeBeforeUpdate = apiTokenRepository.findAll().size();
        apiToken.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApiTokenMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(apiToken))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamApiToken() throws Exception {
        int databaseSizeBeforeUpdate = apiTokenRepository.findAll().size();
        apiToken.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApiTokenMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(apiToken)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateApiTokenWithPatch() throws Exception {
        // Initialize the database
        apiTokenRepository.saveAndFlush(apiToken);

        int databaseSizeBeforeUpdate = apiTokenRepository.findAll().size();

        // Update the apiToken using partial update
        ApiToken partialUpdatedApiToken = new ApiToken();
        partialUpdatedApiToken.setId(apiToken.getId());

        partialUpdatedApiToken.expDate(UPDATED_EXP_DATE).createdBy(UPDATED_CREATED_BY);

        restApiTokenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedApiToken.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedApiToken))
            )
            .andExpect(status().isOk());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeUpdate);
        ApiToken testApiToken = apiTokenList.get(apiTokenList.size() - 1);
        assertThat(testApiToken.getToken()).isEqualTo(DEFAULT_TOKEN);
        assertThat(testApiToken.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testApiToken.getExpDate()).isEqualTo(UPDATED_EXP_DATE);
        assertThat(testApiToken.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testApiToken.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testApiToken.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testApiToken.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateApiTokenWithPatch() throws Exception {
        // Initialize the database
        apiTokenRepository.saveAndFlush(apiToken);

        int databaseSizeBeforeUpdate = apiTokenRepository.findAll().size();

        // Update the apiToken using partial update
        ApiToken partialUpdatedApiToken = new ApiToken();
        partialUpdatedApiToken.setId(apiToken.getId());

        partialUpdatedApiToken
            .token(UPDATED_TOKEN)
            .status(UPDATED_STATUS)
            .expDate(UPDATED_EXP_DATE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY);

        restApiTokenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedApiToken.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedApiToken))
            )
            .andExpect(status().isOk());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeUpdate);
        ApiToken testApiToken = apiTokenList.get(apiTokenList.size() - 1);
        assertThat(testApiToken.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testApiToken.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testApiToken.getExpDate()).isEqualTo(UPDATED_EXP_DATE);
        assertThat(testApiToken.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testApiToken.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testApiToken.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testApiToken.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingApiToken() throws Exception {
        int databaseSizeBeforeUpdate = apiTokenRepository.findAll().size();
        apiToken.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApiTokenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, apiToken.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(apiToken))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchApiToken() throws Exception {
        int databaseSizeBeforeUpdate = apiTokenRepository.findAll().size();
        apiToken.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApiTokenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(apiToken))
            )
            .andExpect(status().isBadRequest());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamApiToken() throws Exception {
        int databaseSizeBeforeUpdate = apiTokenRepository.findAll().size();
        apiToken.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restApiTokenMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(apiToken)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ApiToken in the database
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteApiToken() throws Exception {
        // Initialize the database
        apiTokenRepository.saveAndFlush(apiToken);

        int databaseSizeBeforeDelete = apiTokenRepository.findAll().size();

        // Delete the apiToken
        restApiTokenMockMvc
            .perform(delete(ENTITY_API_URL_ID, apiToken.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ApiToken> apiTokenList = apiTokenRepository.findAll();
        assertThat(apiTokenList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
