package com.egovja.notificationservice.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.egovja.notificationservice.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SMSHostConfigTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SMSHostConfig.class);
        SMSHostConfig sMSHostConfig1 = new SMSHostConfig();
        sMSHostConfig1.setId(1L);
        SMSHostConfig sMSHostConfig2 = new SMSHostConfig();
        sMSHostConfig2.setId(sMSHostConfig1.getId());
        assertThat(sMSHostConfig1).isEqualTo(sMSHostConfig2);
        sMSHostConfig2.setId(2L);
        assertThat(sMSHostConfig1).isNotEqualTo(sMSHostConfig2);
        sMSHostConfig1.setId(null);
        assertThat(sMSHostConfig1).isNotEqualTo(sMSHostConfig2);
    }
}
