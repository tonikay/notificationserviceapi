package com.egovja.notificationservice.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.egovja.notificationservice.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MDATest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MDA.class);
        MDA mDA1 = new MDA();
        mDA1.setId(1L);
        MDA mDA2 = new MDA();
        mDA2.setId(mDA1.getId());
        assertThat(mDA1).isEqualTo(mDA2);
        mDA2.setId(2L);
        assertThat(mDA1).isNotEqualTo(mDA2);
        mDA1.setId(null);
        assertThat(mDA1).isNotEqualTo(mDA2);
    }
}
