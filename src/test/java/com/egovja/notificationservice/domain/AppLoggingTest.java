package com.egovja.notificationservice.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.egovja.notificationservice.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AppLoggingTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AppLogging.class);
        AppLogging appLogging1 = new AppLogging();
        appLogging1.setId(1L);
        AppLogging appLogging2 = new AppLogging();
        appLogging2.setId(appLogging1.getId());
        assertThat(appLogging1).isEqualTo(appLogging2);
        appLogging2.setId(2L);
        assertThat(appLogging1).isNotEqualTo(appLogging2);
        appLogging1.setId(null);
        assertThat(appLogging1).isNotEqualTo(appLogging2);
    }
}
