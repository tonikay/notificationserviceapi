package com.egovja.notificationservice.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.egovja.notificationservice.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SMSTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SMS.class);
        SMS sMS1 = new SMS();
        sMS1.setId(1L);
        SMS sMS2 = new SMS();
        sMS2.setId(sMS1.getId());
        assertThat(sMS1).isEqualTo(sMS2);
        sMS2.setId(2L);
        assertThat(sMS1).isNotEqualTo(sMS2);
        sMS1.setId(null);
        assertThat(sMS1).isNotEqualTo(sMS2);
    }
}
