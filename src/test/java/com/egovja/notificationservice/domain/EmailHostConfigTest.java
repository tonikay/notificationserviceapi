package com.egovja.notificationservice.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.egovja.notificationservice.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EmailHostConfigTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmailHostConfig.class);
        EmailHostConfig emailHostConfig1 = new EmailHostConfig();
        emailHostConfig1.setId(1L);
        EmailHostConfig emailHostConfig2 = new EmailHostConfig();
        emailHostConfig2.setId(emailHostConfig1.getId());
        assertThat(emailHostConfig1).isEqualTo(emailHostConfig2);
        emailHostConfig2.setId(2L);
        assertThat(emailHostConfig1).isNotEqualTo(emailHostConfig2);
        emailHostConfig1.setId(null);
        assertThat(emailHostConfig1).isNotEqualTo(emailHostConfig2);
    }
}
