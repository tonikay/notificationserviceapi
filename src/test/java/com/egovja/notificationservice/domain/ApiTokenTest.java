package com.egovja.notificationservice.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.egovja.notificationservice.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ApiTokenTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ApiToken.class);
        ApiToken apiToken1 = new ApiToken();
        apiToken1.setId(1L);
        ApiToken apiToken2 = new ApiToken();
        apiToken2.setId(apiToken1.getId());
        assertThat(apiToken1).isEqualTo(apiToken2);
        apiToken2.setId(2L);
        assertThat(apiToken1).isNotEqualTo(apiToken2);
        apiToken1.setId(null);
        assertThat(apiToken1).isNotEqualTo(apiToken2);
    }
}
